<?php


namespace WBuilder\Core;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Request as HttpRequest;
use WBuilder\Core\Classes\Cart\Cart;
use WBuilder\Core\Common\Exception\InvalidRequestException;
use WBuilder\Core\Common\Http\ClientInterface;
use WBuilder\Core\Common\Http\Exception;
use WBuilder\Core\Common\Messages\RequestInterface;
use WBuilder\Core\Common\Messages\ResponseInterface;
use WBuilder\Core\Enums\Mode;
use WBuilder\Core\Models\Address;
use WBuilder\Core\Models\Article;
use WBuilder\Core\Models\ArticleCustomerComment;
use WBuilder\Core\Models\ArticlePaginate;
use WBuilder\Core\Models\Category;
use WBuilder\Core\Models\City;
use WBuilder\Core\Models\Country;
use WBuilder\Core\Models\Currency;
use WBuilder\Core\Models\Customer;
use WBuilder\Core\Models\Draft;
use WBuilder\Core\Models\FavoriteAction;
use WBuilder\Core\Models\Language;
use WBuilder\Core\Models\Meta;
use WBuilder\Core\Models\Order;
use WBuilder\Core\Models\OrderPaginate;
use WBuilder\Core\Models\Page;
use WBuilder\Core\Models\Paginate;
use WBuilder\Core\Models\Product;
use WBuilder\Core\Models\ProductColor;
use WBuilder\Core\Models\ProductCustomerReview;
use WBuilder\Core\Models\ProductFavorite;
use WBuilder\Core\Models\ProductPaginate;
use WBuilder\Core\Models\Service;
use WBuilder\Core\Models\State;
use WBuilder\Core\Models\Template;
use WBuilder\Core\Types\ArticleSortField;
use WBuilder\Core\Types\ListOfCategories;
use WBuilder\Core\Types\ListOfItems;
use WBuilder\Core\Types\ListOfMenu;
use WBuilder\Core\Types\ListOfProduct;
use WBuilder\Core\Types\ListOfProductColor;
use WBuilder\Core\Types\ListOfProductSize;
use WBuilder\Core\Types\ListOfService;
use WBuilder\Core\Types\ProductSortField;

class Builder extends AbstractBuilder
{
    public function getName()
    {
        return "Website Builder Gateway";
    }
    public function getDefaultParameters()
    {
        return array(
            'authorize_key' => config("website-builder.authorize_key") ?: "key",
            'mode' => config("website-builder.mode")?: "test",
            "preview_mode" => config("website-builder.preview_mode") ?: false,
            "endpoint" => config("website-builder.endpoint") ?: "",
            "microservices" => config("website-builder.microservices") ?: "",
        );
    }
    public function getPreviewMode()
    {
        return $this->getParameter('preview_mode');
    }

    public function setPreviewMode($value)
    {
        return $this->setParameter('preview_mode', $value);
    }
    public function getAuthorizeKey()
    {
        return $this->getParameter('authorize_key');
    }

    public function setAuthorizeKey($value)
    {
        return $this->setParameter('authorize_key', $value);
    }

    public function getEndpoint()
    {
        return $this->getParameter('endpoint');
    }

    public function setEndpoint($value)
    {
        return $this->setParameter('endpoint', $value);
    }
    public function getIsDraft()
    {
        return $this->getParameter('is_draft');
    }

    public function setIsDraft($value)
    {
        return $this->setParameter('is_draft', $value);
    }

    /**
     * Get meta details
     *
     * @param $meta_key //group name or meta_key for fetch data
     * @param $page // number of page
     * @param $limit // limit of data
     * @return Paginate
     */
    public function meta($meta_key, $page = 1, $limit = 10)
    {
        $method = '\WBuilder\Core\Messages\GetMetaDataRequest';
        $parameters = array(
            "meta_key" => $meta_key,
            'page' => $page,
            "limit" => $limit
        );
        return $this->default_response($this->callRequest($method, $parameters));
    }

    /**
     * Get articles by special meta_key or group
     *
     * @param $meta_key //group name or meta_key for fetch data
     * @param $page // number of page
     * @param $limit // limit of data
     * @param ?ArticleSortField $sort_field
     * @param string $sort_type
     * @return ArticlePaginate
     */
    public function articles($meta_key, $page = 1, $limit = 10, $sort_field = null, $sort_type = null)
    {
        $method = '\WBuilder\Core\Messages\GetArticlesDataRequest';
        $parameters = array(
            "meta_key" => $meta_key,
            'page' => $page,
            "limit" => $limit,
            'sort_field' => $sort_field,
            'sort_type' => $sort_type,
        );
        return $this->default_response($this->callRequest($method, $parameters));
    }
    protected function default_response($response){
        if($response) return $response;
        $std = new \stdClass();
        $std->data = new ListOfItems();
        $std->total = 0;
        $std->current_page = 0;
        $std->from = 0;
        $std->last_page = 0;
        $std->per_page = 10;
        $std->to = 0;
        $std->last_page = 0;
        return $std;
    }

    /**
     * Get products by special group
     *
     * @param $collection_name //group name for fetch data
     * @param $page // number of page
     * @param $limit // limit of data
     * @return ProductPaginate
     */
    public function productByGroup($collection_name, $page = 1, $limit = 10)
    {
        $method = '\WBuilder\Core\Messages\GetProductGroupDataRequest';
        $parameters = array(
            "collections" => [$collection_name],
            'page' => $page,
            "limit" => $limit
        );
        return $this->callRequest($method, $parameters);
    }

    /**
     * Get Template Details
     *
     * @return Template
     */
    public function template() : ?Template
    {
        return $this->template;
    }

    /**
     * Get Website Draft Details
     *
     * @return Draft
     */
    public function draft() : ?Draft
    {
        return $this->draft;
    }

    public function key($meta_key, $index = null){
        $find = $this->template->meta->search($meta_key, 'meta_key');
        if($find)
            return $find->data;

        return $find;
    }

    public function style($meta_key){
        if($this->draft){
            $find = $this->draft->meta_data->meta->search($meta_key, 'meta_key');
            if($find)
                return $find->style;
        }
        $find = $this->template->meta->search($meta_key, 'meta_key');
        if($find)
            return $find->style;

        return null;
    }

    public function meta_key($meta_key, $index = null){
        if($this->draft){
            $find = $this->draft->meta_data->meta->search($meta_key, 'meta_key');
            if($find)
                return $find;
        }
        $find = $this->template->meta->search($meta_key, 'meta_key');
        if($find)
            return $find;

        return $find;
    }

    /**
     * Translate text by default language
     *
     * @param string $value
     * @return string
     */
    public function translation($value){
        $language = (session()->has('language'))?session()->get('language'):$this->template->config['default_language'];
        if(key_exists($language, $value))
            return $value[$language];
        if(key_exists($this->template->config['default_language'], $value))
            return $value[$this->template->config['default_language']];
        return null;
    }

    /**
     * Get List Of Languages
     *
     * @return Language[]
     */
    public function languages()
    {
        $method = '\WBuilder\Core\Messages\GetLanguagesRequest';
        $parameters = array();
        return $this->callRequest($method, $parameters);

    }

    /**
     * Get List Of Currencies
     *
     * @return Currency[]
     */
    public function currencies()
    {
        $method = '\WBuilder\Core\Messages\GetCurrenciesRequest';
        $parameters = array();
        return $this->callRequest($method, $parameters);
    }

    /**
     * Get List Of Countries
     *
     * @return Country[]
     */
    public function countries()
    {
        $method = '\WBuilder\Core\Messages\GetCountriesRequest';
        $parameters = array();
        return $this->callRequest($method, $parameters);

    }

    /**
     * Get List Of States
     *
     * @return State[]
     */
    public function states($country_id = null)
    {
        $method = '\WBuilder\Core\Messages\GetStatesRequest';
        $parameters = array(
            "country_id" => $country_id
        );
        return $this->callRequest($method, $parameters);
    }

    /**
     * Get List Of Cities
     *
     * @return City[]
     */
    public function cities($state_id = null, $country_id = null)
    {
        $method = '\WBuilder\Core\Messages\GetCitiesRequest';
        $parameters = array(
            "country_id" => $country_id,
            "state_id" => $state_id,
        );
        return $this->callRequest($method, $parameters);
    }

    /**
     * Get Product Details By Id
     *
     * @param int $id
     * @return Product
     */
    public function product($id)
    {
        $method = '\WBuilder\Core\Messages\GetProductRequest';
        $parameters = array(
            'id' => $id
        );
        return $this->callRequest($method, $parameters);
    }

    /**
     * Get Product Details By Slug
     *
     * @param string $slug
     * @return Product
     */
    public function productBySlug($slug)
    {
        $method = '\WBuilder\Core\Messages\GetProductBySlugRequest';
        $parameters = array(
            'slug' => $slug
        );
        return $this->callRequest($method, $parameters);
    }

    /**
     * Get List Of categories
     *
     * @return ?ListOfCategories
     */
    public function categories()
    {
        $method = '\WBuilder\Core\Messages\GetListOfCategoriesRequest';
        $parameters = array();
        return $this->callRequest($method, $parameters);
    }

    /**
     * Get List Of product colors
     *
     * @return ?ListOfProductColor
     */
    public function colors()
    {
        $method = '\WBuilder\Core\Messages\GetListOfProductColorsRequest';
        $parameters = [
            'name' => 'color'
        ];
        return $this->callRequest($method, $parameters);
    }

    /**
     * Get List Of product sizes
     *
     * @return ?ListOfProductSize
     */
    public function sizes()
    {
        $method = '\WBuilder\Core\Messages\GetListOfProductSizesRequest';
        $parameters = [
            'name' => 'size'
        ];
        return $this->callRequest($method, $parameters);
    }

    /**
     * Search in products list
     * @param int $limit
     * @param int $page
     * @param string $keyword
     * @param array $categories
     * @param array $colors
     * @param array $sizes
     * @param ?ProductSortField $sort_field
     * @param string $sort_type
     * @return ProductPaginate
     * @throws Common\Exception\InvalidRequestException
     */
    public function products($limit, $page, $keyword = "", $categories = [], $colors = [], $sizes = [], $sort_field = null, $sort_type = null)
    {
        if(in_array("", $colors))
            $colors = [];
        if(in_array("", $sizes))
            $sizes = [];
        $options = array_merge($colors, $sizes);
        $method = '\WBuilder\Core\Messages\GetProductsDataRequest';
        $price = null;
        if(($this->httpRequest->get('pf') || $this->httpRequest->get('pf') == "0") && ($this->httpRequest->get('pt') || $this->httpRequest->get('pt') == "0")){
            $price = [
                "min" => $this->httpRequest->get('pf'),
                "max" => $this->httpRequest->get('pt'),
            ];
        }
        $parameters = array(
            'limit' => $limit,
            'page' => $page,
            'keyword' => $keyword,
            'categories' => (!in_array("", $categories))?$categories:[],
            "price" => $price,
            'option_values' => $options,
            'sort_field' => $sort_field,
            'sort_type' => $sort_type,
        );
        return $this->callRequest($method, $parameters);
    }

    /**
     * @param $email
     * @param $firstname
     * @param $lastname
     * @param $password
     * @param $gender
     * @param $phone_number
     * @return ?Customer
     */
    public function register($email, $firstname, $lastname, $password, $gender, $phone_number)
    {
        $method = '\WBuilder\Core\Messages\CustomerRegisterRequest';
        $parameters = array(
            'email' => $email,
            'firstname' => $firstname,
            'lastname' => $lastname,
            'password' => $password,
            'gender' => $gender,
            'phone_number' => $phone_number,
        );
        return $this->callRequest($method, $parameters);
    }

    /**
     * @param $email
     * @param $password
     * @return ?Customer
     */
    public function login($email, $password)
    {
        $method = '\WBuilder\Core\Messages\CustomerLoginRequest';
        $parameters = array(
            'username' => $email,
            'password' => $password,
            "grant_type" => "password",
            "scope" => ["*"],
            "client_id" => config("website-builder.client_id"),
            "client_secret" => config("website-builder.client_secret")
        );
        return $this->callRequest($method, $parameters);
    }

    /**
     * @param $token
     * @return ?Customer
     */
    public function retrieveByUserId($token)
    {
        $method = '\WBuilder\Core\Messages\GetUserByIdRequest';
        $parameters = array(
            'access_token' => $token
        );
        return $this->callRequest($method, $parameters);
    }

    /**
     * update customer information
     * @param $id
     * @param $firstname
     * @param $lastname
     * @param $gender
     * @param $phone_number
     * @return ?Customer
     */
    public function customerUpdate($id, $firstname, $lastname, $gender, $phone_number)
    {
        $method = '\WBuilder\Core\Messages\CustomerUpdateRequest';
        $parameters = array(
            'id' => $id,
            'firstname' => $firstname,
            'lastname' => $lastname,
            'gender' => $gender,
            'phone_number' => $phone_number,
        );
        return $this->callRequest($method, $parameters);
    }

    /**
     * submit review for product
     * @param $product_id
     * @param $customer_id
     * @param $name
     * @param $email
     * @param $title
     * @param $body
     * @param $rate
     * @param $parent_id
     * @return ?ProductCustomerReview
     */
    public function submitReview($product_id, $customer_id, $name, $email, $title, $body, $rate, $parent_id = null)
    {
        $method = '\WBuilder\Core\Messages\SubmitProductReviewRequest';
        $parameters = array(
            'product_id' => $product_id,
            'customer_id' => $customer_id,
            'parent_id' => $parent_id,
            'name' => $name,
            'email' => $email,
            'title' => $title,
            'body' => $body,
            'rate' => $rate,
        );
        return $this->callRequest($method, $parameters);
    }

    /**
     * Retrieve Address Information By Id
     *
     * @param $id
     * @return ?Address
     */
    public function retrieveByAddressId($id)
    {
        $method = '\WBuilder\Core\Messages\GetAddressByIdRequest';
        $parameters = array(
            'id' => $id
        );
        return $this->callRequest($method, $parameters);
    }

    /**
     * Create New Address For Customers
     *
     * @param $customer_id
     * @param $type
     * @param $firstname
     * @param $lastname
     * @param $email
     * @param $title
     * @param $country_id
     * @param $state_id
     * @param $city_id
     * @param $postal_code
     * @param $address
     * @return ?Address
     */
    public function createNewAddress($customer_id, $type, $firstname, $lastname, $email, $title, $country_id, $state_id, $city_id, $postal_code, $address)
    {
        $method = '\WBuilder\Core\Messages\CreateAddressRequest';
        $parameters = array(
            'customer_id' => $customer_id,
            'type' => $type,
            'firstname' => $firstname,
            'lastname' => $lastname,
            'email' => $email,
            'title' => $title,
            'country_id' => $country_id,
            'state_id' => $state_id,
            'city_id' => $city_id,
            'postal_code' => $postal_code,
            'address' => $address,
        );
        return $this->callRequest($method, $parameters);
    }

    /**
     * Update Address For Customers BY Id
     *
     * @param $id
     * @param $firstname
     * @param $lastname
     * @param $email
     * @param $title
     * @param $country_id
     * @param $state_id
     * @param $city_id
     * @param $postal_code
     * @param $address
     * @return ?Address
     */
    public function updateAddress($id, $firstname, $lastname, $email, $title, $country_id, $state_id, $city_id, $postal_code, $address)
    {
        $method = '\WBuilder\Core\Messages\UpdateAddressRequest';
        $parameters = array(
            'id' => $id,
            'firstname' => $firstname,
            'lastname' => $lastname,
            'email' => $email,
            'title' => $title,
            'country_id' => $country_id,
            'state_id' => $state_id,
            'city_id' => $city_id,
            'postal_code' => $postal_code,
            'address' => $address,
        );
        return $this->callRequest($method, $parameters);
    }

    /**
     * Delete Customer Address
     *
     * @param $id
     * @return ?array
     */
    public function deleteAddress($id)
    {
        $method = '\WBuilder\Core\Messages\DeleteAddressRequest';
        $parameters = array(
            'id' => $id
        );
        return $this->callRequest($method, $parameters);
    }

    /**
     * Get List Of Customer Orders
     *
     * @param $limit
     * @param $page
     * @param $customer_id
     * @return ?OrderPaginate
     */
    public function orders($limit, $page, $customer_id)
    {
        $method = '\WBuilder\Core\Messages\GetCustomerOrdersRequest';
        $parameters = array(
            'customer_id' => $customer_id,
            'limit' => $limit,
            'page' => $page,
        );
        return $this->callRequest($method, $parameters);
    }

    /**
     * Get List Of Customer Wisheslist
     *
     * @param $limit
     * @param $page
     * @param $customer_id
     * @return ?ProductPaginate
     */
    public function wishlist($limit, $page, $customer_id)
    {
        $method = '\WBuilder\Core\Messages\GetCustomerWishlistRequest';
        $parameters = array(
            'customer_id' => $customer_id,
            'limit' => $limit,
            'page' => $page,
        );
        return $this->callRequest($method, $parameters);
    }

    /**
     * Add to favorite products
     *
     * @param $customer_id
     * @param $product_id
     * @return ?FavoriteAction
     */
    public function favorite($product_id, $customer_id)
    {
        $method = '\WBuilder\Core\Messages\FavoriteRequest';
        $parameters = array(
            'customer_id' => $customer_id,
            'id' => $product_id
        );
        return $this->callRequest($method, $parameters);
    }

    /**
     * Check favorite products
     *
     * @param $customer_id
     * @param $product_id
     * @return ?ProductFavorite
     */
    public function checkFavorite($product_id, $customer_id)
    {
        $method = '\WBuilder\Core\Messages\CheckFavoriteRequest';
        $parameters = array(
            'customer_id' => $customer_id,
            'id' => $product_id
        );
        return $this->callRequest($method, $parameters);
    }

    /**
     * Get list of payment services
     *
     * @return ?ListOfService
     */
    public function payments()
    {
        $method = '\WBuilder\Core\Messages\GetPaymentServicesRequest';
        $parameters = array();
        return $this->callRequest($method, $parameters);
    }

    /**
     * Get list of delivery services
     *
     * @return ?ListOfService
     */
    public function deliveries()
    {
        $method = '\WBuilder\Core\Messages\GetDeliveryServicesRequest';
        $parameters = array();
        return $this->callRequest($method, $parameters);
    }

    /**
     * Get list of menu
     * @param $group
     * @return ?ListOfMenu
     */
    public function menu($group)
    {
        $method = '\WBuilder\Core\Messages\GetListOfMenuRequest';
        $parameters = array();
        return $this->callRequest($method, $parameters);
    }

    /**
     * Get Page Data By Slug
     *
     * @param string $slug
     * @return ?Page
     */
    public function page($slug)
    {
        $method = '\WBuilder\Core\Messages\GetPageRequest';
        $parameters = array(
            'slug' => $slug
        );
        return $this->callRequest($method, $parameters);
    }

    /**
     * Create order and get payment url
     * @param integer $customer_id
     * @param string $shipping_address
     * @param string $billing_address
     * @param integer $shipping_service_id
     * @param integer $shipping_sevice_rate_id
     * @param integer $currency_id
     * @param string $payment_callback_url
     * @param integer $payment_method
     * @return ?Order
     */
    public function createOrder(
        $customer_id,
        $shipping_address,
        $billing_address,
        $shipping_service_id,
        $shipping_sevice_rate_id,
        $currency_id,
        $payment_callback_url,
        $payment_method
    )
    {
        $method = '\WBuilder\Core\Messages\CreateOrderRequest';
        $parameters = array(
            "customer_id" => $customer_id,
            "shipping_address" => json_encode($shipping_address),
            "billing_address" => json_encode($billing_address),
            'shipping_service_id' => $shipping_service_id,
            'shipping_sevice_rate_id' => $shipping_sevice_rate_id,
            'payment_callback_url' => $payment_callback_url,
            'payment_method' => $payment_method,
            'cart' => $this->cart()->get()->toJson(),
            'cart_summary' => $this->cart()->summary()->toJson(),
            'currency_id' => $currency_id
        );
        return $this->callRequest($method, $parameters);
    }

    /**
     * Get Order Details By order id
     *
     * @param integer $customer_id
     * @param string $order_id
     * @return Article
     */
    public function orderByOrderId($customer_id, $order_id)
    {
        $method = '\WBuilder\Core\Messages\GetOrderDetailsByOrderIdRequest';
        $parameters = array(
            'customer_id' => $customer_id,
            'order_id' => $order_id
        );
        return $this->callRequest($method, $parameters);
    }

    /**
     * Get Article Details By Id
     *
     * @param int $id
     * @return Article
     */
    public function article($id)
    {
        $method = '\WBuilder\Core\Messages\GetArticleRequest';
        $parameters = array(
            'id' => $id
        );
        return $this->callRequest($method, $parameters);
    }

    /**
     * submit review for product
     * @param $post_id
     * @param $customer_id
     * @param $name
     * @param $email
     * @param $body
     * @return ?ArticleCustomerComment
     */
    public function submitArticleReview($post_id, $customer_id, $name, $email, $body)
    {
        $method = '\WBuilder\Core\Messages\SubmitArticleReviewRequest';
        $parameters = array(
            'id' => $post_id,
            'customer_id' => $customer_id,
            'name' => $name,
            'email' => $email,
            'body' => $body
        );
        return $this->callRequest($method, $parameters);
    }

    /**
     * Get Cart
     *
     * @return Cart
     */
    public function cart()
    {
        return $this->cart;
    }

    /**
     * Get default language
     *
     * @return Language
     */
    public function getDefaultLanguage()
    {
        if($this->draft())
            return $this->draft()->meta_data->languages[0];

        $languages = $this->languages();
        if(count($languages) > 0)
            return $languages[0];
        return null;
    }

    /**
     * Get default currency
     *
     * @return Currency
     */
    public function getDefaultCurrency()
    {
        if($this->draft())
            return $this->draft()->meta_data->currencies[0];

        $currencies = $this->currencies();
        if(count($currencies) > 0)
            return $currencies[0];
        return null;
    }

    /**
     * Get module data by name
     *
     * @return Module
     */
    public function module($name)
    {
        $method = '\WBuilder\Core\Messages\GetModuleRequest';
        $parameters = array(
            "name" => $name
        );
        return $this->callRequest($method, $parameters);
    }
}
