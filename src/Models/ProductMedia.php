<?php
namespace WBuilder\Core\Models;

class ProductMedia extends Model
{
    public $id;
    public $product_id;
    public $product_color_id;
    public $additional_data;
    public $created_at;
    public $updated_at;
    public $url;

    public function init($data){
        $this->url = $data['media']['file_url'];
        $this->additional_data['caption'] = $data['caption'];
    }

}
