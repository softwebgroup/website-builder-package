<?php
namespace WBuilder\Core\Models;

use WBuilder\Core\Types\Price;

class ProductPrice extends Model
{
    public $id;
    public $product_id;
    public Currency $currency;
    public float $price;
    public $discount_type;
    public ?float $discount;
    public ?float $tax;
    public ?Price $summary;
    public $sort;
    public $created_at;
    public $updated_at;

    public function init($data){
        $this->summary = $this->total_price($data);
    }


    protected function total_price($data) : Price{
        $result = array(
            "sub_total" => $this->price,
            "discount" => 0,
            "tax" => 0,
            "total_price" => $this->price
        );
        if($data['discount']){
            if($data['discount']['type'] == "amount"){
                $result['discount'] = $data['discount']['value'];
            }else{
                $result['discount'] = (($result['total_price'] * $data['discount']['value']) / 100);
            }
            $this->discount = $data['discount']['value'];
            $this->discount_type = $result['discount']['type'];
            $result['total_price'] = $result['sub_total'] - $this->discount;
        }else{
            $this->discount = 0;
            $this->discount_type = "amount";
        }
        $result['total_price'] = $result['total_price'] + $result['tax'];
        return new Price($result);
    }

}
