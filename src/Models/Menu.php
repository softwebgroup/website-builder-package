<?php
namespace WBuilder\Core\Models;

use WBuilder\Core\Types\ListOfMenu;

class Menu extends Model
{
    public $id;
    public $title;
    public ?ListOfMenu $children;
    public $sort;
    public $link_to;
    public $page_id;
    public ?Page $page;
    public $created_at;
    public $updated_at;

    public function init($data){
        $this->children =(key_exists("children", $data))?builder_list_of_data($data['children'], ListOfMenu::class, Menu::class):null;
        $this->link_to = (key_exists("url", $data))?$data['url']:null;
    }
}
