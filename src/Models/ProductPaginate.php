<?php
namespace WBuilder\Core\Models;
use WBuilder\Core\Types\ListOfArticles;
use WBuilder\Core\Types\ListOfProduct;

class ProductPaginate extends Model
{
    public $current_page;
    public $from;
    public $last_page;
    public $per_page;
    public $to;
    public $total;
    public ?ListOfProduct $data;
    public function init($data){
        if(array_key_exists('value', $data)){
            $this->current_page = $data['value']['meta']['current_page'];
            $this->from = $data['value']['meta']['from'];
            $this->last_page = $data['value']['meta']['last_page'];
            $this->per_page = $data['value']['meta']['per_page'];
            $this->to = $data['value']['meta']['to'];
            $this->total = $data['value']['meta']['total'];
            $this->data = builder_list_of_data($data['value']['data'], ListOfProduct::class, Product::class);
        }else{
            $this->current_page = $data['meta']['current_page'];
            $this->from = $data['meta']['from'];
            $this->last_page = $data['meta']['last_page'];
            $this->per_page = $data['meta']['per_page'];
            $this->to = $data['meta']['to'];
            $this->total = $data['meta']['total'];
            $this->data = builder_list_of_data($data['data'], ListOfProduct::class, Product::class);
        }

    }
}
