<?php
namespace WBuilder\Core\Models;

use WBuilder\Core\Types\ListOfArticleMedia;
use WBuilder\Core\Types\ListOfArticleCustomerComment;

class Article extends Model
{
    public $id;
    public $title;
    public $short_content;
    public $long_content;
    public $seo_keywords;
    public $comments_count;
    public $visit_count;
    public $additional_data;
    public $created_at;
    public $updated_at;
    public $last_visited_at;
    public $image;
    public ?ListOfArticleCustomerComment $comments;

    public function init($data){
        $this->short_content = array_key_exists('description', $data)?$data['description']:null;
        $this->long_content = array_key_exists('content', $data)?$data['content']:null;
        $this->additional_data = array_key_exists('data_additional', $data)?$data['data_additional']:null;
        $this->comments = builder_list_of_data($data['reviews'], ListOfArticleCustomerComment::class, ArticleCustomerComment::class);
    }

    /**
     * return article title as slug split by dash for url
     * @return string
     */
    public function slug(){
        return \Illuminate\Support\Str::slug($this->title, "-");
    }

    /**
     * return article detail url
     * @return string
     */
    public function url(){
        $params = ['id' => $this->id, 'title' => $this->slug()];
        return route('article', route_params($params));
    }
}
