<?php
namespace WBuilder\Core\Models;
use WBuilder\Core\Models\Customer;
use WBuilder\Core\Types\ListOfArticleCustomerComment;

class ArticleCustomerComment extends Model
{
    public $id;
    public $name;
    public $email;
    public $avatar;
    public $user_id;
    public $body;
    public $score;
    public ?ListOfArticleCustomerComment $replies;
    public $created_at;
    public $updated_at;

    public function init($data){
        $this->body = $data['comment'];
        if($data['user']){
            $this->name = $data['user']['name'];
            $this->email = $data['user']['email'];
            $this->avatar = $data['user']['avatar_url'];
        }
    }

}

