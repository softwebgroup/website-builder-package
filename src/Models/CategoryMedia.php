<?php
namespace WBuilder\Core\Models;

use WBuilder\Core\Types\ListOfCategories;

class CategoryMedia extends Model
{
    public $id;
    public $url;
    public $path;
    public $image;
    public $caption;
    public $additional_data;
    public $created_at;
    public $updated_at;

    public function init($data){
        $this->url = $data['media']['file_url'];
        $this->path = $data['media']['file_url'];
        $this->image = $data['media']['file_url'];
        $this->created_at = $data['media']['created_at'];
        $this->updated_at = $data['media']['updated_at'];
    }
}
