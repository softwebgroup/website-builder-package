<?php
namespace WBuilder\Core\Models;
use WBuilder\Core\Types\ListOfArticles;

class ArticlePaginate extends Model
{
    public $current_page;
    public $from;
    public $last_page;
    public $per_page;
    public $to;
    public $total;
    public ?ListOfArticles $data;
    public function init($data){
        if(array_key_exists('meta', $data['value'])){
            $this->current_page = $data['value']['meta']['current_page'];
            $this->from = $data['value']['meta']['from'];
            $this->last_page = $data['value']['meta']['last_page'];
            $this->per_page = $data['value']['meta']['per_page'];
            $this->to = $data['value']['meta']['to'];
            $this->total = $data['value']['meta']['total'];
            $this->data = builder_list_of_data($data['value']['data'], ListOfArticles::class, Article::class);
        }else{
            $this->current_page = 1;
            $this->from = 1;
            $this->last_page = 1;
            $this->per_page = count($data['value']);
            $this->to = 1;
            $this->total = count($data['value']);
            $this->data = builder_list_of_data($data['value'], ListOfArticles::class, Article::class);
        }

    }
}
