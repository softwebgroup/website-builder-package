<?php
namespace WBuilder\Core\Models;

use Illuminate\Support\Facades\Auth;
use WBuilder\Core\Builder;
use WBuilder\Core\Types\ListOfProductMedia;
use WBuilder\Core\Types\ListOfProductSize;
use WBuilder\Core\Types\ListOfProductPrice;
use WBuilder\Core\Types\ListOfProductColor;
use WBuilder\Core\Types\ListOfCategories;
use WBuilder\Core\Types\ListOfProductCustomerReview;

class Product extends Model
{
    public $id;
    public $code;
    public $sku;
    public $stock;
    public $slug;
    public $rating;
    public $ratings;
    public $status;
    public $additional_data;
    public $created_at;
    public $updated_at;
    public $title;
    public $short_content;
    public $long_content;
    public $seo_keywords;
    public ?ProductPrice $price;
    public $image;
    public ?ListOfProductMedia $media;
    public ?ListOfProductPrice $prices;
    public ?ListOfCategories $categories;
    public ?ListOfProductSize $sizes;
    public ?ListOfProductColor $colors;
    public ?ListOfProductCustomerReview $reviews;


    public function init($data){
        $this->code = $data['slug'];
        $this->short_content = ($data['short_content'])?$data['short_content']:$data['content'];
        $this->long_content = $data['content'];
        $this->prices = builder_list_of_data($data['prices'], ListOfProductPrice::class, ProductPrice::class, ['product_id' => $this->id]);
        $this->media = builder_list_of_data($data['images'], ListOfProductMedia::class, ProductMedia::class, ['product_id' => $this->id]);
        $this->image = ($data['image'] && $data['image']['media'])?$data['image']['media']['file_url']:null;
        $colors = [];
        $sizes = [];
        $color_option_id = 0;
        $size_option_id = 0;
        if($data['options']){
            foreach ($data['options'] as $option){
                if(strtolower($option['name']) == "color")
                    $color_option_id = $option['id'];
                if(strtolower($option['name']) == "size")
                    $size_option_id = $option['id'];
            }
            foreach ($data['variants'] as $variant){
                foreach ($variant['values'] as $variant_value){
                    if($variant_value['option_id'] == $size_option_id)
                        $sizes[] = $variant;
                    if($variant_value['option_id'] == $color_option_id)
                        $colors[] = $variant;
                }
            }
        }
        $this->sizes = builder_list_of_data($sizes, ListOfProductSize::class, ProductSize::class, ['product_id' => $this->id, 'option_id' => $size_option_id]);
        $this->colors = builder_list_of_data($colors, ListOfProductColor::class, ProductColor::class, ['product_id' => $this->id, 'option_id' => $color_option_id]);
    }

    /**
     * check $value in categories title
     * @param $value keyword for check name of category
     * @return |null
     */
    public function has_category($value){
        $result = $this->categories->search($value, 'code');
        if($result)
            return $result->name;
        return null;
    }

    /**
     * return list of category name as string split by comma
     * @return string
     */
    public function categories(){
        $categories = [];
        /** @var Category $category */
        foreach ($this->categories as $category){
            $categories[] = $category->name;
        }
        return implode(", ", $categories);
    }

    /**
     * return product title as slug split by dash for url
     * @return string
     */
    public function slug(){
        return $this->slug;
    }

    public function is_favorite(){
        if(!Auth::check()) return false;
        try{
            if($favorite = \builder()->checkFavorite($this->id, Auth::user()->getAuthIdentifier())){
                return $favorite->id != null;
            }

        }catch (\Exception $exception){

        }
        return false;
    }

    /**
     * return product detail url
     * @return string
     */
    public function url($color_id = null, $size_id = null){
        $params = ['pid' => $this->id, 'title' => $this->slug()];
        if($color_id) $params['cid'] = $color_id;
        if($size_id) $params['sid'] = $size_id;
        return route('product', route_params($params));
    }


    function toJson() {
        return json_encode(array(
            "id" => $this->id,
            "code" => $this->code,
            "stock" => $this->stock,
            "rating" => $this->rating,
            "ratings" => $this->ratings,
            "status" => $this->status,
            "additional_data" => $this->additional_data,
            "created_at" => $this->created_at,
            "updated_at" => $this->updated_at,
            "title" => $this->title,
            "short_content" => $this->short_content,
            "long_content" => $this->long_content,
            "seo_keywords" => $this->seo_keywords,
            "price" => ($this->price)?$this->price->toJson():null,
            "image" => $this->image,
            "media" => ($this->media)?$this->media->toJson():null,
            "prices" => ($this->prices)?$this->prices->toJson():null,
            "categories" => ($this->categories)?$this->categories->toJson():null,
            "sizes" => ($this->sizes)?$this->sizes->toJson():null,
            "colors" => ($this->colors)?$this->colors->toJson():null,
            "reviews" => ($this->reviews)?$this->reviews->toJson():null
        ));
    }
}
