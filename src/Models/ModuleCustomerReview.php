<?php
namespace WBuilder\Core\Models;
use WBuilder\Core\Models\Customer;

class ModuleCustomerReview extends Model
{
    public $id;
    public $name;
    public $email;
    public ?Customer $customer;
    public ?ModuleCustomerReview $parent;
    public $title;
    public $body;
    public $rate;
    public $created_at;
    public $updated_at;


}

