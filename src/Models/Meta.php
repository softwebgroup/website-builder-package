<?php
namespace WBuilder\Core\Models;

use WBuilder\Core\Builder;
use WBuilder\Core\Types\ListOfArticles;
use WBuilder\Core\Types\ListOfCategories;
use WBuilder\Core\Types\ListOfMenu;
use WBuilder\Core\Types\ListOfPages;
use WBuilder\Core\Types\ListOfProduct;
use WBuilder\Core\Types\ListOfService;
use WBuilder\Core\Types\ListOfSocial;

class Meta extends Model
{
    public $id;
    public ?TemplateGroup $group;
    public $order;
    public $title;
    public $meta_key;
    public $meta_type;
    public $conditions;
    public $created_at;
    public $updated_at;
    public $data;
    public $style;

    public function init($data){
        $this->meta_type = $data['type'];
        $this->meta_key = $data['key'];
        if($this->meta_type == 'post')
            $this->data = builder_list_of_data($data['value'], ListOfArticles::class, Article::class);
        elseif ($this->meta_type == 'product')
            $this->data = builder_list_of_data($data['value'], ListOfProduct::class, Product::class);
        elseif ($this->meta_type == 'form')
            $this->data = $data['form'];
        elseif ($this->meta_type == 'text' || $this->meta_type == 'textarea'){
            $this->data = $data['value'];

        }
        elseif($this->meta_type == 'tags')
            $this->data = $data['value'];
        elseif ($this->meta_type == 'image')
            $this->data = $data['value'];
        elseif ($this->meta_type == 'image_group'){
            
        }

        else
            $this->data = $data['value'];
    }

}

