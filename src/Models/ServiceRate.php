<?php
namespace WBuilder\Core\Models;

class ServiceRate extends Model
{
    public $id;
    public $title;
    public $price;
    public $name;
    public Currency $currency;
    public $sort;
    public $created_at;
    public $updated_at;


}
