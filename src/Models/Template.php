<?php
namespace WBuilder\Core\Models;


use WBuilder\Core\Types\ListOfMeta;

class Template extends Model
{
    public $id;
    public $key;
    public $name;
    public $image;
    public $status;
    public ?Language $language;
    public ?Currency $currency;
    /** @var Language[] $languages */
    public $languages;
    /** @var Currency[] $currencies */
    public $currencies;
    /** @var ListOfMeta[] $meta */
    public $meta;
    public $images;


    public function init($data){
        if($data && key_exists("languages", $data) && $data['languages']){
            $list = [];
            foreach ($data['languages'] as $item)
                $list[] = new Language($item);
            $this->languages = $list;
        }
        if($data && key_exists("currencies", $data) && $data['currencies']){
            $list = [];
            foreach ($data['currencies'] as $item)
                $list[] = new Currency($item);
            $this->currencies = $list;
        }
        $this->image = ($data['image'] && $data['image']['image'])?$data['image']['image']['file_url']:null;
        $this->key = $data['app_id'];
        $this->meta = builder_list_of_data($data['meta'], ListOfMeta::class, Meta::class);

    }

}
