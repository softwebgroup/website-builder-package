<?php
namespace WBuilder\Core\Models;

use WBuilder\Core\Types\ListOfMeta;

class Section extends Model
{
    public $id;
    public $title;
    public $description;
    public $path;
    public $meta_elements;
    public $created_at;
    public $updated_at;

    public function init($data){

    }
}
