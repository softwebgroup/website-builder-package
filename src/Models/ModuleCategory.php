<?php
namespace WBuilder\Core\Models;

use WBuilder\Core\Types\ListOfArticleMedia;
use WBuilder\Core\Types\ListOfArticleCustomerComment;

class ModuleCategory extends Model
{
    public $id;
    public ?ModuleCategory $parent;
    public $title;
    public $description;
    public $keywords;
    public $images;
    public $image;
    public $status;
    public $created_at;
    public $updated_at;
    public function init($data){
        $this->image = $data['images']?$data['images'][0]:null;
    }
}
