<?php
namespace WBuilder\Core\Models;

class ProductColor extends Model
{
    public $id;
    public $option_id;
    public $product_id;
    public $color;
    public $title;
    public $stock;
    public $order;
    public $created_at;
    public $updated_at;

    public function init($data, $index){
        if(array_key_exists('value', $data)){
            $this->title = $data['value'];
            $this->order = $index;
            $this->color = ($data['additional_data'] && array_key_exists('color', $data['additional_data']))?$data['additional_data']['color']:null;
        }else{
            $this->title = $data['name'];
            $this->order = $index;
            foreach ($data['values'] as $value){
                if($value['option_id'] == $this->option_id){
                    $this->color = ($value['additional_data'] && array_key_exists('color', $value['additional_data']))?$value['additional_data']['color']:null;
                }
            }
        }
    }

}
