<?php
namespace WBuilder\Core\Models;
use WBuilder\Core\Types\ListOfArticles;

class Paginate extends Model
{
    public $current_page;
    public $from;
    public $last_page;
    public $per_page;
    public $to;
    public $total;
    public $data;
    public function init($data){

        $this->current_page = 1;
        $this->from = 1;
        $this->last_page = 1;
        $this->per_page = count($data['value']);
        $this->to = 1;
        $this->total = count($data['value']);
        $this->data = $data['value'];
    }

}
