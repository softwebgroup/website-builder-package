<?php
namespace WBuilder\Core\Models;

class OrderCartItem extends Model
{
    public $id;
    public ?Product $product;
    public ?ProductColor $color;
    public ?ProductSize $size;
    public $quantity;
    public $price;
    public $total_price;
    public $status;
    public $created_at;
    public $updated_at;

}
