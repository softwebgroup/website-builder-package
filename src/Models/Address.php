<?php
namespace WBuilder\Core\Models;


class Address extends Model
{
    public $id;
    public ?Country $country;
    public ?City $city;
    public ?State $state;
    public $type;
    public $title;
    public $firstname;
    public $lastname;
    public $email;
    public $address;
    public $postal_code;
    public $created_at;
    public $updated_at;


    public function fullAddress(){
        $address = [];
        if($this->country) $address[] = $this->country->name;
        if($this->city) $address[] = $this->city->name;
        if($this->state) $address[] = $this->state->name;
        $address[] = $this->address;
        return implode(", ", $address);
    }
    public function fullName(){
        return $this->firstname." ".$this->lastname;
    }
}
