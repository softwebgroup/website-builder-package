<?php
namespace WBuilder\Core\Models;

use WBuilder\Core\Types\ListOfAddress;
class Customer extends AuthModel
{
    public $id;
    public $app_id;
    public $email;
    public $firstname;
    public $lastname;
    public $full_name;
    public $gender;
    public $phone_number;
    public $avatar;
    public $favorite_count;
    public ?ListOfAddress $billing_addresses;
    public ?ListOfAddress $shipping_addresses;
    public $access_token;


    public function init($data){
        $this->avatar = $data['avatar_url'];
        $this->phone_number = $data['msisdn'];
    }

}
