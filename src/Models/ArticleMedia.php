<?php
namespace WBuilder\Core\Models;


class ArticleMedia extends Model
{
    public $id;
    public $url;
    public $path;
    public $additional_data;
    public $created_at;
    public $updated_at;

    public function init($data){
        $this->url = builder_resource_url($data['url']);

    }

}
