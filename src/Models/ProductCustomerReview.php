<?php
namespace WBuilder\Core\Models;
use WBuilder\Core\Models\Customer;

class ProductCustomerReview extends Model
{
    public $id;
    public $name;
    public $email;
    public ?Customer $customer;
    public ?ProductCustomerReview $parent;
    public $title;
    public $body;
    public $rate;
    public $created_at;
    public $updated_at;


}

