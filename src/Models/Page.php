<?php
namespace WBuilder\Core\Models;

use WBuilder\Core\Types\ListOfMenu;
use WBuilder\Core\Types\ListOfSections;

class Page extends Model
{
    public $id;
    public $type;
    public $slug;
    public $is_default;
    public $title;
    public $seo_keywords;
    public $seo_title;
    public $seo_description;
    public $accessibility;
    public $path;
    public $status;
    public ?ListOfSections $sections;
    public $created_at;
    public $updated_at;

    public function init($data){
        
    }
}
