<?php
namespace WBuilder\Core\Models;

class Provider extends Model
{
    public $id;
    public $code;
    public $type;
    public $image;
    public $label;
    public $conditions;
    public $additional_data;
    public $created_at;
    public $updated_at;

    public function init($data){

    }
}
