<?php
namespace WBuilder\Core\Models;

use WBuilder\Core\Types\ListOfModuleCategory;
use WBuilder\Core\Types\ListOfModuleCustomerReview;

class Module extends Model
{
    public $id;
    public $name;
    public $slug_prefix;
    public $icon;
    public $price;
    public $title;
    public $description;
    public $content;
    public $keywords;
    public $data_additional;
    public $images;
    public $image;
    public ?ListOfModuleCustomerReview $reviews;
    public ?ListOfModuleCategory $categories;
    public $status;
    public $created_at;
    public $updated_at;
    public function init($data){
        $this->image = $data['images']?$data['images'][0]:null;
        $this->data_additional = ($data['data_additional'])??[];
    }

}
