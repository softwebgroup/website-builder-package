<?php
namespace WBuilder\Core\Models;

class ProductSize extends Model
{
    public $id;
    public $option_id;
    public $product_id;
    public $size;
    public $stock;
    public $order;
    public $created_at;
    public $updated_at;

    public function init($data, $index){

        if(array_key_exists('value', $data)){
            $this->size = $data['value'];
            $this->order = $index;
        }else{
            $this->size = $data['name'];
            $this->order = $index;
        }
    }
}
