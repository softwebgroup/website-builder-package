<?php
namespace WBuilder\Core\Models;

use WBuilder\Core\Types\ListOfCategories;
use WBuilder\Core\Types\ListOfCategoryMedia;

class Category extends Model
{
    public $id;
    public $category_id;
    public $code;
    public $title;
    public $description;
    public ?ListOfCategories $children;
    public ?ListOfCategoryMedia $media;
    public $sort;
    public $image;
    public $status;
    public $created_at;
    public $updated_at;

    public function init($data){
        $this->category_id = $data['id'];
        $this->code = $data['slug'];
        $this->title = $data['name'];
        $this->children = builder_list_of_data($data['children'], ListOfCategories::class, Category::class);
        $this->media = builder_list_of_data($data['images'], ListOfCategoryMedia::class, CategoryMedia::class);
        if($data['image'])
            $this->image = new CategoryMedia($data['image']);
    }
}
