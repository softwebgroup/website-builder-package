<?php


namespace WBuilder\Core\Traits;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

trait ProductController
{
    public function product($title, $pid, Request $request){
        $product = builder()->product($pid);
        if(!$product) abort(404);
        $color = ($cid = $request->get("cid"))?(($product->colors->search($cid))?:abort(404)):null;
        $size = ($sid = $request->get("sid"))?(($product->sizes->search($sid))?:abort(404)):null;

        return wbuilder_view('product-detail', compact('product', 'color', 'size'));
    }

    public function favorite(Request $request){
        if(!Auth::check()) abort(401);
        $favorite = builder()->favorite($request->get('product_id'), Auth::user()->getAuthIdentifier());
        if($request->get('json'))
            return response()->json($favorite);

        return back();
    }

    public function submitReview(Request $request){
        $customer_id = (Auth::check())?Auth::user()->getAuthIdentifier():null;
        try{
            $review = builder()->submitReview(
                $request->get('product_id'),
                $customer_id,
                $request->get('name'),
                $request->get('email'),
                $request->get('title'),
                $request->get('body'),
                $request->get('rate'),
                $request->get('parent_id'),
            );
        }catch (\Exception $exception){
            return response()->json(json_decode($exception->getMessage()), 400);
        }
        if($request->get('json'))
            return response()->json($review);

        return back();
    }
}
