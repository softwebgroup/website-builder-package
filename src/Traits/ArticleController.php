<?php


namespace WBuilder\Core\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

trait ArticleController
{
    public function articles(Request $request, $meta){
        $meta_info = builder()->meta_key($meta);
        if(!$meta_info) abort(404);

        $per_page = 12;
        $page = ($request->get('page'))?:1;
        $articles = builder()->articles(
                $meta,
                $page,
                $per_page
            );
        return wbuilder_view('articles', compact('meta_info', 'per_page', 'articles'));
    }

    public function article($id, $title){
        $post = builder()->article($id);
        if(!$post) abort(404);

        return wbuilder_view('article-detail', compact('post'));
    }

    public function submitReview(Request $request){
        $customer_id = (Auth::check())?Auth::user()->getAuthIdentifier():null;
        try{
            $review = builder()->submitArticleReview(
                $request->get('post_id'),
                $customer_id,
                $request->get('name'),
                $request->get('email'),
                $request->get('body')
            );
        }catch (\Exception $exception){
            return response()->json(json_decode($exception->getMessage()), 400);
        }
        if($request->get('json'))
            return response()->json($review);

        return back();
    }
}
