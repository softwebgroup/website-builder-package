<?php


namespace WBuilder\Core\Traits;


use Illuminate\Http\Request;
use WBuilder\Core\Builder;

trait HomeController
{
    public function index(Builder $builder){
        \builder()->menu("Header");
        return wbuilder_view('index');
    }
    public function page($slug, Request $request){
        try {
            $page = \builder()->page($slug);
            if($page)
                return wbuilder_view('page', compact('page'));
            $product = \builder()->productBySlug($slug);
            if($product) {
                $color = ($cid = $request->get("cid"))?(($product->colors->search($cid))?:abort(404)):null;
                $size = ($sid = $request->get("sid"))?(($product->sizes->search($sid))?:abort(404)):null;
                return wbuilder_view('product-detail', compact('product', 'color', 'size'));
            }

        }catch (\Exception $exception){

        }
        abort(404);
    }
    public function blank($data){
        try {
            $page = json_decode(urldecode($data));
            $page->title = $page->translations[0]->title;
            $page->content = $page->translations[0]->content;
            return wbuilder_view('page', compact('page'));
        }catch (\Exception $exception){
            abort(404);
        }
    }
}
