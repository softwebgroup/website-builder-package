<?php


namespace WBuilder\Core\Traits;


use Illuminate\Http\Request;
use WBuilder\Core\Models\Product;
use WBuilder\Core\Models\ProductColor;
use WBuilder\Core\Models\ProductSize;
use WBuilder\Core\Types\ListOfProductColor;
use WBuilder\Core\Types\ListOfProductSize;

trait AjaxController
{
    public function states(Request $request){
        $states = builder()->states($request->get('country_id'));
        return response()->json($states, 200);
    }
    public function cities(Request $request){
        $cities = builder()->cities($request->get('state_id'));
        return response()->json($cities, 200);
    }

    public function productDetails(Request $request){
        $product = builder()->product($request->get('pid'));
        if(!$product) abort(404);

        return wbuilder_view('ajax/'.$request->get('v'), compact('product'));
    }


    public function generateDataInTemplate(Request $request){
        $element_id = \Illuminate\Support\Str::uuid();

        $parameters = array(
            'data' => $request->get('data'),
            'template' => $request->get('template'),
            'element_id' => $element_id
        );
        if($request->get('param')){
            $parameters = array_merge($parameters, $request->get('param'));

            if(key_exists("type", $request->get('param'))){
                $type = $request->get('param')['type'];
                $item_type = null;
                $items_type = null;
                if($type == "article"){
                    $items_type = \WBuilder\Core\Types\ListOfArticles::class;
                    $item_type = \WBuilder\Core\Models\Article::class;
                }else if($type == "product"){
                    $items_type = \WBuilder\Core\Types\ListOfProduct::class;
                    $item_type = \WBuilder\Core\Models\Product::class;
                }

                $parameters['item_variable'] = $type;
                $parameters['items_type'] = $items_type;
                $parameters['item_type'] = $item_type;
            }

        }
        return response()->json(array(
            "html" => wbuilder_view("{$request->get('template')}", $parameters)->render(),
            "count" => 1
        ));
    }
}
