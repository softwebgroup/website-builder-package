<?php


namespace WBuilder\Core\Traits;


use Illuminate\Http\Request;

trait ShopController
{
    public function shop(Request $request){
        $per_page = 12;
        $page = ($request->get('page'))?:1;
        $products = builder()
            ->products(
                $per_page,
                $page,
                $request->get('keyword'),
                explode(",", $request->get('c')),
                explode(",", $request->get('co')),
                explode(",", $request->get('s')),
                $request->get('sort_by'),
                $request->get('sort_dir')
            );
        return wbuilder_view('shop', compact('products', 'per_page'));
    }
}
