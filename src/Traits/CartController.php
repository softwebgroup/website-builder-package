<?php


namespace WBuilder\Core\Traits;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use WBuilder\Core\Common\Http\Exception;

trait CartController
{
    protected function shippingAddressValidator(Request $request){
        return Validator::make($request->all(), [
            'shipping_address_id' => '',
            'firstname' => 'required_if:shipping_address_id,-1|max:255',
            'lastname' => 'required_if:shipping_address_id,-1|max:255',
            'email' => 'required_if:shipping_address_id,-1|max:255',
            'title' => 'required_if:shipping_address_id,-1|max:255',
            'country_id' => 'required_if:shipping_address_id,-1',
            'city_id' => 'required_if:shipping_address_id,-1',
            'state_id' => 'required_if:shipping_address_id,-1',
            'postal_code' => 'required_if:shipping_address_id,-1',
            'address' => 'required_if:shipping_address_id,-1',
        ]);
    }
    protected function billingAddressValidator(Request $request){
        return Validator::make($request->all(), [
            'shipping_address_id' => '',
            'firstname' => 'required_if:billing_address_id,-1|max:255',
            'lastname' => 'required_if:billing_address_id,-1|max:255',
            'email' => 'required_if:billing_address_id,-1|max:255',
            'title' => 'required_if:billing_address_id,-1|max:255',
            'country_id' => 'required_if:billing_address_id,-1',
            'city_id' => 'required_if:billing_address_id,-1',
            'state_id' => 'required_if:billing_address_id,-1',
            'postal_code' => 'required_if:billing_address_id,-1',
            'address' => 'required_if:billing_address_id,-1',
        ]);
    }
    protected function getCallbackUrl(){
        return url("/cart/payment/callback");
    }
    protected function redirectPayment($order){
        builder()->cart()->flush();
        Session::remove('shipping-address');
        Session::remove('billing-address');
        Session::remove('shipping-method');

        return Redirect::away($order->payment_config->redirect_url);
    }
    public function checkout(Request $request){
        $step = ($request->get('step'))?$request->get('step'):"shipping-address";
        $countries = builder()->countries();
        $states = null;
        $cities = null;
        $address = null;
        $data = $request->session()->get($step);
        if($data){
            $address = ((object)$data);
            if(property_exists($address, 'country_id'))
                $states = builder()->states($address->country_id);
            if(property_exists($address, 'state_id'))
                $cities = builder()->cities($address->state_id);

        }
        if($request->method() == "POST") {
            if($step == "shipping-address"){
                $states = builder()->states($request->get('country_id'));
                $cities = builder()->cities($request->get('state_id'));
                $validator = $this->shippingAddressValidator($request);
                if ($validator->fails())
                    return back()->withErrors($validator)->withInput()
                        ->with('step', 'shipping-address')
                        ->with('error', true)
                        ->with("states", $states)
                        ->with("cities", $cities);

                $request->session()->put($step, $request->all());
                $step = "billing-address";
                return redirect(route('checkout', route_params(['step' => 'billing-address'])))->with('step', $step)
                    ->with("states", $states)
                    ->with("cities", $cities);
            }
            if($step == "billing-address"){
                $states = builder()->states($request->get('country_id'));
                $cities = builder()->cities($request->get('state_id'));
                $validator = $this->billingAddressValidator($request);
                if ($validator->fails())
                    return back()->withErrors($validator)->withInput()
                        ->with('step', 'billing-address')
                        ->with('error', true)
                        ->with("states", $states)
                        ->with("cities", $cities);

                $request->session()->put($step, $request->all());
                $step = "shipping-method";
                return redirect(route('checkout', route_params(['step' => 'shipping-method'])))->with('step', $step)
                    ->with("states", $states)
                    ->with("cities", $cities);
            }
            if($step == "shipping-method"){
                $validator = Validator::make($request->all(), [
                    'shipping_method' => 'required',
                    'shipping_fee' => 'required'
                ]);
                if ($validator->fails())
                    return back()->withErrors($validator)->withInput()
                        ->with('step', 'shipping-method')
                        ->with('error', true);
                $request->session()->put($step, $request->all());

                $rate = null;
                foreach (builder()->deliveries()->all() as $service){
                    $service_rate = $service->rates->search($request->get('shipping_fee'));
                    if($service_rate)
                        $rate = $service_rate;
                }
                builder()->cart()->setShippingFee($rate);

                $step = "payment";
                return redirect(route('checkout', route_params(['step' => 'payment'])))->with('step', $step);
            }
            if($step == "payment"){
                $validator = Validator::make($request->all(), [
                    'payment_method' => 'required'
                ]);
                if ($validator->fails())
                    return back()->withErrors($validator)->withInput()
                        ->with('step', 'payment')
                        ->with('error', true);
                $shippingAddress = $request->session()->get('shipping-address');
                $billingAddress = $request->session()->get('billing-address');
                $shippingMethod = $request->session()->get('shipping-method');
                try{
                    $order = builder()->createOrder(
                        Auth::user()->getAuthIdentifier(),
                        $shippingAddress,
                        $billingAddress,
                        $shippingMethod['shipping_method'],
                        $shippingMethod['shipping_fee'],
                        builder()->getDefaultCurrency()->id,
                        $this->getCallbackUrl(),
                        $request->get('payment_method')
                    );
                    return $this->redirectPayment($order);
                }catch (\Exception $exception){
                    return back()->withErrors(array("payment_status" => $exception->getMessage()))->withInput()
                        ->with('step', 'payment')
                        ->with('error', true);
                }
            }


        }
        return wbuilder_view('checkout/index', array(
            'step' => $step,
            'countries' => $countries,
            'states' => $states,
            'cities' => $cities,
            'address_info' => $address
        ));
    }

    public function cart(){
        return wbuilder_view('cart');
    }
    public function addToCart(Request $request){
        $cart = builder()->cart()->add($request->get('pid'), $request->get('cid'), $request->get('quantity'));
        if($request->get('json'))
            return response()->json($cart);

        return redirect()->back()->with('cart_added_item', true);
    }
    public function deleteCartItem(Request $request){
        $cart = builder()->cart()->remove($request->get('id'));
        if($request->get('json'))
            return response()->json($cart);

        return redirect()->back()->with('cart_removed_item', true);
    }

    public function paymentCallback(Request $request){
        try{
            $order = builder()->orderByOrderId(Auth::user()->id, $request->get('oid'));
            if(!$order) abort(404);
            if($order->status['code'] == 1){
                return wbuilder_view('checkout/index', array(
                    'step' => "pay-success",
                    'order' => $order
                ));
            }
            return wbuilder_view('checkout/index', array(
                'step' => "pay-failed",
                'message' => $request->get('message'),
                'order' => $order
            ));

        }catch (Exception $exception){

        }
        abort(404);
    }
}
