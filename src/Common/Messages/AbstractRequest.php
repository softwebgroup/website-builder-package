<?php
namespace WBuilder\Core\Common\Messages;

use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request as HttpRequest;
use WBuilder\Core\Common\Exception\InvalidRequestException;
use WBuilder\Core\Common\Exception\RuntimeException;
use WBuilder\Core\Common\Helper;
use WBuilder\Core\Common\Http\ClientInterface;
use WBuilder\Core\Common\ParametersTrait;
use WBuilder\Core\Models\Draft;
use WBuilder\Core\Models\Template;
use WBuilder\Core\Models\TemplateGroup;

abstract class AbstractRequest implements RequestInterface
{
    use ParametersTrait {
        setParameter as traitSetParameter;
        getParameter as traitGetParameter;
    }

    /**
     * The request client.
     *
     * @var ClientInterface
     */
    protected $httpClient;

    /**
     * The HTTP request object.
     *
     * @var \Symfony\Component\HttpFoundation\Request
     */
    protected $httpRequest;

    /**
     * An associated ResponseInterface.
     *
     * @var ResponseInterface
     */
    protected $response;

    /**
     *
     * @var Template
     */
    protected ?Template $template;

    /**
     *
     * @var Draft
     */
    protected ?Draft $draft;

    /**
     * Create a new Request
     *
     * @param ClientInterface $httpClient  A HTTP client to make API calls with
     * @param HttpRequest $httpRequest A Symfony HTTP request object
     * @param Template $template
     * @param Draft $draft
     */
    public function __construct(ClientInterface $httpClient, HttpRequest $httpRequest, ?Template $template, ?Draft $draft)
    {
        $this->httpClient = $httpClient;
        $this->httpRequest = $httpRequest;
        $this->template = $template;
        $this->draft = $draft;
        $this->initialize();

    }

    /**
     * Initialize the object with parameters.
     *
     * If any unknown parameters passed, they will be ignored.
     *
     * @param array $parameters An associative array of parameters
     *
     * @return $this
     * @throws RuntimeException
     */
    public function initialize(array $parameters = array())
    {
        if (null !== $this->response) {
            throw new RuntimeException('Request cannot be modified after it has been sent!');
        }
        $this->parameters = new ParameterBag;

        Helper::initialize($this, $parameters);

        return $this;
    }

    /**
     * Set a single parameter
     *
     * @param string $key The parameter key
     * @param mixed $value The value to set
     * @return $this
     * @throws RuntimeException if a request parameter is modified after the request has been sent.
     */
    protected function setParameter($key, $value)
    {
        if (null !== $this->response) {
            throw new RuntimeException('Request cannot be modified after it has been sent!');
        }

        return $this->traitSetParameter($key, $value);
    }

    protected function getParameter($key)
    {
        return $this->traitGetParameter($key);
    }

    /**
     * Send the request
     *
     * @return ResponseInterface
     */
    public function send()
    {
        $data = $this->getData();
        if(!$this->draft){
            if(method_exists($this, 'cache') && $this->cache())
                return $this->cache();

            $class = new \ReflectionClass($this);
            Log::info("START REQUEST => " . $class->getName());
            $result = $this->sendData($data);
            if($result->isSuccessful()){
                $data_type = null;
                $model = null;
                $list_type = null;
                if(property_exists($this, 'data_type') && ($this->data_type == 'list_object' || $this->data_type == 'list' || $this->data_type == 'paginate')){
                    $data_type = $this->data_type;
                }
                if(property_exists($this, 'model')){
                    $rp = new \ReflectionProperty($this, 'model');
                    $model = $rp->getType()->getName();
                }
                if(property_exists($this, 'list_type')){
                    $rp = new \ReflectionProperty($this, 'list_type');
                    $list_type = $rp->getType()->getName();
                }

                if($data_type == 'list'){
                    $list = [];
                    if($result->getData()){
                        foreach ($result->getData() as $item){
                            if($model){
                                $list[] = new $model($item);
                            }else{
                                $list[] = ($item);
                            }
                        }
                    }

                    return $list;
                }else if($data_type == 'list_object'){
                    return builder_list_of_data($result->getData(), $list_type, $model);
                }else if($data_type == 'paginate') {
                    if($model){
                        return new $model($result->getData());
                    }
                }else{
                    if($model){
                        return new $model($result->getData());
                    }
                }
                return $result->getData();
            }
            Log::info("END REQUEST => " . $class->getName());
            $description = (is_array($result->getErrorDescription()))?json_encode($result->getErrorDescription()):($result->getErrorDescription());
            throw new InvalidRequestException($description);
        }
        return $this->parseData();

    }

    /**
     * Get the associated Response.
     *
     * @return ResponseInterface
     */
    public function getResponse()
    {
        if (null === $this->response) {
            throw new RuntimeException('You must call send() before accessing the Response!');
        }

        return $this->response;
    }

    public function getAuthorizeKey()
    {
        return $this->getParameter('authorize_key');
    }

    public function setAuthorizeKey($value)
    {
        return $this->setParameter('authorize_key', $value);
    }

    public function getPreviewMode()
    {
        return $this->getParameter('preview_mode');
    }

    public function setPreviewMode($value)
    {
        return $this->setParameter('preview_mode', $value);
    }

    public function getEndpoint()
    {
        return $this->getParameter('endpoint');
    }

    public function setEndpoint($value)
    {
        return $this->setParameter('endpoint', $value);
    }

    public function getMetaKey()
    {
        return $this->getParameter('meta_key');
    }

    public function setMetaKey($value)
    {
        return $this->setParameter('meta_key', $value);
    }

    public function getCode()
    {
        return $this->getParameter('code');
    }

    public function setCode($value)
    {
        return $this->setParameter('code', $value);
    }

    public function getId()
    {
        return $this->getParameter('id');
    }

    public function setId($value)
    {
        return $this->setParameter('id', $value);
    }

    public function getLimit()
    {
        return $this->getParameter('limit');
    }

    public function setLimit($value)
    {
        return $this->setParameter('limit', $value);
    }
    public function getKeyword()
    {
        return $this->getParameter('keyword');
    }

    public function setKeyword($value)
    {
        return $this->setParameter('keyword', $value);
    }

    public function getCategories()
    {
        return $this->getParameter('categories');
    }

    public function setCategories($value)
    {
        return $this->setParameter('categories', $value);
    }

    public function getColors()
    {
        return $this->getParameter('colors');
    }

    public function setColors($value)
    {
        return $this->setParameter('colors', $value);
    }

    public function getSizes()
    {
        return $this->getParameter('sizes');
    }

    public function setSizes($value)
    {
        return $this->setParameter('sizes', $value);
    }

    public function getPage()
    {
        return $this->getParameter('page');
    }

    public function setPage($value)
    {
        return $this->setParameter('page', $value);
    }

    public function getSortField()
    {
        return $this->getParameter('sort_field');
    }

    public function setSortField($value)
    {
        return $this->setParameter('sort_field', $value);
    }

    public function getSortType()
    {
        return $this->getParameter('sort_type');
    }

    public function setSortType($value)
    {
        return $this->setParameter('sort_type', $value);
    }

    public function getEmail()
    {
        return $this->getParameter('email');
    }

    public function setEmail($value)
    {
        return $this->setParameter('email', $value);
    }

    public function getFirstname()
    {
        return $this->getParameter('firstname');
    }

    public function setFirstname($value)
    {
        return $this->setParameter('firstname', $value);
    }

    public function getLastname()
    {
        return $this->getParameter('lastname');
    }

    public function setLastname($value)
    {
        return $this->setParameter('lastname', $value);
    }

    public function getPassword()
    {
        return $this->getParameter('password');
    }

    public function setPassword($value)
    {
        return $this->setParameter('password', $value);
    }

    public function getGender()
    {
        return $this->getParameter('gender');
    }

    public function setGender($value)
    {
        return $this->setParameter('gender', $value);
    }

    public function getPhoneNumber()
    {
        return $this->getParameter('phone_number');
    }

    public function setPhoneNumber($value)
    {
        return $this->setParameter('phone_number', $value);
    }

    public function getCountryId()
    {
        return $this->getParameter('country_id');
    }

    public function setCountryId($value)
    {
        return $this->setParameter('country_id', $value);
    }

    public function getStateId()
    {
        return $this->getParameter('state_id');
    }

    public function setStateId($value)
    {
        return $this->setParameter('state_id', $value);
    }

    public function getCityId()
    {
        return $this->getParameter('city_id');
    }

    public function setCityId($value)
    {
        return $this->setParameter('city_id', $value);
    }

    public function getPostalCode()
    {
        return $this->getParameter('postal_code');
    }

    public function setPostalCode($value)
    {
        return $this->setParameter('postal_code', $value);
    }

    public function getAddress()
    {
        return $this->getParameter('address');
    }

    public function setAddress($value)
    {
        return $this->setParameter('address', $value);
    }

    public function getTitle()
    {
        return $this->getParameter('title');
    }

    public function setTitle($value)
    {
        return $this->setParameter('title', $value);
    }

    public function getCustomerId()
    {
        return $this->getParameter('customer_id');
    }

    public function setCustomerId($value)
    {
        return $this->setParameter('customer_id', $value);
    }

    public function getType()
    {
        return $this->getParameter('type');
    }

    public function setType($value)
    {
        return $this->setParameter('type', $value);
    }

    public function getShippingAddress()
    {
        return $this->getParameter('shipping_address');
    }

    public function setShippingAddress($value)
    {
        return $this->setParameter('shipping_address', $value);
    }

    public function getBillingAddress()
    {
        return $this->getParameter('billing_address');
    }

    public function setBillingAddress($value)
    {
        return $this->setParameter('billing_address', $value);
    }

    public function getShippingServiceId()
    {
        return $this->getParameter('shipping_service_id');
    }

    public function setShippingServiceId($value)
    {
        return $this->setParameter('shipping_service_id', $value);
    }

    public function getShippingSeviceRateId()
    {
        return $this->getParameter('shipping_sevice_rate_id');
    }

    public function setShippingSeviceRateId($value)
    {
        return $this->setParameter('shipping_sevice_rate_id', $value);
    }

    public function getCart()
    {
        return $this->getParameter('cart');
    }

    public function setCart($value)
    {
        return $this->setParameter('cart', $value);
    }

    public function getCartSummary()
    {
        return $this->getParameter('cart_summary');
    }

    public function setCartSummary($value)
    {
        return $this->setParameter('cart_summary', $value);
    }

    public function getCurrencyId()
    {
        return $this->getParameter('currency_id');
    }

    public function setCurrencyId($value)
    {
        return $this->setParameter('currency_id', $value);
    }

    public function getName()
    {
        return $this->getParameter('name');
    }

    public function setName($value)
    {
        return $this->setParameter('name', $value);
    }

    public function getBody()
    {
        return $this->getParameter('body');
    }

    public function setBody($value)
    {
        return $this->setParameter('body', $value);
    }

    public function getRate()
    {
        return $this->getParameter('rate');
    }

    public function setRate($value)
    {
        return $this->setParameter('rate', $value);
    }

    public function getProductId()
    {
        return $this->getParameter('product_id');
    }

    public function setProductId($value)
    {
        return $this->setParameter('product_id', $value);
    }

    public function getParentId()
    {
        return $this->getParameter('parent_id');
    }

    public function setParentId($value)
    {
        return $this->setParameter('parent_id', $value);
    }

    public function getGroup()
    {
        return $this->getParameter('group');
    }

    public function setGroup($value)
    {
        return $this->setParameter('group', $value);
    }

    public function getPaymentCallbackUrl()
    {
        return $this->getParameter('payment_callback_url');
    }

    public function setPaymentCallbackUrl($value)
    {
        return $this->setParameter('payment_callback_url', $value);
    }

    public function getPaymentMethod()
    {
        return $this->getParameter('payment_method');
    }

    public function setPaymentMethod($value)
    {
        return $this->setParameter('payment_method', $value);
    }

    public function getOrderId()
    {
        return $this->getParameter('order_id');
    }

    public function setOrderId($value)
    {
        return $this->setParameter('order_id', $value);
    }

    public function getCollections()
    {
        return $this->getParameter('collections');
    }

    public function setCollections($value)
    {
        return $this->setParameter('collections', $value);
    }

    public function getOptionValues()
    {
        return $this->getParameter('option_values');
    }

    public function setOptionValues($value)
    {
        return $this->setParameter('option_values', $value);
    }

    public function getPrice()
    {
        return $this->getParameter('price');
    }

    public function setPrice($value)
    {
        return $this->setParameter('price', $value);
    }

    public function getScope()
    {
        return $this->getParameter('scope');
    }

    public function setScope($value)
    {
        return $this->setParameter('scope', $value);
    }

    public function getClientId()
    {
        return $this->getParameter('client_id');
    }

    public function setClientId($value)
    {
        return $this->setParameter('client_id', $value);
    }

    public function getClientSecret()
    {
        return $this->getParameter('client_secret');
    }

    public function setClientSecret($value)
    {
        return $this->setParameter('client_secret', $value);
    }

    public function getUsername()
    {
        return $this->getParameter('username');
    }

    public function setUsername($value)
    {
        return $this->setParameter('username', $value);
    }

    public function getGrantType()
    {
        return $this->getParameter('grant_type');
    }

    public function setGrantType($value)
    {
        return $this->setParameter('grant_type', $value);
    }

    public function getAccessToken()
    {
        return $this->getParameter('access_token');
    }

    public function setAccessToken($value)
    {
        return $this->setParameter('access_token', $value);
    }

    public function getSlug()
    {
        return $this->getParameter('slug');
    }

    public function setSlug($value)
    {
        return $this->setParameter('slug', $value);
    }






}
