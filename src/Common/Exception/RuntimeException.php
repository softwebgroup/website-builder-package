<?php

namespace WBuilder\Core\Common\Exception;

/**
 * Runtime Exception
 */
class RuntimeException extends \RuntimeException implements WBuilderException
{
}
