<?php
namespace WBuilder\Core;

use Illuminate\Http\Client\HttpClientException;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request as HttpRequest;
use Symfony\Component\HttpFoundation\Session\Session;
use WBuilder\Core\Common\Exception\InvalidRequestException;
use WBuilder\Core\Common\Http\Client;
use WBuilder\Core\Common\Http\ClientInterface;
use WBuilder\Core\Common\Messages\ResponseInterface;
use WBuilder\Core\Common\ParametersTrait;
use WBuilder\Core\Models\Template;
use WBuilder\Core\Models\Draft;
use WBuilder\Core\Messages\AbstractRequest;

abstract class AbstractBuilder implements BuilderInterface
{
    use ParametersTrait {
        setParameter as traitSetParameter;
        getParameter as traitGetParameter;
    }

    protected $session;
    /**
     * @var ClientInterface
     */
    protected $httpClient;

    /**
     * @var \Symfony\Component\HttpFoundation\Request
     */
    protected $httpRequest;

    protected ?Template $template = null;

    protected ?Draft $draft = null;

    protected $cart;
    /**
     * Create a new gateway instance
     *
     * @param ClientInterface          $httpClient  A HTTP client to make API calls with
     * @param HttpRequest     $httpRequest A Symfony HTTP request object
     */
    public function __construct(ClientInterface $httpClient = null, HttpRequest $httpRequest = null)
    {
        $this->session = new Session();
        $this->session->start();
        $this->httpClient = $httpClient ?: $this->getDefaultHttpClient();
        $this->httpRequest = $httpRequest ?: $this->getDefaultHttpRequest();
        $this->initialize();
        if($this->getDefaultParameters()['authorize_key'] != "authorize_key"){
            $this->template = $this->callRequest('\WBuilder\Core\Messages\GetTemplateRequest', array());
        }

        $this->cart = new Classes\Cart\Cart($this);

    }
    /**
     * Initialize this gateway with default parameters
     *
     * @param  array $parameters
     * @return \WBuilder\Core\AbstractBuilder
     */
    public function initialize(array $parameters = array())
    {
        $this->parameters = new ParameterBag;

        // set default parameters
        foreach ($this->getDefaultParameters() as $key => $value) {
            if (is_array($value)) {
                $this->parameters->set($key, reset($value));
            } else {
                $this->parameters->set($key, $value);
            }
        }


        return $this;
    }

    /**
     * @return array
     */
    public function getDefaultParameters()
    {
        return array();
    }

    /**
     * @param  string $key
     * @return mixed
     */
    public function getParameter($key)
    {
        return $this->traitGetParameter($key);
    }

    /**
     * @param  string $key
     * @param  mixed  $value
     * @return $this
     */
    public function setParameter($key, $value)
    {
        return $this->traitSetParameter($key, $value);
    }

    /**
     * Send the request
     *
     * @param string $class The request class name
     * @param array $parameters
     * @return \WBuilder\Core\Common\Messages\AbstractRequest
     */
    protected function createRequest($class, array $parameters = array())
    {
        $obj = new $class($this->httpClient, $this->httpRequest, $this->template, $this->draft);
        return $obj->initialize(array_replace($this->getParameters(), $parameters));
    }

    /**
     * Get the global default HTTP client.
     *
     * @return Client
     */
    protected function getDefaultHttpClient()
    {
        return new Client();
    }

    /**
     * Get the global default HTTP request.
     *
     * @return HttpRequest
     */
    protected function getDefaultHttpRequest()
    {
        return HttpRequest::createFromGlobals();
    }


    public function getTemplate()
    {
        return $this->template;
    }

    public function getDraft()
    {
        return $this->draft;
    }


    protected function callRequest($method, $parameters){
        try{
            $d = $this->createRequest($method, $parameters)->send();
            return $d;
        }catch (\Exception $exception){

        }
        return null;
    }


}
