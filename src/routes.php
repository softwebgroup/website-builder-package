<?php

Route::get('/', [\App\Http\Controllers\Builder\HomeController::class, 'index'])->name('web-home');
Route::get('/profile', [\App\Http\Controllers\Builder\AccountController::class, 'profile'])->middleware('auth:web')->name('profile');
Route::post('/profile', [\App\Http\Controllers\Builder\AccountController::class, 'profile'])->middleware('auth:web');
Route::get('/wishlist', [\App\Http\Controllers\Builder\AccountController::class, 'wishlist'])->middleware('auth:web')->name('wishlist');
Route::get('/checkout', [\App\Http\Controllers\Builder\CartController::class, 'checkout'])->middleware('auth:web')->name('checkout');
Route::post('/checkout', [\App\Http\Controllers\Builder\CartController::class, 'checkout'])->middleware('auth:web');
Route::get('/address/{type}', [\App\Http\Controllers\Builder\AccountController::class, 'profileAddress'])->middleware('auth:web')->name('profile-address');
Route::get('/address/{type}/new', [\App\Http\Controllers\Builder\AccountController::class, 'newProfileAddress'])->middleware('auth:web')->name('new-profile-address');
Route::post('/address/{type}/new', [\App\Http\Controllers\Builder\AccountController::class, 'newProfileAddress'])->middleware('auth:web');
Route::get('/address/{type}/edit', [\App\Http\Controllers\Builder\AccountController::class, 'newProfileAddress'])->middleware('auth:web')->name('edit-profile-address');
Route::post('/address/{type}/edit', [\App\Http\Controllers\Builder\AccountController::class, 'newProfileAddress'])->middleware('auth:web');
Route::get('/address/delete/{id}', [\App\Http\Controllers\Builder\AccountController::class, 'deleteAddress'])->middleware('auth:web')->name('profile-address-delete');
Route::get('/profile/orders', [\App\Http\Controllers\Builder\AccountController::class, 'orders'])->middleware('auth:web')->name('profile-orders');
Route::post('/product/favorite/add', [\App\Http\Controllers\Builder\ProductController::class, 'favorite'])->middleware('auth:web')->name('add-to-favorite');
Route::post('/product/review/submit', [\App\Http\Controllers\Builder\ProductController::class, 'submitReview'])->name('submit-product-review');
Route::group(['prefix' => '/cart'], function() {
    Route::get('/', [\App\Http\Controllers\Builder\CartController::class, 'cart'])->name('cart');
    Route::post('/add', [\App\Http\Controllers\Builder\CartController::class, 'addToCart'])->name('add-to-cart');
    Route::post('/delete', [\App\Http\Controllers\Builder\CartController::class, 'deleteCartItem'])->name('delete-cart-item');
    Route::get('/payment/callback', [\App\Http\Controllers\Builder\CartController::class, 'paymentCallback']);
});
Route::get('/shop', [\App\Http\Controllers\Builder\ShopController::class, 'shop'])->name('shop');
Route::get('/product/{title}/{pid}', [\App\Http\Controllers\Builder\ProductController::class, 'product'])->name('product');
Route::get('/articles/{meta}', [\App\Http\Controllers\Builder\ArticleController::class, 'article'])->name('articles-list');
Route::get('/article/{id}/{title}', [\App\Http\Controllers\Builder\ArticleController::class, 'article'])->name('article');
Route::post('/article/review/submit', [\App\Http\Controllers\Builder\ArticleController::class, 'submitReview'])->name('submit-post-review');
Route::group(['prefix' => '/ajax'], function() {
    Route::get('/product-details', [\App\Http\Controllers\Builder\AjaxController::class, 'productDetails'])->name('ajax-product-details');
    Route::post('/states', [\App\Http\Controllers\Builder\AjaxController::class, 'states'])->name('get-states');
    Route::post('/cities', [\App\Http\Controllers\Builder\AjaxController::class, 'cities'])->name('get-cities');
    Route::post('/template/data-refresh', [\App\Http\Controllers\Builder\AjaxController::class, 'generateDataInTemplate'])->name('generate-data-template');
});


Route::get('/login', [\App\Http\Controllers\Builder\LoginController::class, 'showLoginForm'])->name('login');
Route::post('/login', [\App\Http\Controllers\Builder\LoginController::class, 'login']);
Route::get('/register', [\App\Http\Controllers\Builder\RegisterController::class, 'showRegistrationForm'])->name('register');
Route::post('/register', [\App\Http\Controllers\Builder\RegisterController::class, 'register']);
Route::get('/password/reset', [\App\Http\Controllers\Builder\ForgotPasswordController::class, 'showLinkRequestForm'])->name('password.request');
Route::post('/password/reset', [\App\Http\Controllers\Builder\ForgotPasswordController::class, 'reset'])->name('password.update');
Route::post('/password/email', [\App\Http\Controllers\Builder\ForgotPasswordController::class, 'sendResetLinkEmail'])->name('password.email');
Route::get('/password/reset/{token}', [\App\Http\Controllers\Builder\ForgotPasswordController::class, 'showResetForm'])->name('password.reset');
Route::get('/password/confirm', [\App\Http\Controllers\Builder\ForgotPasswordController::class, 'showConfirmForm'])->name('password.confirm');
Route::post('/password/confirm', [\App\Http\Controllers\Builder\ForgotPasswordController::class, 'confirm']);
Route::post('/logout', [\App\Http\Controllers\Builder\LoginController::class, 'logout'])->name('logout');
Route::get('/blank_page/{data}', [\App\Http\Controllers\Builder\HomeController::class, 'blank']);
