<?php

namespace WBuilder\Core\Middleware;

use Illuminate\Console\Command;
use Illuminate\Http\Request;
use InvalidArgumentException;

class ModuleMiddleware
{
    public function handle(Request $request, $next, $module)
    {
        
        return $next($request);
    }
}
