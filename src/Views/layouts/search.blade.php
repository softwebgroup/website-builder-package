<ul class="pro-header-right-options">
    <li class="dropdown search-field">
        <button class="btn btn-light dropdown-toggle" type="button" id="dropdownAccountButton31" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-search"></i>
        </button>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownAccountButton31">
            <form>
                <div class="search-field-module">
                    <input type="text" class="form-control" id="inlineFormInputGroup01" placeholder="{{ __('content.search.products') }}">
                    <button class="btn" type="submit">
                        <i class="fas fa-search"></i>
                    </button>
                </div>
            </form>
        </div>
    </li>

    @if (\Illuminate\Support\Facades\Auth::check())
    <li class="dropdown profile-tags">
        <button class="btn btn-light dropdown-toggle" type="button" id="dropdownAccountButton42" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-user"></i>
        </button>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownAccountButton42">
            <a class="dropdown-item" href="{{route('profile', route_params())}}">{{ __('content.dashboard') }}</a>
            <a class="dropdown-item" href="{{route('wishlist', route_params())}}">{{ __('content.wishlist') }}&nbsp;({{auth()->user()->favorite_count}})</a>
            <a class="dropdown-item" href="javascript:void(0)" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('content.logout') }}</a>
            <form id="logout-form" action="{{ route('logout', route_params()) }}" method="POST" class="d-none">
                @csrf
            </form>
        </div>
    </li>
    <li>
        <a href="{{route('wishlist', route_params())}}" class="btn btn-light" >
            <i class="far fa-heart"></i>
            <span class="badge badge-secondary header-favorite-counter">{{auth()->user()->favorite_count}}</span>
        </a>
    </li>
    @endif
    <li class="dropdown">
        <button class="btn btn-light dropdown-toggle" type="button" id="dropdownCartButton16" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-shopping-bag"></i>
            <span class="badge badge-secondary header-cart-counter">{{builder()->cart()->summary()->getCount()}}</span>
        </button>
        <div class="dropdown-menu dropdown-menu-right header-cart-list" aria-labelledby="dropdownCartButton16">
            @include(render_view("cart.header"))
        </div>
    </li>
</ul>
