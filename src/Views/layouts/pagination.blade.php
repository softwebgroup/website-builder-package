@php($paginator->setPath(\Request::url()))
@if ($paginator->hasPages())
<nav aria-label="navigation">
    <ul class="pagination">
        <li class="page-item @if ($paginator->onFirstPage()) disabled @endif">
            <a class="page-link page-link-prev" href="{{ $paginator->previousPageUrl() }}" aria-label="{{__('content.previous')}}" tabindex="-1" aria-disabled="true">
                {{__('content.prev')}}
            </a>
        </li>
        @for ($page = 1; $page <= $rows->last_page; $page++)
            @if ($page == $paginator->items()['current_page'])
                <li class="page-item active" aria-current="page"><a class="page-link" href="#">{{ $page }}</a></li>
            @else
                <li class="page-item" aria-current="page"><a class="page-link" href="{{ $paginator->url($page) }}">{{ $page }}</a></li>
            @endif

        @endfor
        <li class="page-item-total">{{__('content.of')}} {{$page - 1}}</li>

        @if ($paginator->hasMorePages())
            <li class="page-item">
                <a class="page-link page-link-next" href="{{ $paginator->nextPageUrl() }}" aria-label="{{__('content.next')}}">
                    {{__('content.next')}}
                </a>
            </li>
        @endif

    </ul>
</nav>
@endif
