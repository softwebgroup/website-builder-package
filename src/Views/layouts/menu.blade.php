<?php $group_name = "Header"; ?>
@if($menu = builder()->menu($group_name))

    <nav class="navbar navbar-expand-lg">
        <div class="navbar-collapse ref_elm_menu_list_{{$group_name}}" data-template="template/menu-header">
            @include(render_view("template.menu-header"), ['data' => $menu])
        </div>
    </nav>
@endif
