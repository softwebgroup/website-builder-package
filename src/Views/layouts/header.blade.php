<header id="headerOne" class="header-area header-one header-desktop">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 col-xl-2 col-lg-1">
                <div class="logo">
                    @if($logo = builder()->key('logo_dark'))
                        <a href="{{route('web-home', route_params())}}" >
                            <img class="img-fluid ref_elm_logo_dark" src="{{ builder_image_url(builder()->key('logo_dark')) }}" alt="logo here">
                        </a>
                    @endif

                </div>
            </div>
            <div class="col-12 col-lg-9 col-xl-6 ">
                @include(render_view("layouts/menu"))
            </div>
            <div class="col-12 col-lg-2 d-flex justify-content-end">
                <div class="navbar-lang">
                    <ul>
                        @include(render_view("layouts/language"))
                        @include(render_view("layouts/currency"))
                    </ul>
                </div>
                @include(render_view("layouts/search"))
            </div>
        </div>
    </div>
</header>
