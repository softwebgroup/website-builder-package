@if($languages = builder()->languages())
@if(count($languages) > 1)
<li class="dropdown">
    <button class="btn dropdown-toggle" type="button" >
        {{strtoupper(builder()->getDefaultLanguage()->code)}}
    </button>
    <div class="dropdown-menu  dropdown-menu-right" >
        @foreach($languages as $language)
            <a class="dropdown-item" href="#">{{$language->name}}</a>
        @endforeach
    </div>
</li>
@endif
@endif
