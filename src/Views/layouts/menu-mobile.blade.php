@if($menu = builder()->menu("Header"))
    <nav id="navigation-mobile">
        @foreach($menu->all() as $menu_item)
            <a class="main-manu btn ref_elm_menu_{{$menu_item->id}}" href="{{($menu_item->page)?("/".$menu_item->page->slug):$menu_item->link_to}}">
                {{$menu_item->title}}
            </a>
        @endforeach
    </nav>
@endif

