<!DOCTYPE html>
<html lang="{{ config('app.locale') }}" class="no-js">
    <head>
        <meta charset="UTF-8">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        @php($website_title = builder()->key('website_title'))
        <title>{{$website_title}}</title>
        @php($website_description = builder()->key('website_description'))
        <meta name="description" content="{{$website_description}}">
        @php($website_keywords = builder()->key('website_keywords'))
        <meta name="keywords" content="{{$website_keywords}}">

        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="icon" type="image/png" href="{{ asset('assets/images/miscellaneous/fav.png') }}">

        <!-- Fontawesome CSS Files -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css" />
        <!-- Core CSS Files -->
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css?v=2.0.0') }}">

        <!-- Slider Revolution CSS Files -->
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/revolution/css/settings.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/revolution/css/layers.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/revolution/css/navigation.css') }}">
        @include(render_view("layouts/theme-loader"))
        @stack("module_css")
    </head>
    <body>

        @include(render_view("layouts/preloading"))
        <div class="wrapper" style="display: none;">
            @include(render_view("layouts/header"))
            @include(render_view("layouts/header-sticky"))
            @include(render_view("layouts/header-mobile"))
            @yield('content')
            @include(render_view("layouts/footer"))

        </div>
        <div class="mobile-overlay"></div>
        <a href="#" class="btn-secondary " id="back-to-top" title="Back to top">&uarr;</a>


        <div class="notifications" id="notificationWishlist">Product Added To Wishlist</div>
        <div class="notifications" id="deletedNotificationWishlist">Product Deleted From Wishlist</div>
        <div class="notifications" id="notificationCart">Product Added To Cart</div>
        <div class="notifications" id="notificationCompare">Product Added For Compare</div>

        <template id="tmp_loading">
            <div class="spinner0"></div>
        </template>
        @yield('modals')
        <script>
            let CONFIG_ROUTE_ADD_TO_CART = '{{route('add-to-cart', route_params())}}';
            let CONFIG_ROUTE_DELETE_FROM_CART = '{{route('delete-cart-item', route_params())}}';
            let CONFIG_ROUTE_GET_STATES = '{{route('get-states', route_params())}}';
            let CONFIG_ROUTE_GET_CITIES = '{{route('get-cities', route_params())}}';
            let CONFIG_ROUTE_ADD_TO_FAVORITE = '{{route('add-to-favorite', route_params())}}';
            let CONFIG_ROUTE_SUBMIT_PRODUCT_REVIEW = '{{route('submit-product-review', route_params(['json' => 1]))}}';
            let CONFIG_ROUTE_SUBMIT_POST_REVIEW = '{{route('submit-post-review', route_params(['json' => 1]))}}';
            let CONFIG_ROUTE_GENERATE_DATA_IN_TEMPLATE_HTML = '{{route('generate-data-template', route_params())}}';


            let LANG_PLEASE_SELECT_STATE = '{{__('content.select.state')}}';
            let LANG_PLEASE_SELECT_CITY = '{{__('content.select.city')}}';
            let LANG_ADD_TO_FAVORITE = '{{__('content.add.to.wishlist')}}';
            let LANG_DELETE_FROM_FAVORITE = '{{__('content.delete.from.wishlist')}}';

            let CONFIG_CURRENT_CURRENCY = '{!! json_encode(builder()->getDefaultCurrency()) !!}'
        </script>
        <!-- All custom scripts here -->
        <script src="{{ asset('assets/js/scripts.js?v=1.8.0') }}"></script>

        <!-- Slider Revolution core JavaScript files -->
        <script src="{{ asset('assets/revolution/js/jquery.themepunch.tools.min.js') }}"></script>
        <script src="{{ asset('assets/revolution/js/jquery.themepunch.revolution.min.js') }}"></script>

        <!-- Slider Revolution extension scripts. ONLY NEEDED FOR LOCAL TESTING -->
        <script src="{{ asset('assets/revolution/js/extensions/revolution.extension.actions.min.js') }}"></script>
        <script src="{{ asset('assets/revolution/js/extensions/revolution.extension.carousel.min.js') }}"></script>
        <script src="{{ asset('assets/revolution/js/extensions/revolution.extension.kenburn.min.js') }}"></script>
        <script src="{{ asset('assets/revolution/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
        <script src="{{ asset('assets/revolution/js/extensions/revolution.extension.migration.min.js') }}"></script>
        <script src="{{ asset('assets/revolution/js/extensions/revolution.extension.navigation.min.js') }}"></script>
        <script src="{{ asset('assets/revolution/js/extensions/revolution.extension.parallax.min.js') }}"></script>
        <script src="{{ asset('assets/revolution/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/jstree.min.js"></script>
        <script src="{{ asset('assets/revolution/js/extensions/revolution.extension.video.min.js') }}"></script>
        <script src="{{ asset('assets/js/wbuilder-livescripts.js?v=2.0.0') }}"></script>
        @stack("scripts")
        @stack("module_scripts")
    </body>
</html>
