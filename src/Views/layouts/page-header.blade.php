<div class="container-fuild">
    <div class="@if(@$title) page-header @else page-header-no-conent @endif" style="background-image: url('{{asset('assets/images/miscellaneous/page-header.png')}}')">
        @if(@$title)
        <div class="container">
            <h1 class="page-title {{@$title_class}}">{{@$title}}<span>{{@$sub_title}}</span></h1>
        </div><!-- End .container -->
        @endif
    </div>
    <nav aria-label="breadcrumb">
        <div class="container">
            @if(@$breadcrumb)
                <ol class="breadcrumb">
                @foreach($breadcrumb as $breadcrumb_item)
                    @if($breadcrumb_item['url'] != "")
                        <li class="breadcrumb-item {{@$breadcrumb_item['class']}}"><a href="{{$breadcrumb_item['url']}}">{{$breadcrumb_item['title']}}</a></li>
                    @else
                        <li class="breadcrumb-item active {{@$breadcrumb_item['class']}}" aria-current="page">{{$breadcrumb_item['title']}}</li>
                    @endif
                @endforeach
                </ol>
            @endif
        </div>
    </nav>
</div>
