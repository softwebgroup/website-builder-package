

<div class="se-pre-con">
    <div class="pre-loader">
        <div class="">
            <div class="container-fuild">
                <div class="page-header">
                    <div class="container">
                        <h1 class="page-title skeleton-box"></h1>
                        <p class="skeleton-box"></p>
                    </div><!-- End .container -->
                </div>
                <nav aria-label="breadcrumb">
                    <div class="container">
                        <ol class="breadcrumb">
                            <li class="skeleton-box"></li>
                            <li class="skeleton-box" aria-current="page"> </li>
                        </ol>
                    </div>
                </nav>


            </div>
            <div class="pro-content cart-content">
                <div class="container cart-area cart-page-one">
                    <div class="row">
                        <div class="col-12 col-xl-9">
                            <table class="table top-table">
                                <tbody>
                                <tr class="d-flex">
                                    <td class="col-12 col-md-2">
                                        <div class="pro-thumb skeleton-box"></div>
                                    </td>
                                    <td class="col-12 col-md-4">
                                        <div class="item-detail">
                                            <span class="pro-category skeleton-box"></span>
                                            <h3>
                                            </h3>
                                            <div class="item-attributes skeleton-box"></div>

                                        </div>
                                    </td>
                                    <td class="col-12 col-md-1">
                                        <div class="item-price skeleton-box"></div>
                                    </td>
                                    <td class="col-12 col-md-3 justify-content-center" >
                                        <div class="input-group-control skeleton-box">
                                        </div>
                                    </td>
                                    <td class="col-12 col-md-2">
                                        <div class="item-price skeleton-box"></div>
                                    </td>


                                </tr>
                                <tr class="d-flex">
                                    <td class="col-12 col-md-2">
                                        <div class="pro-thumb skeleton-box"></div>
                                    </td>
                                    <td class="col-12 col-md-4">
                                        <div class="item-detail">
                                            <span class="pro-category skeleton-box"></span>
                                            <h3>
                                            </h3>
                                            <div class="item-attributes skeleton-box"></div>

                                        </div>
                                    </td>
                                    <td class="col-12 col-md-1">
                                        <div class="item-price skeleton-box"></div>
                                    </td>
                                    <td class="col-12 col-md-3 justify-content-center" >
                                        <div class="input-group-control skeleton-box">
                                        </div>
                                    </td>
                                    <td class="col-12 col-md-2">
                                        <div class="item-price skeleton-box"></div>
                                    </td>


                                </tr>
                                <tr class="d-flex">
                                    <td class="col-12 col-md-2">
                                        <div class="pro-thumb skeleton-box"></div>
                                    </td>
                                    <td class="col-12 col-md-4">
                                        <div class="item-detail">
                                            <span class="pro-category skeleton-box"></span>
                                            <h3>
                                            </h3>
                                            <div class="item-attributes skeleton-box"></div>

                                        </div>
                                    </td>
                                    <td class="col-12 col-md-1">
                                        <div class="item-price skeleton-box"></div>
                                    </td>
                                    <td class="col-12 col-md-3 justify-content-center" >
                                        <div class="input-group-control skeleton-box">
                                        </div>
                                    </td>
                                    <td class="col-12 col-md-2">
                                        <div class="item-price skeleton-box"></div>
                                    </td>


                                </tr>

                                </tbody>
                            </table>

                            <div class="row justify-content-between click-btn">
                                <div class="col-12 col-lg-6">
                                    <div class="input-group">
                                        <input type="text" class="form-control"  placeholder="Coupon Code" aria-label="coupon-code" readonly>
                                        <div class="input-group-append">
                                            <button class="btn  skeleton-box" type="button" id="coupon-code"></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6 align-right">

                                    <button type="button" class="btn skeleton-box"></button>


                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-xl-3">
                            <div class="summery">
                                <h3 class=" skeleton-box"></h3>
                                <table class="table right-table">

                                    <tbody>
                                    <tr>
                                        <th >
                                            <div class="bgonly skeleton-box"></div>
                                        </th>
                                        <td  class="justify-content-end d-flex">
                                            <div class="bgonly skeleton-box"></div>
                                        </td>

                                    </tr>
                                    <tr>
                                        <th >
                                            <div class="bgonly skeleton-box"></div>
                                        </th>
                                        <td  class="justify-content-end d-flex">
                                            <div class="bgonly skeleton-box"></div>
                                        </td>

                                    </tr>
                                    <tr class="item-price">
                                        <th>
                                            <div class="bgonly skeleton-box"></div>
                                        </th>
                                        <td class="justify-content-end d-flex" >
                                            <div class="bgonly skeleton-box"></div>
                                        </td>

                                    </tr>
                                    </tbody>
                                </table>
                                <a href="#" class="btn skeleton-box" >
                                </a>
                                <a href="#" class="btn skeleton-box"> </a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <footer  class="pro-content footer-area footer-one footer-desktop">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-12 col-lg-6 newsletter">
                            <h5 class=" skeleton-box">

                            </h5>
                            <h3 class=" skeleton-box"></h3>
                            <p class=" skeleton-box"></p>
                            <div class="input-group">
                                <input type="email" class="form-control" placeholder="Enter Your Email..." aria-label="Enter Your Email..." aria-describedby="addon2" readonly>
                                <div class="input-group-append">
                                    <button class="btn  skeleton-box" type="button" id="addon2"></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid p-0">
                    <div class="copyright-content">
                        <div class="container">
                            <div class="row align-items-center">
                                <div class="col-12 col-md-6">
                                    <div class="footer-info">
                                        <h5 class=" skeleton-box"></h5>
                                    </div>

                                </div>
                                <div class="col-12 col-md-6">
                                    <ul class="socials">
                                        <li class="iconbg skeleton-box"></li>
                                        <li class="iconbg skeleton-box"></li>
                                        <li class="iconbg skeleton-box"></li>
                                        <li class="iconbg skeleton-box"></li>
                                        <li class="iconbg skeleton-box"></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>

</div>
