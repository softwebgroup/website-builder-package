@php($body_theme_style = (@$data)?json_decode($data, true):builder()->key("theme-body-style"))
@if($body_theme_style)
    @if(!@$self) <style class="ref_elm_theme-body-style" data-template="layouts/theme-loader"> @endif
        @foreach($body_theme_style as $style)
            @if(key_exists("background-color", $style['styles']))
                {!! implode(",", $style['styles']["background-color"]) !!}{
            background-color: {{$style['color']}};
        }
        @endif
        @if(key_exists("color", $style['styles']))
            {!! implode(",", $style['styles']["color"]) !!}{
            color: {{$style['color']}};
        }
        @endif
        @if(key_exists("border-color", $style['styles']))
            {!! implode(",", $style['styles']["border-color"]) !!}{
            border-color: {{$style['color']}};
        }
        @endif
        @endforeach

        @if(!@$self) </style> @endif

@endif
