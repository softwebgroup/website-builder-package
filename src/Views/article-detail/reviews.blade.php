<?php /** @var \WBuilder\Core\Models\Article $post **/ ?>

<div class="reviews blog-content">
    <div class="review-bubbles widget">
        <h3 class="mb-3">
            Customer Reviews
        </h3>
        @if($post->comments->count() > 0)
            @foreach($post->comments->all() as $customerReview)
                <div class="media">
                    <i class="fas fa-comments mt-1" style="width:68px;height:68px;    font-size: 50px;
                          text-align: center;"></i>
                    <div class="media-body">
                        <h3>{{$customerReview->name}}</h3>
                        <p>{{$customerReview->body}}</p>
                        <div class="post-date">
                            <i class="fa fa-calendar" aria-hidden="true"></i>
                            <a> {{date("j F, Y", strtotime($customerReview->created_at))}}</a>
                        </div>
                    </div>
                </div>

            @endforeach
        @endif
    </div>
    <div class="write-review">
        <h3 class="mb-3">Write a Review</h3>
        <div class="write-review-box">
            <div class="from-group row mb-3">
                <div class="col-6">
                    <div class="row">
                        <div class="col-12">
                            <label for="inlineFormInputGroup01">Name</label></div>
                        <div class="input-group col-12">
                            <input type="text" name="name" class="form-control" id="inlineFormInputGroup01" placeholder="Enter Your Name">
                            <small class="invalid-feedback"></small>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="row">
                        <div class="col-12"> <label for="inlineFormInputGroup1">Email Address</label></div>
                        <div class="input-group col-12">
                            <input type="text" name="email" class="form-control" id="inlineFormInputGroup1" placeholder="Enter Your Email">
                            <small class="invalid-feedback"></small>
                        </div>
                    </div>
                </div>
            </div>

            <div class="from-group row mb-3">
                <div class="col-12"> <label for="inlineFormInputGroup3">Review Body</label></div>
                <div class="input-group col-12">
                    <textarea class="form-control" name="body" rows="5" id="inlineFormInputGroup3" placeholder="Write Your Review"></textarea>
                    <small class="invalid-feedback"></small>
                </div>
            </div>
            <div class="from-group">
                <button type="button" class="btn btn-submit-post-review btn-secondary swipe-to-top" data-form=".write-review-box" data-pid="{{$post->id}}">Submit</button>

            </div>
        </div>
    </div>
</div>
