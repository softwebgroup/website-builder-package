<?php /** @var \WBuilder\Core\Models\Article $post **/ ?>
<section class="pro-content blog-content">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-12 pro-blog-content">
                <div class="row">
                    <div class="col-12 col-md-12">
                        <div class="blog">
                            <div class="blog-thumb">
                                <img class="img-fluid ref_elm_post_{{$post->id}}_image" src="{{$post->image}}" style="width: 100%;" alt="{{$post->title}}">
                            </div>
                            <div class="blog-info">
                                <div class="blog-meta">
                                    <a >
                                        <i class="far fa-calendar-alt"></i>
                                        {{date("j F, Y", strtotime($post->created_at))}}
                                    </a>
                                    <a>
                                        <i class="fas fa-comments"></i> {{$post->comments_count}} Comments
                                    </a>
                                </div>
                                <h3 class="ref_elm_post_{{$post->id}}_title">{{$post->title}}</h3>
                                <div class="ref_elm_post_{{$post->id}}_long_content">
                                    {!! $post->long_content !!}
                                </div>
                            </div>
                        </div>
                        @include(render_view("article-detail/reviews"))
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
