<section class="pro-content compare-content">
    <div class="container">
        <div class="row">

            <div class="col-lg-6">
                <table class="table">
                    <thead>
                    <tr>
                        <th>
                            <div class="img-div">
                                <img class="img-fluid" src="{{ asset('assets/images/index-3/cat-01.png') }}" alt="Image">
                            </div>

                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <h2 >VAYGHN SUEDE SLIP-ON SNEAKER</h2>
                        </td>

                    </tr>
                    <tr>
                        <td><span>Price&nbsp;:&nbsp;</span><span class="price-tag">$95</span></td>

                    </tr>
                    <tr>
                        <td><span>Color&nbsp;:&nbsp;</span>Black</td>

                    </tr>
                    <tr>
                        <td><span>Size&nbsp;:&nbsp;</span>Large</td>

                    </tr>
                    <tr>
                        <td><span>Type&nbsp;:&nbsp;</span>VR Box, Electronics</td>

                    </tr>
                    <tr>
                        <td>
                            <div class="detail-buttons">
                                <a href="product-page1.html" class="btn btn-secondary">View Details</a>
                            </div>

                        </td>

                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-lg-6">
                <table class="table">
                    <thead>
                    <tr>
                        <th >
                            <div class="img-div">
                                <img class="img-fluid" src="{{ asset('assets/images/index-3/cat-02.png') }}" alt="Image">
                            </div>

                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <h2 >VAYGHN SUEDE SLIP-ON SNEAKER</h2>
                        </td>

                    </tr>
                    <tr>
                        <td><span>Price&nbsp;:&nbsp;</span><span class="price-tag">$120</span></td>

                    </tr>
                    <tr>
                        <td><span>Color&nbsp;:&nbsp;</span>Black</td>

                    </tr>
                    <tr>
                        <td><span>Size&nbsp;:&nbsp;</span>Large</td>

                    </tr>
                    <tr>
                        <td><span>Type&nbsp;:&nbsp;</span>Mobile Stand, Electronics</td>

                    </tr>
                    <tr>
                        <td>
                            <div class="detail-buttons">
                                <a href="product-page1.html" class="btn btn-secondary">View Details</a>
                            </div>

                        </td>

                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</section>
