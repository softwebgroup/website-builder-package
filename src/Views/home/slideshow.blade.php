<?php $group_name = 'slideshow'; ?>
<?php $slideshow = builder()->articles('slideshow') ?>
@if($slideshow && $slideshow->total > 0)
    <div class="col-12 ref_elm_{{$group_name}}_articles" data-template="template/rev-slider" data-type="product" data-param='{"type": "article"}'>
        @include(render_view("template/rev-slider"), [
                'items' => $slideshow,
                'item_variable' => 'article',
                'items_type' => \WBuilder\Core\Types\ListOfArticles::class,
                'item_type' => \WBuilder\Core\Models\Article::class,
            ])

    </div>

@endif
