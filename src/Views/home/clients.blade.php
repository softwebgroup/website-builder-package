<?php $group_name = 'clients'; ?>
<?php $clients = builder()->articles('clients'); ?>
@if($clients->total > 0)
<div class="brands-content pro-content" id="clients_section">
    <div class="container">
        <div class="ref_elm_{{$group_name}}_articles" data-template="template/carousel" data-type="article" data-param='{"item_count": 6, "tmpl": "template/client", "type": "article"}'>
            @include(render_view("template/carousel"), [
                'items' => $clients,
                'tmpl' => 'template/client',
                'item_variable' => 'article',
                'items_type' => \WBuilder\Core\Types\ListOfArticles::class,
                'item_type' => \WBuilder\Core\Models\Article::class,
                'item_count' => 6
            ])
        </div>
    </div>
</div>
@endif
