<?php $group_name = 'product_offers'; ?>
<?php $offers_products = builder()->productByGroup($group_name); ?>
@if($offers_products->total > 0)
    <section class="pro-fs-content pro-content">
        <div class="container">
            <div class="col-12 ref_elm_{{$group_name}}_list" data-template="template/carousel" data-type="product" data-param='{"item_count": 1, "tmpl": "template/product-offer", "type": "product"}'>
{{--                @include(render_view("template/carousel"), [--}}
{{--                        'items' => $offers_products,--}}
{{--                        'tmpl' => 'template/product-offer',--}}
{{--                        'item_variable' => 'product',--}}
{{--                        'items_type' => \WBuilder\Core\Types\ListOfProduct::class,--}}
{{--                        'item_type' => \WBuilder\Core\Models\Product::class,--}}
{{--                        'item_count' => 1--}}
{{--                    ])--}}

            </div>
        </div>
    </section>
@endif

