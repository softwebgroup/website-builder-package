<?php $group_name = 'news'; ?>
<?php $news_articles = builder()->articles($group_name, 1, 3); ?>
@if($news_articles->total > 0)
<section class="pro-blog-content pro-content">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 col-md-6">
                <div class="pro-heading-title">
                    @if($news_title = builder()->key('news_title'))
                        <h2 class="ref_elm_news_title" style="{{builder()->style('news_title')}}">{{$news_title}}</h2>
                    @endif
                    @if($news_sub_title = builder()->key('news_sub_title'))
                        <p class="ref_elm_news_sub_title" style="{{builder()->style('news_sub_title')}}">{{$news_sub_title}}</p>
                    @endif
                </div>
            </div>
            <div class="col-12 col-md-6"></div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="ref_elm_{{$group_name}}_articles" data-template="template/carousel" data-type="article" data-param='{"item_count": 3, "tmpl": "template/article", "type": "article"}'>
                    @include(render_view("template/carousel"), [
                        'items' => $news_articles,
                        'tmpl' => 'template/article',
                        'item_variable' => 'article',
                        'items_type' => \WBuilder\Core\Types\ListOfArticles::class,
                        'item_type' => \WBuilder\Core\Models\Article::class,
                        'item_count' => 3
                    ])
                </div>
            </div>
        </div>
    </div>
</section>
@endif
