<?php $group_name = 'new_products'; ?>
<?php $new_products = builder()->productByGroup($group_name) ?>
@if($new_products->total > 0)
<section class="tabs-content pro-content" >
    <div class="container">
        <div class="row ">
            <div class="col-12 col-md-12">
                <div class="pro-heading-title">
                    @if($featured_title = builder()->key('featured_title'))
                        <h2 class="ref_elm_featured_title" style="{{builder()->style('featured_title')}}">{{$featured_title}}</h2>
                    @endif
                    @if($featured_description = builder()->key('featured_description'))
                        <p class="ref_elm_featured_description" style="{{builder()->style('featured_description')}}">{{$featured_description}}</p>
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 ref_elm_{{$group_name}}_list" data-template="template/carousel" data-type="product" data-param='{"item_count": 4, "tmpl": "template/product", "type": "product"}'>
                @include(render_view("template/carousel"), [
                        'items' => $new_products,
                        'tmpl' => 'template/product',
                        'item_variable' => 'product',
                        'items_type' => \WBuilder\Core\Types\ListOfProduct::class,
                        'item_type' => \WBuilder\Core\Models\Product::class,
                        'item_count' => 4
                    ])

            </div>
        </div>
    </div>
</section>
@endif

