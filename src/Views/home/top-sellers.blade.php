<div class="pro-top-sellers pro-content">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 col-md-6">
                <div class="pro-heading-title">
                    <h2> Top Sellers
                    </h2>
                    <p>Lorem ipsum dolor sit amet, consectetur
                    </p>
                </div>
            </div>
            <div class="col-12 col-md-6">

            </div>

        </div>
        <div class="row">
            <div class="col-12">
                <div class="top-seller-carousel-js row">
                    <div class=" ">
                        <div class="product product-2x">
                            <article>
                                <div class="pro-thumb">

                                    <img class="img-fluid" src="{{ asset('assets/images/product_images/product_2x.jpg') }}" alt="Product-Image">

                                </div>

                                <div class="pro-info">
                                    <div class="pro-over">
                                        <h3 >Wood Seating Bench</h3>
                                        <div class="pro-price">
                                            <ins>$119 </ins>
                                            <del>
                                                $182
                                            </del>
                                        </div>
                                        <a href="{{ route('shop', route_params()) }}" class="btn btn-secondary">Buy Now</a>
                                    </div>
                                </div>


                            </article>
                        </div>
                    </div>
                    <div class=" ">
                        <div class="product product-2x">
                            <article>
                                <div class="pro-thumb">
                                    <img class="img-fluid" src="{{ asset('assets/images/product_images/product_2x-02.jpg') }}" alt="Product-Image">

                                </div>

                                <div class="pro-info">
                                    <div class="pro-over">
                                        <h3 >Modern Seating Sofa</h3>
                                        <div class="pro-price">
                                            <ins>$219 </ins>
                                            <del>
                                                $270
                                            </del>
                                        </div>
                                        <a href="{{ route('shop', route_params()) }}" class="btn btn-secondary">Buy Now</a>
                                    </div>
                                </div>


                            </article>
                        </div>
                    </div>
                    <div class=" ">
                        <div class="product product-2x">
                            <article>
                                <div class="pro-thumb">

                                    <img class="img-fluid" src="{{ asset('assets/images/product_images/product_2x-01.jpg') }}" alt="Product-Image">

                                </div>

                                <div class="pro-info">
                                    <div class="pro-over">
                                        <h3 >Modern Single Sofa</h3>
                                        <div class="pro-price">
                                            <ins>$119 </ins>
                                            <del>
                                                $182
                                            </del>
                                        </div>
                                        <a href="{{ route('shop', route_params()) }}" class="btn btn-secondary">Buy Now</a>
                                    </div>
                                </div>


                            </article>
                        </div>
                    </div>
                    <div class=" ">
                        <div class="product product-2x">
                            <article>
                                <div class="pro-thumb">

                                    <img class="img-fluid" src="{{ asset('assets/images/product_images/product_2x-03.jpg') }}" alt="Product-Image">

                                </div>

                                <div class="pro-info">
                                    <div class="pro-over">
                                        <h3 >Black Leather Sofa</h3>
                                        <div class="pro-price">
                                            <ins>$119 </ins>
                                            <del>
                                                $182
                                            </del>
                                        </div>
                                        <a href="{{ route('shop', route_params()) }}" class="btn btn-secondary">Buy Now</a>
                                    </div>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
