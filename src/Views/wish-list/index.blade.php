<section class="pro-content wishlist-content my-4">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-12 ">
                <table class="table top-table">
                <tbody>
                    @foreach($products->data->all() as $favorite)
                        @include(render_view("wish-list/wish-item"), ['product' => $favorite->product])
                    @endforeach

                </tbody>
                </table>
                @include(render_view("wish-list/pagination"))

            </div>
        </div>
    </div>
</section>
