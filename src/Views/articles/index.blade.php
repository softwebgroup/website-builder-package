<section class="pro-content page-content blog-content">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-8 pro-blog-content">
                <div class="ref_elm_{{request()->route('meta')}}_articles" data-template="articles/list">
                    @include(render_view("articles/list"))
                </div>
                @include(render_view("articles/pagination"))

            </div>

            @include(render_view("articles/right-side"))
        </div>
    </div>
</section>
