<?php /** @var \WBuilder\Core\Models\ArticlePaginate $articles */ ?>
<?php $articles = (@$data)?builder_list_of_data(json_decode($data, true), \WBuilder\Core\Types\ListOfArticles::class, \WBuilder\Core\Models\Article::class):$articles->data; ?>
<div class="row">
    @foreach($articles->all() as $article)
        @include(render_view("template/article"), ['article' => $article])
    @endforeach
</div>
