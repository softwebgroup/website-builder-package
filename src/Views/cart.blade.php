@extends(render_view("layouts/master"))
@section("content")
    @include(render_view("layouts/page-header"), [
        "title" => "Cart",
        "sub_title" => "Shopping Cart",
        "breadcrumb" => array(
            array("url" => route('web-home', route_params()), "title" => "Home"),
            array("url" => "", "title" => "Cart")
        )
    ])
    @include(render_view("cart/index"))
@endsection
@section("modals")

@endsection
