<section class="pro-content contact-content" id="wb-section-{{$section->id}}">
    <div class="container">
    <div class="row">
        <div class="col-12 col-sm-12">
            <div class="row">
                <div class="col-12 col-lg-6">
                    <h2 class="title mb-1">{{ wbuilder_render_meta($section->meta_elements, 'contact_information_title') }}</h2>
                    <p class="mb-3">{!! wbuilder_render_meta($section->meta_elements, 'contact_information_text') !!}</p>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="contact-info">
                                <h3>The Office</h3>

                                <ul class="contact-list">
                                    <li>
                                        <i class="fas fa-map-marker"></i>
                                        70 Washington   New York, United States
                                    </li>
                                    <li>
                                        <i class="fa fa-phone" aria-hidden="true"></i>
                                        <a href="tel:#">+92 423 567</a>
                                    </li>
                                    <li>
                                        <i class="fas fa-envelope"></i>
                                        <a href="mailto:#">xyz@abc.com</a>
                                    </li>
                                </ul><!-- End .contact-list -->
                            </div><!-- End .contact-info -->
                        </div><!-- End .col-sm-7 -->

                        <div class="col-sm-6">
                            <div class="contact-info">
                                <h3>The Office</h3>

                                <ul class="contact-list">
                                    <li>
                                        <i class="fas fa-clock"></i>
                                        Monday-Saturday<br>
                                        11am-7pm ET
                                    </li>
                                    <li>
                                        <i class="fas fa-calendar-week"></i>
                                        Sunday <br>
                                        11am-6pm ET

                                    </li></ul><!-- End .contact-list -->
                            </div><!-- End .contact-info -->
                        </div><!-- End .col-sm-5 -->
                    </div>

                </div>
                <div class="col-12 col-lg-6">
                    <div class="form-start">
                        <form>
                            <label class="first-label">Name</label>
                            <div class="input-group">


                                <input type="text" class="form-control" id="validationCustomUsername0" placeholder="Enter Your Name" required="">
                                <div class="invalid-feedback">
                                    Please choose a username.
                                </div>
                            </div>
                            <label>Email</label>
                            <div class="input-group">

                                <input type="text" class="form-control" id="validationCustomUsername1" placeholder="Enter Email here.." required="">
                                <div class="invalid-feedback">
                                    Please choose a username.
                                </div>
                            </div>
                            <label>Phone</label>
                            <div class="input-group">


                                <input type="text" class="form-control textbox" id="validationCustomUsername2" placeholder="Enter Your Phone" required="">
                                <div class="invalid-feedback">
                                    Please choose a username.
                                </div>
                            </div>
                            <label>Message</label>
                            <textarea name="message" placeholder="write..." rows="5" cols="56"></textarea>


                            <button type="submit" class="btn btn-secondary swipe-to-top">SUBMIT <i class="fas fa-location-arrow"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</section>
