@extends(render_view("layouts/master"))
@section("content")
    @stack("checkout_header")
    <div class="pro-content checkout-area">
        <div class="container">
            <div class="row">
                <div class="col-12 col-xl-12">
                    <div class="row">
                        <div class="col-12 col-lg-3 ">
                            @include(render_view("checkout.sidebar"))
                        </div>
                        <div class="col-12 col-lg-9">
                            <div class="checkout-module">
                                <div class="tab-content" >
                                    @yield('checkout_content')
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
@section("modals")

@endsection
