@extends(render_view("layouts.master"))
@section("content")
    @include(render_view("home.index"))
@endsection
@section("modals")
    @include(render_view("layouts.quick-view-modal"))
@endsection
