@extends(render_view("layouts/master"))
@section("content")
    @include(render_view("layouts/page-header"), [
        "title" => "Signup",
        "sub_title" => "Register your account",
        "breadcrumb" => array(
            array("url" => route('web-home', route_params()), "title" => "Home"),
            array("url" => "", "title" => "Signup")
        )
    ])
    <section class="pro-content account-content">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-sm-12 col-md-6">
                    <h2 >{{__('content.new.customer')}}</h2>
                    <div class="registration-process">

                        <form method="POST" action="{{ route('register', route_params()) }}">
                            @csrf
                            <div class="from-group row mb-3">
                                <div class="input-group col-12">
                                    <input type="text" name="firstname" class="form-control @error('firstname') is-invalid @enderror" id="firstname" placeholder="{{__('content.enter.your.firstname')}}" value="{{ old('firstname') }}" required>
                                    @error('firstname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="from-group row mb-3">
                                <div class="input-group col-12">
                                    <input type="text" name="lastname" class="form-control @error('lastname') is-invalid @enderror" id="lastname" placeholder="{{__('content.enter.your.lastname')}}" value="{{ old('lastname') }}" required>
                                    @error('lastname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="from-group row mb-3">
                                <div class="input-group col-12">
                                    <input type="text" name="email" class="form-control @error('email') is-invalid @enderror" id="email" placeholder="{{__('content.enter.your.email')}}" value="{{ old('email') }}" required>
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="from-group row mb-3">
                                <div class="input-group col-12">
                                    <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" id="password" placeholder="{{__('content.enter.your.password')}}" required>
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="from-group">
                                <button type="submit" class="btn btn-primary swipe-to-top">{{__('content.create.account')}}</button>
                                <a href="{{route('login', route_params())}}" class="btn btn-secondary swipe-to-top">{{__('content.login')}}</a>

                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-12 col-sm-12 ">
                    <div class="registration-socials">
                        <div class="col-12 col-sm-12 left">
                            <button type="button" class="btn btn-google"><i class="fab fa-google-plus-g"></i>&nbsp;{{__('content.login.with.google')}}</button>
                            <button type="button" class="btn btn-facebook"><i class="fab fa-facebook-f"></i>&nbsp;{{__('content.login.with.facebook')}}</button>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection
@section("modals")
    @include(render_view("layouts.quick-view-modal"))
@endsection
