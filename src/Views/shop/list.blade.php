<?php /** @var \WBuilder\Core\Models\ProductPaginate $products */ ?>
<?php $products = (@$data && @$template == "shop/list")?builder_list_of_data(json_decode($data, true), \WBuilder\Core\Types\ListOfProduct::class, \WBuilder\Core\Models\Product::class):$products->data; ?>
<div class="row">
    @php
        $col = 'col-12 col-md-4';
        if(request()->has('col')){
            if(request()->get('col') == 4)
                $col = 'col-12 col-md-3';
            else if(request()->get('col') == 3)
                $col = 'col-12 col-md-4';
            else if(request()->get('col') == 2)
                $col = 'col-12 col-md-6';
            else if(request()->get('col') == 1)
                $col = 'col-12';
        }
    @endphp
    @foreach($products->all() as $product)
        @include(render_view("template/product"), ["product" => $product, "class" => $col])
    @endforeach
</div>
