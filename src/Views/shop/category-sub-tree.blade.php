<?php /**  @var \WBuilder\Core\Models\Category $sub_items */ ?>
<li data-id="{{$sub_items->id}}" class="jstree-open" @if(@$selected && in_array($sub_items->id, $selected)) data-jstree='{"selected":true}' @endif>{{$sub_items->title}}
@if ($sub_items->children->count() > 0)
    <ul>
        @if($sub_items->children->count() > 0)
            @foreach ($sub_items->children as $childItems)
                @include('shop.category-sub-tree', ['sub_items' => $childItems])
            @endforeach
        @endif
    </ul>
@endif
</li>
