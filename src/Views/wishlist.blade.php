@extends(render_view("layouts/master"))
@section("content")
    @include(render_view("layouts/page-header"), [
        "title" => "Wishlist",
        "sub_title" => "Like Products",
        "breadcrumb" => array(
            array("url" => route('web-home', route_params()), "title" => "Home"),
            array("url" => "", "title" => "Wishlist")
        )
    ])
    @include(render_view("wish-list/index"))
@endsection
@section("modals")

@endsection
