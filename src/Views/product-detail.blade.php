@extends(render_view("layouts/master"))
@section("content")
    @include(render_view("layouts/page-header"), [
        "breadcrumb" => array(
            array("url" => route('web-home', route_params()), "title" => "Home"),
            array("url" => "", "title" => "Wishlist")
        )
    ])
    @include(render_view("product-detail/index"))
@endsection
@section("modals")

@endsection
