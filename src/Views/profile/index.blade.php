@extends(render_view("profile"))

@section("profile_content")
    <section class="profile-content">
        <div class="heading-title">
            <h2>{{__('content.personal.information')}}</h2>
            <p></p>
        </div>
        @if (session('success'))
            <div class="alert alert-success" role="alert">
                {{__('content.personal.information.updated')}}
            </div>
        @endif
        <form method="POST" action="{{route('profile', route_params())}}">
            @csrf
            <div class="form-row">
                <div class="from-group col-md-12 mb-3">
                    <div class="input-group ">
                        <input type="text" name="firstname"
                               class="form-control @error("firstname") is-invalid @enderror"
                               id="firstname"
                               placeholder="{{__('content.enter.your.firstname')}}"
                               value="{{old('firstname', \Illuminate\Support\Facades\Auth::user()->firstname)}}">
                        @error("firstname")
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="from-group col-md-12 mb-3">
                    <div class="input-group ">
                        <input type="text" name="lastname"
                               class="form-control @error("lastname") is-invalid @enderror"
                               id="lastname"
                               placeholder="{{__('content.enter.your.lastname')}}"
                               value="{{old('lastname', Illuminate\Support\Facades\Auth::user()->lastname)}}">
                        @error("lastname")
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="from-group col-md-12 mb-3">
                    <div class="input-group"  >
                        <input type="text" name="email" readonly class="form-control @error("email") is-invalid @enderror"
                               id="email"
                               placeholder="{{__('content.enter.your.email')}}"
                               value="{{old('email', \Illuminate\Support\Facades\Auth::user()->email)}}">
                        @error("email")
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

            </div>
            <div class="form-row">
                <div class="from-group col-md-12 mb-3">
                    <div class="input-group select-control">
                        <select class="form-control @error("gender") is-invalid @enderror" name="gender" id="gender">
                            <option @if(!\Illuminate\Support\Facades\Auth::user()->gender) selected @endif>{{__('content.choose.your.gender')}}</option>
                            <option value="MALE" @if(\Illuminate\Support\Facades\Auth::user()->gender == "MALE") selected @endif>{{__('content.male')}}</option>
                            <option value="FEMALE" @if(\Illuminate\Support\Facades\Auth::user()->gender == "FEMALE") selected @endif>{{__('content.female')}}</option>
                        </select>
                        @error("gender")
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="from-group col-md-12 mb-3">
                    <div class="input-group"  >
                        <input type="text"
                               class="form-control @error("phone_number") is-invalid @enderror"
                               name="phone_number"
                               id="phone_number"
                               placeholder="{{__('content.enter.your.phone.number')}}"
                               value="{{old('phone_number', \Illuminate\Support\Facades\Auth::user()->phone_number)}}">
                        @error("phone_number")
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 justify-content-end btn-cont">
                <div class="row">
                    <button type="submit" class="btn btn-secondary">{{__('content.update')}}</button>
                </div>
            </div>
        </form>
    </section>
@endsection
@push("profile_header")
    @include(render_view("layouts/page-header"), [
        "title" => __('content.dashboard'),
        "sub_title" => __('content.profile'),
        "breadcrumb" => array(
            array("url" => route('web-home', route_params()), "title" => __('content.home')),
            array("url" => "", "title" => __('content.profile'))
        )
    ])
@endpush
