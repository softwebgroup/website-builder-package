@extends(render_view("profile"))

@section("profile_content")
    @if(\Illuminate\Support\Facades\Auth::user()->shipping_addresses->count() > 0)
        <?php /** @var \WBuilder\Core\Models\Address $address */ ?>
        @foreach(\Illuminate\Support\Facades\Auth::user()->shipping_addresses->all() as $address)
            <table class="table shipping-content">
                <thead>
                <tr>
                    <th scope="col">{{$address->title}}</th>
                    <th scope="col" class="d-none d-md-block text-center">{{__('content.action')}}</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        <label class="form-check-label" for="defaultCheck0">{{$address->address}}</label>
                    </td>
                    <td class="edit-tag">
                        <ul>
                            <li><a href="{{route('edit-profile-address', route_params(['id' => $address->id, 'type' => request()->route('type')]))}}"> <i class="fas fa-pen"></i> </a></li>
                            <li><a href="{{route('profile-address-delete', route_params(['id' => $address->id]))}}"> <i class="fas fa-trash-alt"></i></a></li>
                        </ul>
                    </td>
                </tr>
                </tbody>
            </table>
        @endforeach
    @endif
    <a href="{{route('new-profile-address', route_params(['type' => request()->route('type')]))}}" class="btn btn-secondary">{{__('content.add.address.title')}}</a>

@endsection
@push("profile_header")
    @include(render_view("layouts/page-header"), [
        "title" => __('content.dashboard'),
        "sub_title" => __('content.shipping.addresses'),
        "breadcrumb" => array(
            array("url" => route('web-home', route_params()), "title" => __('content.home')),
            array("url" => route('profile', route_params()), "title" => __('content.dashboard')),
            array("url" => "", "title" => __('content.shipping.addresses'))
        )
    ])
@endpush


