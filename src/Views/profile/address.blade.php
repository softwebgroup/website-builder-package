@extends(render_view("profile"))

@section("profile_content")
    @if(request()->route('type') == "billing")
        @include(render_view("profile.billing.index"))
    @else
        @include(render_view("profile.shipping.index"))
    @endif


@endsection
@push("profile_header")
    @include(render_view("layouts/page-header"), [
        "title" => "Dashboard",
        "sub_title" => "Billing Address",
        "breadcrumb" => array(
            array("url" => route('web-home', route_params()), "title" => __('content.home')),
            array("url" => route('profile', route_params()), "title" => __('content.dashboard')),
            array("url" => "", "title" => "Dashboard")
        )
    ])
@endpush


