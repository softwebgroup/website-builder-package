@extends(render_view("profile"))

@section("profile_content")
    <section class="order-content">
        @foreach($orders->data->all() as $order_item)
            @include(render_view("template/order-item"), ['order' => $order_item])
        @endforeach
        @include(render_view("layouts.pagination"), ['paginator' => new \Illuminate\Pagination\Paginator($orders, $orders->per_page), 'rows' => $orders, 'route' => 'profile-orders'])
    </section>

@endsection
@push("profile_header")
    @include(render_view("layouts/page-header"), [
        "title" => __('content.orders'),
        "sub_title" => __('content.orders.subtitle'),
        "breadcrumb" => array(
            array("url" => route('web-home', route_params()), "title" => "Home"),
            array("url" => route('profile', route_params()), "title" => "Dashboard"),
            array("url" => "", "title" => __('content.orders'))
        )
    ])
@endpush


