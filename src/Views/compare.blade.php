@extends(render_view("layouts/master"))
@section("content")
    @include(render_view("layouts/page-header"), [
        "title" => "Compare",
        "sub_title" => "Compare Products Overview",
        "breadcrumb" => array(
            array("url" => route('home', route_params()), "title" => "Home"),
            array("url" => "", "title" => "Compare")
        )
    ])
    @include(render_view("compare/index"))
@endsection
@section("modals")

@endsection
