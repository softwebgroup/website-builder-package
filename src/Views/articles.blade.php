<?php /** @var \WBuilder\Core\Models\Meta $meta_info **/ ?>
@extends(render_view("layouts/master"))
@section("content")
    @include(render_view("layouts/page-header"), [
        "breadcrumb" => array(
            array("url" => route('web-home', route_params()), "title" => "Home"),
            array("url" => "", "title" => $meta_info->title)
        ),
        "title" => $meta_info->title
    ])
    @include(render_view("articles/index"))
@endsection
@section("modals")

@endsection
