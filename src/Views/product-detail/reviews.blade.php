<?php /** @var \WBuilder\Core\Models\Product $product **/ ?>

<div class="reviews">
    <div class="review-bubbles">
        <h3 class="mb-3">
            Customer Reviews
        </h3>
        @if($product->reviews->count() > 0)
            @foreach($product->reviews->all() as $customerReview)
                <div class="review-bubble-single">
                    <div class="review-bubble-bg">
                        <div class="pro-rating" data-rate="{{$customerReview->rate}}"></div>
                        <h4>{{$customerReview->title}}</h4>
                        <span>{{date("Y", strtotime($customerReview->created_at))}}</span>
                        <p>{{$customerReview->body}}</p>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
    <div class="write-review">
        <h3 class="mb-3">Write a Review</h3>
        <div class="write-review-box">
            <div class="from-group row mb-3">
                <div class="col-6">
                    <div class="row">
                        <div class="col-12">
                            <label for="inlineFormInputGroup01">Name</label></div>
                        <div class="input-group col-12">
                            <input type="text" name="name" class="form-control" id="inlineFormInputGroup01" placeholder="Enter Your Name">
                            <small class="invalid-feedback"></small>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="row">
                        <div class="col-12"> <label for="inlineFormInputGroup1">Email Address</label></div>
                        <div class="input-group col-12">
                            <input type="text" name="email" class="form-control" id="inlineFormInputGroup1" placeholder="Enter Your Email">
                            <small class="invalid-feedback"></small>
                        </div>
                    </div>
                </div>
            </div>

            <div class="from-group row mb-3">
                <div class="col-12"> <label for="inlineFormInputGroup2">Review Title</label></div>
                <div class="input-group col-12">

                    <input type="text" name="title" class="form-control" id="inlineFormInputGroup2" placeholder="Title of Review">
                    <small class="invalid-feedback"></small>
                </div>
            </div>
            <div class="from-group row mb-3">
                <div class="col-12"> <label for="inlineFormInputGroup3">Review Body</label></div>
                <div class="input-group col-12">
                    <textarea class="form-control" name="body" rows="5" id="inlineFormInputGroup3" placeholder="Write Your Review"></textarea>
                    <small class="invalid-feedback"></small>
                </div>
            </div>
            <div class="from-group row mb-3">
                <div class="col-12"> <label for="inlineFormInputGroup2">Rating</label></div>
                <div class="col-12">
                    <div class="rate-star-container">
                        <div class="pro-rating">
                            <i class="fas fa-star" value="1"></i>
                            <i class="far fa-star" value="2"></i>
                            <i class="far fa-star" value="3"></i>
                            <i class="far fa-star" value="4"></i>
                            <i class="far fa-star" value="5"></i>
                        </div>
                        <input type="hidden" name="rate" value="1" />
                        <small class="invalid-feedback"></small>
                    </div>
                </div>


            </div>
            <div class="from-group">
                <button type="button" class="btn btn-submit-product-review btn-secondary swipe-to-top" data-form=".write-review-box" data-pid="{{$product->id}}">Submit</button>

            </div>
        </div>
    </div>
</div>
