<div class="pro-content page-content product-page product-page-one ref_elm_product_item_{{$product->id}}">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-6">
                @include(render_view("product-detail/gallery"))
            </div>
            <div class="col-12 col-lg-6">
                @include(render_view("product-detail/details"))
            </div>
        </div>
        @include(render_view("product-detail/tabs"))
    </div>
    @include(render_view("product-detail/related-products"))
</div>
