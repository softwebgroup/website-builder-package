<?php /** @var \WBuilder\Core\Models\Product $product **/ ?>
@if($product->long_content)
    {!! $product->long_content !!}
@else
    {!! nl2br($product->short_content) !!}
@endif

