<?php /** @var \WBuilder\Core\Models\Product $product **/ ?>
<div class="slider-wrapper">
    <div class="slider-for">
        @if($product->media->count() > 0)

            @foreach($product->media->all() as $media)
                <a class="slider-for__item ex1 fancybox-button" href="{{ $media->url }}" data-fancybox-group="fancybox-button" title="Lorem ipsum dolor sit amet">
                    <img src="{{ $media->url }}" alt="Zoom Image" />
                </a>
            @endforeach
        @endif
    </div>

    <div class="slider-nav">
        @if($product->media->count() > 0)
            @foreach($product->media->all() as $media)
                <div class="slider-nav__item">
                    <img src="{{ $media->url }}" alt="Zoom Image"/>
                </div>
            @endforeach
        @endif
    </div>
</div>
