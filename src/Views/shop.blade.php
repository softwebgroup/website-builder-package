@extends(render_view("layouts/master"))
@section("content")
    @include(render_view("layouts/page-header"), [
        "title" => "Shop",
        "sub_title" => "Search in products",
        "breadcrumb" => array(
            array("url" => route('web-home', route_params()), "title" => "Home"),
            array("url" => "", "title" => "Shop")
        )
    ])
    @include(render_view("shop/index"))
@endsection
@section("modals")
    @include(render_view("layouts.quick-view-modal"))
@endsection
