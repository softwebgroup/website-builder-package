<form method="POST" action="{{route('checkout', route_params(['step' => 'payment']))}}">
    @csrf
@include(render_view("checkout/payment-cart"))
<div class="heading">
    <h3>{{__('content.payment.methods')}}</h3>
</div>

<div class="col-12 col-sm-12 ">
    <div class="row">
        <ul class="payement-way @error("payment_method") is-invalid @enderror">
            @if($payment_services = builder()->payments())
                @foreach($payment_services->all() as $index => $service)
                    <li>
                        <!-- Default checked -->
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" value="{{$service->id}}" {{
                                    (old('payment_method'))?
                                    ((old('payment_method') == $service->id)?"checked":""):
                                    ((@$address_info)?(($address_info->payment_method == $service->id)?"checked":""):(($index == 0)?"checked":""))
                                    }}
                                   id="defaultChecked_{{$index}}" name="payment_method">
                            <label class="custom-control-label"
                                   for="defaultChecked_{{$index}}"><img src="{{ ($service->image) }}" alt="{{$service->label}}" height="21" /></label>
                        </div>
                    </li>
                @endforeach
            @endif
        </ul>
        @error("payment_method")
        <span class="invalid-feedback mb-3" >
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>
@error("payment_status")
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-danger">{{ $message }}</div>
    </div>
</div>
@enderror
<div class="col-12 col-sm-12 justify-content-end btn-cont">

    <div class="row">
        <a href="{{route('checkout', route_params(['step' => 'shipping-method']))}}" class="btn btn-light">{{__('content.back')}}</a>
        <button type="submit" class="btn btn-secondary">{{__('content.checkout')}}</button>
    </div>
</div>
</form>
