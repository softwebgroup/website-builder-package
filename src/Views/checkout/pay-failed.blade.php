<?php /** @var \WBuilder\Core\Models\Order $order **/ ?>
<section class="pro-content empty-content">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="pro-empty-page">
                    <h2 style="font-size: 150px;color: #f44336;"><i class="far fa-times-circle"></i></h2>
                    <h1 style="color: #f44336;">Payment Failed</h1>
                    <p>
                        {{$message}}.<br/>
                        Go to the
                        <a href="{{route('profile-orders')}}" class="btn-link"><b>Order page</b></a>.
                    </p>
                </div>

            </div>
        </div>
    </div>
</section>
