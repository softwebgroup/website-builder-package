<div class="checkoutd-nav">
    <ul class="nav flex-column nav-pills mb-3">
        <li class="nav-item">
            <a class="nav-link @if(@$step == "shipping-address" || !@$step) active @endif" href="javascript:void(0)">{{__('content.shipping.address')}}</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if(@$step == "billing-address") active @endif" href="javascript:void(0)">{{__('content.billing.address')}}</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if(@$step == "shipping-method") active @endif" href="javascript:void(0)">{{__('content.shipping.method')}}</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if(@$step == "payment") active @endif" href="javascript:void(0)">{{__('content.payment')}}</a>
        </li>
    </ul>
</div>
