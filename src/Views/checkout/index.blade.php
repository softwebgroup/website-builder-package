@extends(render_view("checkout"))

@section("checkout_content")
    @if(@$step == "shipping-address" || !@$step)
        @include(render_view("checkout.shipping-address"))
    @elseif(@$step == "billing-address")
        @include(render_view("checkout.billing-address"))
    @elseif(@$step == "shipping-method")
        @include(render_view("checkout.shipping-methods"))
    @elseif(@$step == "payment")
        @include(render_view("checkout.payment"))
    @else
        @include(render_view("checkout.$step"))
    @endif
@endsection

@push("checkout_header")
    @include(render_view("layouts/page-header"), [
        "title" => "Checkout",
        "sub_title" => "Checkout your cart",
        "breadcrumb" => array(
            array("url" => route('web-home', route_params()), "title" => "Home"),
            array("url" => "", "title" => "Checkout")
        )
    ])
@endpush
