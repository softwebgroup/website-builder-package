<?php /** @var \WBuilder\Core\Models\Order $order **/ ?>
<section class="pro-content empty-content">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="pro-empty-page">
                    <h2 style="font-size: 150px;color: #4caf50;"><i class="far fa-check-circle"></i></h2>
                    <h1 style="color: #4caf50;">Thank You</h1>
                    <p>
                        You have successfully place your order. Your order id is {{$order->order_id}}.<br/>
                        Go to the
                        <a href="{{route('profile-orders')}}" class="btn-link"><b>Order page</b></a>.
                    </p>
                </div>

            </div>
        </div>
    </div>
</section>
