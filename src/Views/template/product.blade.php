<?php /** @var \WBuilder\Core\Models\Product $product **/ ?>
<div class="{{@$class}} ref_elm_product_item_{{$product->id}}">
    <div class="product @if(@$class == "col-12") product-list @endif">
        @if(@$class == "col-12")
            <article>
                <div class="row align-items-start">
                    <div class="col-12 col-lg-3">
                        <div class="pro-thumb">
                            <img class="img-fluid ref_elm_product_item_{{$product->id}}_image" id="myImage1" src="{{ $product->image }}" alt="{{$product->title}}">
                            <div class="badges">
                                @if($categoryName = $product->has_category("featured"))
                                    <div class="badge badge-info">
                                        {{$categoryName}}
                                    </div>
                                @endif
                                @if($product->stock <= 0)
                                    <div class="badge badge-dark">
                                        {{__('content.out.of.stock')}}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-9">
                        <div class="pro-info">
                            <div class="pro-category">
                                {{$product->categories()}}
                            </div>
                            <h3>
                                <a class="ref_elm_product_item_{{$product->id}}_title" href="{{ $product->url() }}">
                                    {{$product->title}}
                                </a>
                            </h3>
                            @if($short_content = $product->short_content)
                            <p>{!! nl2br($short_content) !!}</p>
                            @endif
                            <div class="pro-price ref_elm_price_{{$product->id}}_value">
                                @if($product->price->summary->discount > 0)
                                    <ins>{{number_format($product->price->summary->total_price, 2)}} {{$product->price->currency->symbol}}
                                        <del>
                                            {{number_format($product->price->summary->sub_total, 2)}} {{$product->price->currency->symbol}}
                                        </del>
                                    </ins>
                                @else
                                    <ins>{{number_format($product->price->summary->total_price, 2)}} {{$product->price->currency->symbol}}</ins>
                                @endif
                            </div>
                        </div>
                        <div class="row align-items-end">
                            <div class="col-12 col-lg-6">
                                <div class="pro-options">
                                    <div class="ref_elm_{{$product->id}}_product_colors" data-template="template/product-colors">
                                        @include(render_view("template.product-colors"), ["colors" => $product->colors])
                                    </div>
                                    <div class="ref_elm_{{$product->id}}_product_sizes" data-template="template/product-sizes">
                                        @include(render_view("template.product-sizes"), ["sizes" => $product->sizes])
                                    </div>

                                </div>
                            </div>
                            <div class="col-12 col-lg-6">
                                <div class="icons">
                                    <button class="btn-light icon" onclick="document.getElementById('add-to-cart-form-{{$product->id}}').submit()">
                                        <i class="fas fa-shopping-bag"></i>
                                    </button>
                                    <form id="add-to-cart-form-{{$product->id}}" method="post" action="{{ route('add-to-cart', route_params()) }}">
                                        @csrf
                                        <input type="hidden" name="pid" value="{{$product->id}}" />
                                        <input type="hidden" name="cid" value="{{(@$color?$color->id:"")}}" />
                                        <input type="hidden" name="quantity" value="1" />
                                    </form>
                                    <button class="btn-light icon" onclick="document.getElementById('frm-product-to-favorite-{{$product->id}}').submit()">
                                        <i class="fas fa-heart"></i>
                                    </button>
                                    <form action="{{route('add-to-favorite', route_params())}}" method="post" id="frm-product-to-favorite-{{$product->id}}">
                                        @csrf
                                        <input type="hidden" value="{{$product->id}}" name="product_id" />
                                    </form>
                                    <button class="btn-light icon btn-ajax-modal"
                                            data-modal="#quickViewModal"
                                            data-m="get"
                                            data-u="{{route('ajax-product-details', route_params(['pid' => $product->id, "v" => "product-details-modal"]))}}"
                                            data-pid="{{$product->id}}"
                                    >
                                        <i class="fas fa-eye"></i>
                                    </button>
                                    <button class="btn-light icon" onclick="notificationCompare();">
                                        <i class="fas fa-align-right" data-fa-transform="rotate-90"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        @else
            <article>
                <div class="pro-thumb">
                    <img class="img-fluid ref_elm_product_item_{{$product->id}}_image" id="myImage1" src="{{ $product->image }}" alt="{{$product->title}}">
                    <div class="badges">
                        @if($categoryName = $product->has_category("featured"))
                            <div class="badge badge-info">
                                {{$categoryName}}
                            </div>
                        @endif
                        @if($product->stock <= 0)
                            <div class="badge badge-dark">
                                {{__('content.out.of.stock')}}
                            </div>
                        @endif
                    </div>
                    <div class="pro-hover-icons">
                        <div class="icons">
                            <button class="btn-light icon" onclick="document.getElementById('add-to-cart-form-{{$product->id}}').submit()">
                                <i class="fas fa-shopping-bag"></i>
                            </button>
                            <form id="add-to-cart-form-{{$product->id}}" method="post" action="{{ route('add-to-cart', route_params()) }}">
                                @csrf
                                <input type="hidden" name="pid" value="{{$product->id}}" />
                                <input type="hidden" name="cid" value="{{(@$color?$color->id:"")}}" />
                                <input type="hidden" name="quantity" value="1" />
                            </form>
                            <button class="btn-light icon btn-ajax-modal"
                                    data-modal="#quickViewModal"
                                    data-m="get"
                                    data-u="{{route('ajax-product-details', route_params(['pid' => $product->id, "v" => "product-details-modal"]))}}"
                                    data-pid="{{$product->id}}"
                            >
                                <i class="fas fa-eye"></i>
                            </button>
                            <input type="hidden" class="p-color-{{$product->id}}" value="" />
                            <button class="btn-light icon" onclick="notificationCompare();" data-product-id="{{$product->id}}">
                                <i class="fas fa-align-right" data-fa-transform="rotate-90"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="pro-info">
                    <div class="pro-category">
                        {{$product->categories()}}
                    </div>
                    <h3>
                        <a class="ref_elm_product_item_{{$product->id}}_title" href="{{ $product->url() }}">
                            {{$product->title}}
                        </a>
                    </h3>

                    <div class="pro-price ref_elm_price_{{$product->id}}_value">
                        @if($product->price->summary->discount > 0)
                            <ins>{{number_format($product->price->summary->total_price, 2)}} {{$product->price->currency->symbol}}
                                <del>
                                    {{number_format($product->price->summary->sub_total, 2)}} {{$product->price->currency->symbol}}
                                </del>
                            </ins>
                        @else
                            <ins>{{number_format($product->price->summary->total_price, 2)}} {{$product->price->currency->symbol}}</ins>
                        @endif

                        <button  class="btn-light icon" onclick="document.getElementById('frm-product-to-favorite-{{$product->id}}').submit()">
                            @if($product->is_favorite())
                                <i class="fas fa-heart"></i>
                            @else
                                <i class="far fa-heart"></i>
                            @endif
                        </button>
                        <form action="{{route('add-to-favorite', route_params())}}" method="post" id="frm-product-to-favorite-{{$product->id}}">
                            @csrf
                            <input type="hidden" value="{{$product->id}}" name="product_id" />
                        </form>
                    </div>
                </div>
                <div class="pro-options">
                    <div class="ref_elm_{{$product->id}}_product_colors" data-template="template/product-colors">
                        @include(render_view("template.product-colors"), ["colors" => $product->colors])
                    </div>
                    <div class="ref_elm_{{$product->id}}_product_sizes" data-template="template/product-sizes">
                        @include(render_view("template.product-sizes"), ["sizes" => $product->sizes])
                    </div>
                </div>
            </article>
        @endif



    </div>
</div>
