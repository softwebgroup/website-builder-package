<?php /** @var \WBuilder\Core\Models\Article $article **/ ?>
<div class="brand ref_elm_post_{{$article->id}}">
    <img class="ref_elm_post_{{$article->id}}_image" src="{{ $article->image }}" alt="brand-logo" />
</div>
