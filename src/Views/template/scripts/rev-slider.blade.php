<script>
    var tpj=jQuery;
    var revapi;
    tpj(document).ready(function() {
        if(tpj("#rev_slider_{{$element_id}}").revolution == undefined){
            revslider_showDoubleJqueryError("#rev_slider_{{$element_id}}");
        }else{
            revapi = tpj("#rev_slider_{{$element_id}}").show().revolution({
                sliderType:"standard",
                jsFileLocation:"../revolution/js/",
                //sliderLayout:"fullscreen",
                dottedOverlay:"none",
                delay:9000,
                navigation: {
                    keyboardNavigation:"off",
                    keyboard_direction: "horizontal",
                    mouseScrollNavigation:"off",
                    mouseScrollReverse:"default",
                    onHoverStop:"off",
                    touch:{
                        touchenabled:"on",
                        swipe_threshold: 75,
                        swipe_min_touches: 1,
                        swipe_direction: "horizontal",
                        drag_block_vertical: false
                    }
                    ,
                    bullets: {
                        enable:true,
                        hide_onmobile:true,
                        hide_under:960,
                        style:"hermes",
                        hide_onleave:false,
                        direction:"horizontal",
                        h_align:"center",
                        v_align:"bottom",
                        h_offset:0,
                        v_offset:30,
                        space:5,
                        tmp:''
                    }
                },
                responsiveLevels:[1400,1200,992,576],
                visibilityLevels:[1400,1200,992,576],
                gridwidth:[1280,992,676,320],
                gridheight:[520,380,300,220],
                lazyType:"none",
                parallax: {
                    type:"mouse",
                    origo:"slidercenter",
                    speed:2000,
                    levels:[2,3,4,5,6,7,12,16,10,50,46,47,48,49,50,55],
                    type:"mouse",
                    disable_onmobile:"on"
                },
                shadow:0,
                spinner:"off",
                stopLoop:"on",
                stopAfterLoops:0,
                stopAtSlide:0,
                shuffle:"off",
                autoHeight:"off",
                fullScreenAutoWidth:"off",
                fullScreenAlignForce:"off",
                fullScreenOffsetContainer: "",
                fullScreenOffset: "60px",
                disableProgressBar:"on",
                hideThumbsOnMobile:"off",
                hideSliderAtLimit:0,
                hideCaptionAtLimit:0,
                hideAllCaptionAtLilmit:0,
                debugMode:false,
                fallbacks: {
                    simplifyAll:"off",
                    nextSlideOnWindowFocus:"off",
                    disableFocusListener:false,
                }
            });
            var newCall = new Object(),
                cslide;

            newCall.callback = function() {
                var proc = revapi.revgetparallaxproc(),
                    fade = 1+proc,
                    scale = 1+(Math.abs(proc)/10);

                punchgs.TweenLite.set(revapi.find('.slotholder, .rs-background-video-layer'),{opacity:fade,scale:scale});
            }
            newCall.inmodule = "parallax";
            newCall.atposition = "start";

            // revapi.bind("revolution.slide.onloaded",function (e) {
            // revapi.revaddcallback(newCall);
            // });
        }
    });	/*ready*/

</script>
