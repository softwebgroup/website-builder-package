<script>
    (function(jQuery){
        var tabCarousel = jQuery('#tab_carousel_{{$element_id}}');
        if (tabCarousel.length) {
            tabCarousel.each(function(){
                var thisCarousel = jQuery(this),
                    item =  jQuery(this).data('item'),
                    itemmobile =  jQuery(this).data('itemmobile');


                thisCarousel.slick({
                    lazyLoad: 'progressive',
                    dots: true,
                    arrows: false,
                    infinite: true,
                    speed: 300,
                    slidesToShow: item || 4,
                    slidesToScroll: item || 4,
                    adaptiveHeight: true,
                    responsive: [{
                        breakpoint: 992,
                        settings: {
                            slidesToShow: itemmobile || 3,
                            slidesToScroll: itemmobile || 3
                        }
                    },
                        {
                            breakpoint: 768,
                            settings: {
                                slidesToShow: itemmobile || 2,
                                slidesToScroll: itemmobile || 2
                            }
                        },
                        {
                            breakpoint: 576,
                            settings: {
                                slidesToShow: itemmobile || 1,
                                slidesToScroll: itemmobile || 1
                            }
                        }]
                });
            });
        };
    })(jQuery);
</script>
