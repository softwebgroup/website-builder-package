<?php $items = (@$data)?builder_list_of_data(json_decode($data, true), $items_type, $item_type):$items->data; ?>
<?php $element_id = (@$element_id)?:\Illuminate\Support\Str::uuid(); ?>
<div class="row" id="tab_carousel_{{$element_id}}" data-item="{{$item_count}}">
    @foreach($items->all() as $item)
        @include(render_view($tmpl), [$item_variable => $item])
    @endforeach
</div>
@if((@$data))
    @include(render_view("template.scripts.carousel"), ['element_id' => $element_id])
@else
    @push("scripts")
        @include(render_view("template.scripts.carousel"), ['element_id' => $element_id])
    @endpush
@endif

