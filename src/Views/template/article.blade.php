<?php /** @var \WBuilder\Core\Models\Article $article **/ ?>
<div class="{{@$class?$class:"col-12 col-md-6"}} ref_elm_post_{{$article->id}}">
    <div class="blog">
        <div class="blog-thumb">
            <img class="img-fluid" src="{{$article->image}}" alt="{{$article->title}}" class="ref_elm_post_{{$article->id}}_image" />
        </div>

        <div class="blog-info">
            <div class="blog-meta">
                <a >
                    <i class="far fa-calendar-alt"></i>
                    {{date("j F, Y", strtotime($article->created_at))}}
                </a>
                <a>
                    <i class="fas fa-comments"></i> {{$article->comments_count}} Comments
                </a>
            </div>
            <h3><a href="{{$article->url()}}" class="ref_elm_post_{{$article->id}}_title">{{$article->title}}</a></h3>
            <p class="ref_elm_post_{{$article->id}}_short_content">{{$article->short_content}}</p>
        </div>
    </div>
</div>
