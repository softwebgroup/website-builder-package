<?php /** @var \WBuilder\Core\Types\ListOfProductColor $colors */ ?>
<?php $colors = (@$data && @$template == "template/product-colors")?builder_list_of_data(json_decode($data, true), \WBuilder\Core\Types\ListOfProductColor::class, \WBuilder\Core\Models\ProductColor::class):$colors; ?>
@if($colors->count() > 0)
    <div class="color-option">
        <ul class="product1">
            @foreach($colors->all() as $color)
                <li><a style="background-color: {{$color->color}}" href="javascript:void(0);" data-cid="{{$color->id}}" data-pid="{{$color->product_id}}"></a></li>
            @endforeach
        </ul>
    </div>
@endif
