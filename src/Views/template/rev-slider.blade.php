<?php $items = (@$data)?builder_list_of_data(json_decode($data, true), $items_type, $item_type):$items->data; ?>
<?php $element_id = (@$element_id)?:\Illuminate\Support\Str::uuid(); ?>
<!-- Revolution Layer Slider -->
<div id="rev_slider_{{$element_id}}_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="scroll-effect136" data-source="gallery" style="padding:0px;">
    <!-- START REVOLUTION SLIDER 5.4.1 fullscreen mode -->
    <div id="rev_slider_{{$element_id}}" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.4.1">
        <ul>
            <!-- SLIDE  3-->
            @foreach($items->all() as $index => $item)
                @if(key_exists("align_view", $item->additional_data))
                    @include(render_view("layouts.slideshow.".$item->additional_data['align_view']), [
                        "id" => $item->id,
                        "title" => $item->title,
                        "short_content" => $item->short_content,
                        "image" => $item->image,
                        "index" => $index
                    ])
                @endif
            @endforeach
        </ul>
        <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
    </div>
</div><!-- END REVOLUTION SLIDER -->

@if((@$data))
    @include(render_view("template.scripts.rev-slider"), ['element_id' => $element_id])
@else
    @push("scripts")
        @include(render_view("template.scripts.rev-slider"), ['element_id' => $element_id])
    @endpush
@endif

