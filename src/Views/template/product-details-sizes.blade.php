<?php /** @var \WBuilder\Core\Types\ListOfProductSize $sizes **/ ?>
<?php /** @var \WBuilder\Core\Models\Product $product **/ ?>
@if($sizes->count() > 0)
    <div class="size-option">
        <b>{{__('content.size')}} : </b>
        <ul class="product-model1">
            @foreach($sizes->all() as $size_item)
                <li class="@if($size && $size->id == $size_item->id) active @endif"><a class="size-select" href="{{ $product->url(null, $size_item->id) }}">{{$size_item->size}}</a></li>
            @endforeach
        </ul>
    </div>
@endif
