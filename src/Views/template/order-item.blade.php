<?php /** @var \WBuilder\Core\Models\Order $order */ ?>
<div class="accordian">
    <table class="table top-table order-table">
        <tbody>
        <tr class="d-flex">
            <td class="col-12 col-md-3 first-item">
                {{date("d F Y", strtotime($order->created_at))}}
            </td>
            <td class="col-12 col-md-2">{{$order->order_id}}</td>
            <td class="col-12 col-md-3">
                <div class="text-secondary">{{$order->status['label']}}</div>
            </td>
            <td class="col-12 col-md-2">
                {{$order->total_price}}{{$order->currency->symbol}}
            </td>
            <td class="col-12 col-md-2">
                <a class="btn btn-link" data-toggle="collapse" href="#orderDetail_{{$order->order_id}}" role="button" aria-expanded="false">
                    {{__('content.view')}}
                </a>
            </td>
        </tr>
        </tbody>
    </table>
    <div class="collapse" id="orderDetail_{{$order->order_id}}">
        <div class="card card-body">
            <section class="orderdetail-content">
                <div class="row">
                    <div class="col-12 col-md-6">
                        <h3>{{__('content.order.id')}} {{$order->order_id}}</h3>
                        <table class="table order-id">
                            <tbody>
                            <tr class="d-flex">
                                <td class="col-6 col-md-6"> <strong>{{__('content.order.status')}}</strong> </td>
                                <td class="pending col-6 col-md-6" ><p>{{$order->status['label']}}</p></td>
                            </tr>
                            <tr class="d-flex">
                                <td class="col-6 col-md-6"><strong>{{__('content.order.date')}}</strong></td>
                                <td  class="underline col-6 col-md-6" >{{date("d/m/Y", strtotime($order->created_at))}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-12 col-md-6">
                        <h3>{{__('content.payment.methods')}}</h3>
                        <table class="table order-id">
                            <tbody>
                            <tr class="d-flex">
                                <td class="address col-12 col-md-6"><strong>{{__('content.payment.gateway')}}</strong></td>
                            </tr>
                            <tr class="d-flex">
                                <td  class="address col-12 col-md-12"></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">

                    <div class="col-12 col-md-6">
                        <h3>
                            {{__('content.shipping.details')}}
                        </h3>
                        <table class="table order-id">
                            <tbody>
                            <tr class="d-flex">
                                <td class="address col-12"><strong>{{__('content.shipping.address')}}</strong></td>
                            </tr>
                            <tr  class="d-flex">
                                <td class="address col-12">{{$order->shipping_address->fullAddress()}}</td>
                            </tr>
                            <tr class="d-flex">
                                <td class="address col-4"><strong>{{__('content.contact')}}</strong></td>
                                <td class="address col-8">{{$order->shipping_address->fullName()}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-12 col-md-6">
                        <h3>
                            {{__('content.billing.details')}}
                        </h3>
                        <table class="table order-id">
                            <tbody>
                            <tr class="d-flex">
                                <td class="address col-12"><strong>{{__('content.billing.address')}}</strong></td>
                            </tr>
                            <tr  class="d-flex">
                                <td class="address col-12">{{$order->billing_address->fullAddress()}}</td>
                            </tr>
                            <tr class="d-flex">
                                <td class="address col-4"><strong>{{__('content.contact')}}</strong></td>
                                <td class="address col-8">{{$order->billing_address->fullName()}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <table class="table orderdetail-content top-table">
                    <tbody>
                    @foreach($order->cart->all() as $cart_item)
                        <tr class="d-flex">
                            <td class="col-12 col-md-2">
                                <img class="img-fluid" src="{{ $cart_item->product->image }}" alt="{{$cart_item->product->title}}">
                            </td>
                            <td class="col-12 col-md-4">
                                <div class="item-detail">
                                    <span class="pro-category">{{$cart_item->product->categories()}}</span>
                                    <h3>{{$cart_item->product->title}}</h3>
                                    <div class="item-attributes"></div>

                                </div>
                            </td>
                            <td class="col-12 col-md-1 item-price">{{$cart_item->price}}{{$order->currency->symbol}}</td>
                            <td class="col-12 col-md-3" >
                                x {{$cart_item->quantity}}
                            </td>
                            <td class="col-12 col-md-2 item-total">{{$cart_item->total_price}}{{$order->currency->symbol}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </section>
        </div>
    </div>
</div>
