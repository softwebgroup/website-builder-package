<ul class="navbar-nav">
    @if(is_string($data))
        @php($data = json_decode($data))
        @foreach($data as $menu_item)
            <li class="nav-item">
                <a class="nav-link ref_elm_menu_{{$menu_item->id}}" href="{{($menu_item->page)?("/".$menu_item->page->slug):$menu_item->link_to}}">
                    {{$menu_item->title}}
                </a>
            </li>
        @endforeach
    @else
        @foreach($data->all() as $menu_item)
            <li class="nav-item">
                <a class="nav-link ref_elm_menu_{{$menu_item->id}}" href="{{($menu_item->page)?("/".$menu_item->page->slug):$menu_item->link_to}}">
                    {{$menu_item->title}}
                </a>
            </li>
        @endforeach
    @endif
</ul>
