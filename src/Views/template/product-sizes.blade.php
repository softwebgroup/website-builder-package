<?php /** @var \WBuilder\Core\Types\ListOfProductSize $sizes */ ?>
<?php $sizes = (@$data && @$template == "template/product-sizes")?builder_list_of_data(json_decode($data, true), \WBuilder\Core\Types\ListOfProductSize::class, \WBuilder\Core\Models\ProductSize::class):$sizes; ?>
@if($sizes->count() > 0)
    <div class="size-option">
        <ul class="product4">
            @foreach($sizes->all() as $size)
                <li><a class="size-select" href="javascript:void(0);">{{$size->size}}</a></li>
            @endforeach
        </ul>
    </div>
@endif
