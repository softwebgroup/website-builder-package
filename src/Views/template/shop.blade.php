<div class="row">
    <div class="col-12 col-lg-3">
        @include(render_view("shop/filter"))
    </div>
    <div class="col-12 col-lg-9">
        @include(render_view("shop/toolbar"))
        <div id="swap" class="productbar ref_elm_shop_list" data-template="shop/list">
            @include(render_view("shop/list"))
        </div>
        @include(render_view("shop/pagination"))
    </div>
</div>
