@extends(render_view("layouts/master"))
@section("content")
    @include(render_view("layouts/page-header"), [
        "breadcrumb" => array(
            array("url" => route('web-home', route_params()), "title" => "Home"),
            array("url" => "", "title" => @$title, "class" => "ref_elm_module_title")
        ),
        "title" => @$title,
        "title_class" => "ref_elm_module_title"
    ])
    <div class="pro-content checkout-area">
        <div class="container">
            {!! $view !!}
        </div>
    </div>
@endsection
@section("modals")

@endsection
