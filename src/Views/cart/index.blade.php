<section class="pro-content cart-content">
    <div class="container cart-area cart-page-one">
        <div class="row">
            <div class="col-12 col-xl-9">
                <table class="table top-table">
                    <tbody>
                    @foreach(builder()->cart()->get()->all() as $cart_item)
                    <tr class="d-flex">
                        <td class="col-12 col-md-2">
                            <img class="img-fluid" src="{{ $cart_item->product->image }}" alt="{{$cart_item->product->title}}">
                        </td>
                        <td class="col-12 col-md-4">
                            <div class="item-detail">
                                <span class="pro-category">{{$cart_item->product->categories()}}</span>
                                <h3>{{$cart_item->product->title}}</h3>
                                <div class="item-attributes"></div>

                            </div>
                        </td>
                        <td class="col-12 col-md-3 item-price">
                            @if($cart_item->product->price->summary->discount > 0)
                                <ins>{{number_format($cart_item->product->price->summary->total_price, 2)}}{{$cart_item->product->price->currency->symbol}}
                                    <del>
                                        {{number_format($cart_item->product->price->summary->sub_total, 2)}}{{$cart_item->product->price->currency->symbol}}
                                    </del>
                                </ins>
                            @else
                                <ins>{{number_format($cart_item->product->price->summary->total_price, 2)}}{{$cart_item->product->price->currency->symbol}}</ins>
                            @endif
                        </td>
                        <td class="col-12 col-md-1 justify-content-center item-total" >
                            x {{$cart_item->quantity}}
                        </td>
                        <td class="col-12 col-md-2 item-total">{{number_format($cart_item->product->price->summary->total_price * $cart_item->quantity)}}{{builder()->getDefaultCurrency()->symbol}}</td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>

                <div class="row justify-content-between click-btn">
                    <div class="col-12 col-lg-6">
                        <div class="input-group">
                            <input type="text" class="form-control"  placeholder="{{__('content.coupon.code')}}" aria-label="{{__('content.coupon.code')}}" aria-describedby="coupon-code">
                            <div class="input-group-append">
                                <button class="btn btn-secondary" type="button" id="coupon-code">{{__('content.apply')}}</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 align-right">

                    </div>
                </div>
            </div>
            <div class="col-12 col-xl-3">
                <div class="summery">
                    <h3>{{__('content.cart.totals')}}</h3>
                    <table class="table right-table">
                        <tbody>
                        <tr>
                            <th >{{__('content.subtotal')}}</th>
                            <td  class="justify-content-end d-flex">{{number_format(builder()->cart()->summary()->getSubTotal(), 2)}}{{builder()->getDefaultCurrency()->symbol}}</td>
                        </tr>
                        <tr>
                            <th >{{__('content.coupon')}}</th>
                            <td class="justify-content-end d-flex" >{{number_format(builder()->cart()->summary()->getDiscount(), 2)}}{{builder()->getDefaultCurrency()->symbol}}</td>
                        </tr>
                        <tr class="item-price">
                            <th>{{__('content.total')}}</th>
                            <td class="justify-content-end d-flex" >{{number_format(builder()->cart()->summary()->getTotal(), 2)}}{{builder()->getDefaultCurrency()->symbol}}</td>
                        </tr>
                        </tbody>
                    </table>
                    <a href="{{route('checkout', route_params())}}" class="btn btn-secondary btn-block" style="text-transform: uppercase">{{__('content.proceed.to.checkout')}}</a>

                    <a href="{{ route('shop', route_params()) }}" class="btn btn-link btn-block" style="text-transform: uppercase">{{__('content.continue.shopping')}}</a>
                </div>

            </div>
        </div>
    </div>
</section>
