<?php /** @var \WBuilder\Core\Models\Page $page **/ ?>
@extends(render_view("layouts/master"))
@section("content")
    @include(render_view("layouts/page-header"), [
        "breadcrumb" => array(
            array("url" => route('web-home', route_params()), "title" => "Home"),
            array("url" => "", "title" => $page->title, "class" => "ref_elm_page_".$page->id."_title")
        ),
        "title" => $page->title,
        "title_class" => "ref_elm_page_".$page->id."_title"
    ])
    @if($page->path)
        {{wbuilder_view($page->path, ['page' => $page])}}
    @else
        @if($page->sections->count() > 0)
            @foreach($page->sections->all() as $section)
                {{wbuilder_view($section->path, ['section' => $section])}}
            @endforeach
        @endif
    @endif

@endsection
@section("modals")

@endsection
