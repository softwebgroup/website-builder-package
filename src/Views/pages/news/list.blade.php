<?php /** @var \WBuilder\Core\Models\ArticlePaginate $articles */ ?>
<div class="row">
    @foreach($articles->data->all() as $article)
        @include(render_view("template/article", ['article' => $article]))
    @endforeach
</div>
