<div class="col-12 col-lg-4  d-lg-block d-xl-block blog-menu">
    <?php $popular_articles = builder()->articles(request()->route('slug'), 1, 5, \WBuilder\Core\Types\ArticleSortField::$_POPULAR_ARTICLES, "desc"); ?>

    @if($popular_articles && $popular_articles->data->count() > 0)
    <div class="widget">
        <h3 class="widget-title">Popular Post</h3><!-- End .widget-title -->
        @foreach($popular_articles->data->all() as $article)
            <div class="media">
                <img src="{{$article->image}}" alt="{{$article->title}}" style="width: 100px; height: 80px">
                <div class="media-body">
                    <h3><a href="{{$article->url()}}">{{$article->title}}</a></h3>
                    <div class="post-date">
                        <i class="fa fa-calendar" aria-hidden="true"></i>
                        <a>{{date("j F, Y", strtotime($article->created_at))}}</a>
                    </div>

                </div>
            </div>
        @endforeach
    </div>
    @endif

</div>
