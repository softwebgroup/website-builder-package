
@include(render_view("layouts.pagination"), ['paginator' => new \Illuminate\Pagination\Paginator($articles, $articles->per_page), 'rows' => $articles, 'route' => request()->route('slug')])
