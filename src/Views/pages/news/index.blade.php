<?php $data = builder()->articles("news", request()->get('page'), 2); ?>
<section class="pro-content page-content blog-content" id="page-{{$page->id}}">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-8 pro-blog-content">
                <div class="ref_elm_{{request()->route('meta')}}_articles" data-template="articles/list">
                    {{ wbuilder_view("pages/news/list", ['articles' => $data]) }}
                </div>
                {{ wbuilder_view("pages/news/pagination", ['articles' => $data]) }}
            </div>
            {{ wbuilder_view("pages/news/right-side", ['articles' => $data]) }}
        </div>
    </div>
</section>
