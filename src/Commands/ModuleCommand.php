<?php


namespace WBuilder\Core\Commands;

use Illuminate\Support\Str;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class ModuleCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:module {module : Name of the new module}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Modules Using Command Line';

    /**
     * @throws \Exception
     */
    public function handle()
    {

        $module = Str::studly($this->argument('module'));
        $moduleRaw = Str::snake($this->argument('module'));

        if($module) {

            $this->warn("--------------------------------");
            $this->warn("Creating a new module: " .$module);
            $this->warn("--------------------------------");

            $routePath = app_path('Modules/'. $module. '/routes.php');
            $configPath = app_path('Modules/'. $module. '/config.php');
            $indexPath = app_path('Modules/'. $module. '/Views/index.blade.php');
            $enTransPath = app_path('Modules/'. $module. '/Translations/en/general.php');
            $frTransPath = app_path('Modules/'. $module. '/Translations/fr/general.php');
            $cssPath = app_path('Modules/'. $module. '/Assets/css/app.css');
            $jsPath = app_path('Modules/'. $module. '/Assets/js/app.js');
            $imagePath = app_path('Modules/'. $module. '/Assets/images/test.png');

            $moduleViewPath = app_path('Modules/'. $module. '/Views');
            $moduleAssetsPath = app_path('Modules/'. $module. '/Assets');
            $moduleAssetImagesPath = app_path('Modules/'. $module. '/Assets/images');
            $moduleAssetJsPath = app_path('Modules/'. $module. '/Assets/js');
            $moduleAssetCssPath = app_path('Modules/'. $module. '/Assets/css');
            $moduleModelPath = app_path('Modules/'. $module. '/Models');
            $moduleCommandsPath = app_path('Modules/'. $module. '/Commands');
            $moduleMiddlewarePath = app_path('Modules/'. $module. '/Middleware');
            $moduleControllerPath = app_path('Modules/'. $module. '/Controllers');
            $moduleEnTranslationPath = app_path('Modules/'. $module. '/Translations/en');
            $moduleFrTranslationPath = app_path('Modules/'. $module. '/Translations/fr');

            if( !File::exists($routePath) )
            {
                $this->warn("Creating Folder Structure ...");
                File::makeDirectory($moduleViewPath, 0755, true);
                $this->info("Created: " .$moduleViewPath);

                File::makeDirectory($moduleAssetsPath, 0755, true);
                $this->info("Created: " .$moduleAssetsPath);

                File::makeDirectory($moduleAssetImagesPath, 0755, true);
                $this->info("Created: " .$moduleAssetImagesPath);
                File::makeDirectory($moduleAssetJsPath, 0755, true);
                $this->info("Created: " .$moduleAssetJsPath);
                File::makeDirectory($moduleAssetCssPath, 0755, true);
                $this->info("Created: " .$moduleAssetCssPath);

                File::makeDirectory($moduleCommandsPath, 0755, true);
                $this->info("Created: " .$moduleCommandsPath);

                File::makeDirectory($moduleControllerPath, 0755, true);
                $this->info("Created: " .$moduleControllerPath);

                File::makeDirectory($moduleMiddlewarePath, 0755, true);
                $this->info("Created: " .$moduleMiddlewarePath);

                File::makeDirectory($moduleModelPath, 0755, true);
                $this->info("Created: " .$moduleModelPath);

                File::makeDirectory($moduleEnTranslationPath, 0755, true);
                $this->info("Created: " .$moduleEnTranslationPath);

                File::makeDirectory($moduleFrTranslationPath, 0755, true);
                $this->info("Created: " .$moduleFrTranslationPath);
                $this->info(PHP_EOL);
            }

            $this->warn("Creating File Structure ...");

            // generate home controller
            $file_target = $moduleControllerPath. '/'.Str::studly($module).'Controller.php';
            $file_tpl = __DIR__. '/Templates/controller.tpl';
            $this->create($file_tpl, $file_target, [
                'module' => $module,
                'module_lower' => $moduleRaw,
                'module_studly' => Str::studly($module)
            ]);

            // generate install command
            $file_target = $moduleCommandsPath. '/'.Str::studly($module).'ModuleInstallCommand.php';
            $file_tpl = __DIR__. '/Templates/install.tpl';
            $this->create($file_tpl, $file_target, [
                'module' => $module,
                'module_lower' => $moduleRaw,
                'module_studly' => Str::studly($module)
            ]);

            // generate routes file
            $file_tpl = __DIR__. '/Templates/routes.tpl';
            $this->create($file_tpl, $routePath, [
                'module' => $module,
                'module_lower' => $moduleRaw,
                'module_studly' => Str::studly($module)
            ]);

            // generate config file
            $file_tpl = __DIR__. '/Templates/config.tpl';
            $this->create($file_tpl, $configPath, [
                'module' => $module,
                'module_lower' => $moduleRaw,
                'module_studly' => Str::studly($module)
            ]);

            // generate config file
            $file_tpl = __DIR__. '/Templates/index.tpl';
            $this->create($file_tpl, $indexPath, [
                'module' => $module,
                'module_lower' => $moduleRaw,
                'module_studly' => Str::studly($module)
            ]);

            // en trans
            $file_tpl = __DIR__. '/Templates/en_trans.tpl';
            $this->create($file_tpl, $enTransPath, [
                'module' => $module,
                'module_lower' => $moduleRaw,
                'module_studly' => Str::studly($module)
            ]);

            // css file
            $file_tpl = __DIR__. '/Templates/css.tpl';
            $this->create($file_tpl, $cssPath, [
                'module' => $module,
                'module_lower' => $moduleRaw,
                'module_studly' => Str::studly($module)
            ]);

            // js file
            $file_tpl = __DIR__. '/Templates/js.tpl';
            $this->create($file_tpl, $jsPath, [
                'module' => $module,
                'module_lower' => $moduleRaw,
                'module_studly' => Str::studly($module)
            ]);

            // image file
            $file_tpl = __DIR__. '/Templates/test.png';
            $this->create($file_tpl, $imagePath, [
                'module' => $module,
                'module_lower' => $moduleRaw,
                'module_studly' => Str::studly($module)
            ]);


            $target = $moduleAssetsPath;
            if(!File::exists(public_path('modules/assets/')))
                File::makeDirectory(public_path('modules/assets/'), 0755, true);
            if(!realpath(public_path('modules/assets/'.$moduleRaw))){
                if(File::exists(public_path('modules/assets/'.$moduleRaw)))
                    unlink(public_path('modules/assets/'.$moduleRaw));
                $link = public_path('modules/assets/'.$moduleRaw);
                symlink($target, $link);
            }




        }
    }

    /**
     * @param $template_path
     * @param $target_path
     * @param array $variables
     * @throws \Exception
     */
    private function create($template_path, $target_path, $variables=[])
    {
        if(!file_exists($template_path)) {
            throw new \Exception("File {$template_path} does not exists.");
        }

        $template = file_get_contents($template_path);
        foreach($variables as $key => $value) {
            $template = str_replace('[['.$key.']]', $value, $template);
        }

        @file_put_contents($target_path, $template);
        $this->info("Created: " .$target_path);
    }
}

