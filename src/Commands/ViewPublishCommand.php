<?php

namespace WBuilder\Core\Commands;

use Illuminate\Console\Command;
use InvalidArgumentException;

class ViewPublishCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'builder:publish';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Builder publish view in resources for change views';

    /**
     * Execute the console command.
     *
     * @return void
     *
     * @throws \InvalidArgumentException
     */
    public function handle()
    {
        if (! is_dir($directory = resource_path('views')."/builder/")) {
            mkdir($directory, 0755, true);
        }
        if (! is_dir($directory = resource_path('views')."/builder/sections/")) {
            mkdir($directory, 0755, true);
        }

//        $source = __DIR__.'/../Views/';
//        $dest = resource_path('views/builder/');
//        $this->recursiveCopy($source, $dest);

        $this->info('Builder publishing views generated successfully.');
    }


    protected function recursiveCopy($src, $dst) {
        $dir = opendir($src);
        @mkdir($dst);
        while(( $file = readdir($dir)) ) {
            if (( $file != '.' ) && ( $file != '..' )) {
                if ( is_dir($src . '/' . $file) ) {
                    $this->recursiveCopy($src .'/'. $file, $dst .'/'. $file);
                }
                else {
                    copy($src .'/'. $file,$dst .'/'. $file);
                }
            }
        }
        closedir($dir);
    }

}
