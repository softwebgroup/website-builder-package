<?php

namespace WBuilder\Core\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Str;
use InvalidArgumentException;

class BuilderCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'builder:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Builder website views and routes';

    /**
     * Execute the console command.
     *
     * @return void
     *
     * @throws \InvalidArgumentException
     */
    public function handle()
    {

        $routePath = resource_path('web.php');
        $file_tpl = __DIR__. '/Templates/web.tpl';
        $this->create($file_tpl, $routePath);

        $this->ensureDirectoriesExist();
        $this->exportViews();
        $this->exportBackend();

        $this->info('Builder scaffolding generated successfully.');
    }

    /**
     * Create the directories for the files.
     *
     * @return void
     */
    protected function ensureDirectoriesExist()
    {
        if (! is_dir($directory = resource_path('views')."/builder/")) {
            mkdir($directory, 0755, true);
        }
        if (! is_dir($directory = app_path('Http/Controllers/Builder'))) {
            mkdir($directory, 0755, true);
        }
        if (! is_dir($directory = resource_path('assets'))) {
            mkdir($directory, 0755, true);
        }
    }

    /**
     * Export the authentication views.
     *
     * @return void
     */
    protected function exportViews()
    {
        if((int)app()->version() >= 10){
            Artisan::call("lang:publish");
        }
        $source = __DIR__.'/../../lang/en';
        $dest = resource_path('lang/en');
        if(function_exists('lang_path'))
            $dest = lang_path("en");

        $this->recursiveCopy($source, $dest);

        if(!file_exists(public_path("assets"))) {
            symlink(__DIR__ . '/../../assets', public_path("assets"));
            $this->info(__DIR__ . '/../../assets'." -> ".public_path('assets'));
        }

        if(!file_exists(config_path('website-builder.php')))
            copy(__DIR__.'/../../config/website-builder.php', config_path('website-builder.php'));
    }

    protected function recursiveCopy($src, $dst) {
        $dir = opendir($src);
        @mkdir($dst);
        while(( $file = readdir($dir)) ) {
            if (( $file != '.' ) && ( $file != '..' )) {
                if ( is_dir($src . '/' . $file) ) {
                    $this->recursiveCopy($src .'/'. $file, $dst .'/'. $file);
                }
                else {
                    copy($src .'/'. $file,$dst .'/'. $file);
                }
            }
        }
        closedir($dir);
    }

    /**
     * Export the authentication backend.
     *
     * @return void
     */
    protected function exportBackend()
    {

        $source = __DIR__.'/../Controllers/';
        $dest = app_path('Http/Controllers/Builder');
        $this->recursiveCopy($source, $dest);
    }

    /**
     * Get full view path relative to the application's configured view path.
     *
     * @param  string  $path
     * @return string
     */
    protected function getViewPath($path)
    {
        return implode(DIRECTORY_SEPARATOR, [
            config('view.paths')[0] ?? resource_path('views'), $path,
        ]);
    }

    /**
     * @param $template_path
     * @param $target_path
     * @param array $variables
     * @throws \Exception
     */
    private function create($template_path, $target_path)
    {
        if(!file_exists($template_path)) {
            throw new \Exception("File {$template_path} does not exists.");
        }

        $template = file_get_contents($template_path);

        @file_put_contents($target_path, $template);
        $this->info("Created: " .$target_path);
    }
}
