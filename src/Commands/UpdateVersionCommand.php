<?php

namespace WBuilder\Core\Commands;

use Illuminate\Console\Command;
use InvalidArgumentException;

class UpdateVersionCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'builder:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Builder website views and routes';


    /**
     * Execute the console command.
     *
     * @return void
     *
     * @throws \InvalidArgumentException
     */
    public function handle()
    {
        $files = [
            [__DIR__."/../../config/website-builder.php", config_path('website-builder.php')],
            [__DIR__."/../Views/layouts/master.blade.php", resource_path('views/builder/layouts/master.blade.php')],
            [__DIR__."/../Views/product-detail/reviews.blade.php", resource_path('views/builder/product-detail/reviews.blade.php')],
            [__DIR__."/../Views/shop/toolbar.blade.php", resource_path('views/builder/shop/toolbar.blade.php')],
            [__DIR__."/../Controllers/ProductController.php", app_path('Http/Controllers/Builder/ProductController.php')],
            [__DIR__."/../Controllers/HomeController.php", app_path('Http/Controllers/Builder/HomeController.php')],
            [__DIR__."/../../assets/css/style.css", resource_path('assets/css/style.css')],
            [__DIR__."/../../assets/js/scripts.js", resource_path('assets/js/scripts.js')],
        ];
        foreach ($files as $file){
            copy($file[0],$file[1]);
            $this->info($file[1].' updated.');
        }
        $this->info('Builder scaffolding updated successfully.');
    }


    /**
     * Export the authentication views.
     *
     * @return void
     */
    protected function exportViews()
    {
        $source = __DIR__.'/../Views/';
        $dest = resource_path('views/builder/');
        $this->recursiveCopy($source, $dest);

        $source = __DIR__.'/../../assets';
        $dest = resource_path('assets');
        if(!file_exists($dest."/css"))
            $this->recursiveCopy($source, $dest);


        $source = __DIR__.'/../../lang/en';
        $dest = resource_path('lang/en');
        if(function_exists('lang_path'))
            $dest = lang_path("en");

        $this->recursiveCopy($source, $dest);

        $this->info(public_path("assets")." -> ".resource_path('assets'));
        symlink(resource_path('assets'), public_path("assets"));

        if(!file_exists(config_path('website-builder.php')))
            copy(__DIR__.'/../../config/website-builder.php', config_path('website-builder.php'));
    }

    protected function recursiveCopy($src, $dst) {
        $dir = opendir($src);
        @mkdir($dst);
        while(( $file = readdir($dir)) ) {
            if (( $file != '.' ) && ( $file != '..' )) {
                if ( is_dir($src . '/' . $file) ) {
                    $this->recursiveCopy($src .'/'. $file, $dst .'/'. $file);
                }
                else {
                    copy($src .'/'. $file,$dst .'/'. $file);
                }
            }
        }
        closedir($dir);
    }

    /**
     * Export the authentication backend.
     *
     * @return void
     */
    protected function exportBackend()
    {
        $routes = file_get_contents(base_path('routes/web.php'));
        if(strpos($routes, "BuilderRouter::routes()") === false){
            file_put_contents(
                base_path('routes/web.php'),
                file_get_contents(__DIR__.'/../Stubs/routes.stub'),
                FILE_APPEND
            );
        }
        $source = __DIR__.'/../Controllers/';
        $dest = app_path('Http/Controllers/Builder');
        $this->recursiveCopy($source, $dest);
    }

    /**
     * Get full view path relative to the application's configured view path.
     *
     * @param  string  $path
     * @return string
     */
    protected function getViewPath($path)
    {
        return implode(DIRECTORY_SEPARATOR, [
            config('view.paths')[0] ?? resource_path('views'), $path,
        ]);
    }
}
