@push("module_css")
<link rel="stylesheet" type="text/css" href="{{ asset('modules/assets/[[module_lower]]/css/app.css') }}">
@endpush
<div class="module-[[module_lower]]">
    <h1>{{ __('[[module_lower]]::general.welcome') }}</h1>
    <img src="{{ asset('modules/assets/[[module_lower]]/images/test.png') }}" />
</div>

@push("module_scripts")
<script src="{{ asset('modules/assets/[[module_lower]]/js/app.js') }}"></script>
@endpush
