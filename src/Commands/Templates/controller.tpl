<?php

namespace App\Modules\[[module]]\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class [[module_studly]]Controller extends Controller
{
    public function index(Request $request)
    {
        return wbuilder_view('module',[
            'view' => view('[[module_lower]]::index')
        ]);
    }
}
