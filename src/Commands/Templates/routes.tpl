<?php

/*
|--------------------------------------------------------------------------
| [[module]] Module Routes
|--------------------------------------------------------------------------
|
| Here you can add routes that belongs to [[module]] module
| Only make sure not to add any routes that does not belong here in
| [[module]] Module ...
|
*/
use Illuminate\Support\Facades\Route;

Route::get('/', [\App\Modules\[[module_studly]]\Controllers\[[module_studly]]Controller::class, 'index'])->name('[[module_lower]]');
