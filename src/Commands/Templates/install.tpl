<?php

namespace App\Modules\[[module]]\Commands;
use Illuminate\Console\Command;

class [[module]]ModuleInstallCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'module:blog {action : Action of command}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Modules Using Command Line';

    /**
     * @throws \Exception
     */
    public function handle()
    {
        $action = $this->argument('action');
        $this->{$action}();
    }
    protected function install()
    {
        $this->info("[[module]] module install completed successfully ...");
    }
}
