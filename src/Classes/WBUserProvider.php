<?php
namespace WBuilder\Core\Classes;

use Illuminate\Contracts\Auth\Authenticatable;
use WBuilder\Core\Common\Http\Exception;

class WBUserProvider implements \Illuminate\Contracts\Auth\UserProvider
{
    public function retrieveById($identifier){
        try{
            $user = builder()->retrieveByUserId($identifier);
            return $user;
        }catch (Exception $exception){

        }
        return null;
    }
    public function retrieveByToken($identifier, $token){
        //dd("retrieveByToken");
    }
    public function updateRememberToken(Authenticatable $user, $token){
        //dd("updateRememberToken");
    }
    public function retrieveByCredentials(array $credentials){
        try{
            $user = builder()->login($credentials['email'], $credentials['password']);
            return $user;
        }catch (\Exception $exception){

        }
        return null;
    }
    public function validateCredentials(Authenticatable $user, array $credentials){
        //fdd("validateCredentials");
    }
}
