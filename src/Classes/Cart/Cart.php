<?php


namespace WBuilder\Core\Classes\Cart;


use http\Exception\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Request as HttpRequest;
use WBuilder\Core\Builder;
use WBuilder\Core\Common\Http\ClientInterface;
use WBuilder\Core\Models\Product;
use WBuilder\Core\Models\ProductColor;
use WBuilder\Core\Models\ServiceRate;
use WBuilder\Core\Types\ListOfCartItem;

class Cart implements CartInterface
{
    protected ListOfCartItem $cart;
    protected ?ServiceRate $shipping_fee;
    protected $builder;
    protected $session;
    /**
     * Cart constructor.
     *
     * @param Builder $builder
     */
    public function __construct($builder)
    {
        $this->builder = $builder;
        $this->session = new Session();
        if($this->session->has('shopping_cart')){
            $this->cart = $this->session->get('shopping_cart');
        }else{
            $this->cart = new ListOfCartItem();
        }
        if($this->session->has('shopping_cart_fee')){
            $this->shipping_fee = $this->session->get('shopping_cart_fee');
        }else{
            $this->shipping_fee = null;
        }

    }

    public function add($product_id, $color_id, $quantity){
        $product = $this->builder->product($product_id);
        if(!$product)
            throw new InvalidArgumentException("Product is not valid");
        $color = $product->colors->search($color_id);

        $this->cart->insert(new CartItem($product, $color, $quantity));
        $cart = $this->cart->all();
        $this->session->set('shopping_cart', $this->cart);
        return $cart;
    }

    public function update($product_id, $color_id, $quantity){
        $product = $this->builder->product($product_id);
        if(!$product)
            throw new InvalidArgumentException("Product is not valid");
        $color = $product->colors->search($color_id);
        if($quantity > 0){
            $this->cart->update(new CartItem($product, $color, $quantity));
        }else{
            $this->cart->delete(new CartItem($product, $color));
        }

        $cart = $this->cart->all();
        $this->session->set('shopping_cart', $this->cart);
        return $cart;
    }

    public function delete($product_id, $color_id){
        $product = $this->builder->product($product_id);
        if(!$product)
            throw new InvalidArgumentException("Product is not valid");
        $color = $product->colors->search($color_id);
        $this->cart->delete(new CartItem($product, $color));

        $cart = $this->cart->all();
        $this->session->set('shopping_cart', $this->cart);
        return $cart;
    }

    public function remove($id){
        $this->cart->remove($id);
        $cart = $this->cart->all();
        $this->session->set('shopping_cart', $this->cart);
        return $cart;
    }

    public function flush(){
        $this->session->remove('shopping_cart');
    }

    public function get(){
        return $this->cart;
    }

    public function setShippingFee(ServiceRate $rate){
        $this->shipping_fee = $rate;
        $this->session->set('shopping_cart_fee', $this->shipping_fee);
    }


    public function summary(){
        $summary = new CartSummary();
        $summary->setCount($this->cart->count());
        $summary->setSubTotal($this->cart->sum("sub_total"));
        $summary->setDiscount($this->cart->sum("discount"));
        $summary->setTax($this->cart->sum("tax"));
        $summary->setShipping(0);
        if($this->shipping_fee)
            $summary->setShipping($this->shipping_fee->price);

        $summary->setTotal($this->cart->sum("total_price") + $summary->getShipping());
        return $summary;
    }
}
