<?php


namespace WBuilder\Core\Classes\Cart;


use WBuilder\Core\Types\ListOfCartItem;

interface CartInterface
{
    /**
     * add product to cart
     *
     * @param int $product_id
     * @param int $color_id
     * @param int $quantity
     *
     * @return array
     */
    public function add($product_id, $color_id, $quantity);

    /**
     * update product quantity
     *
     * @param int $product_id
     * @param int $color_id
     * @param int $quantity
     *
     * @return array
     */
    public function update($product_id, $color_id, $quantity);

    /**
     * delete product from cart
     *
     * @param int $product_id
     * @param int $color_id
     *
     * @return array
     */
    public function delete($product_id, $color_id);

    /**
     * get list of cart
     *
     * @return ListOfCartItem
     */
    public function get();

    /**
     * get summary of cart
     *
     * @return CartSummary
     */
    public function summary();
}
