<?php


namespace WBuilder\Core\Providers;


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use WBuilder\Core\Builder;
use WBuilder\Core\Classes\WBUserProvider;
use WBuilder\Core\Commands\BuilderCommand;
use WBuilder\Core\Commands\ModuleCommand;
use WBuilder\Core\Commands\UpdateVersionCommand;
use WBuilder\Core\Commands\ViewPublishCommand;
use WBuilder\Core\Enums\Mode;
use WBuilder\Core\Middleware\ModuleMiddleware;
use WBuilder\Core\RouteMethods;

class BuilderServiceProvider extends ServiceProvider
{
    private $config = 'website-builder';
    public function boot(Builder $builderLoader)
    {

        $this->loadViewsFrom(__DIR__.'/../Views', 'wbuilder');
        $this->offerPublishing();
        $this->registerCommands();

        $routes = file_get_contents(base_path('routes/web.php'));
        if(strpos($routes, "BuilderRouter::routes()") !== false){
            $routes = str_replace(array("\WBuilder\Core\BuilderRouter::routes();"), array(""), $routes);
            file_put_contents(
                base_path('routes/web.php'),
                $routes
            );

        }
        if(strpos($routes, "return view('welcome');") !== false){
            $routes = str_replace(array("Route::get('/', function () {", "return view('welcome');", "});"), array("", "", ""), $routes);
            file_put_contents(
                base_path('routes/web.php'),
                $routes
            );
        }

        $this->enableI18n()->enableModules();
        $this->app->register(RouteServiceProvider::class);

        $this->app->singleton(Builder::class, function ($app) use ($builderLoader) {
            return $builderLoader;
        });

        Auth::provider('eloquent', function ($app, array $config) {
            return new WBUserProvider();
        });
    }
    public function register()
    {
        if(!file_exists(public_path("assets"))) {
            symlink(__DIR__ . '/../../assets', public_path("assets"));
        }
        if(!file_exists(config_path('website-builder.php')))
            copy(__DIR__.'/../../config/website-builder.php', config_path('website-builder.php'));


        $this->mergeConfigFrom(
            __DIR__.'/../../config/website-builder.php',
            'website-builder'
        );

        if ($this->app->runningInConsole()) {
            $this->commands([
                BuilderCommand::class,
                UpdateVersionCommand::class,
                ViewPublishCommand::class,
                ModuleCommand::class
            ]);
        }
    }
    protected function registerCommands()
    {

    }
    protected function offerPublishing()
    {
        if (! function_exists('config_path')) return;
        if(!file_exists(config_path('website-builder.php'))){
            copy(__DIR__.'/../../config/website-builder.php', config_path('website-builder.php'));
            $this->publishes([__DIR__.'/../../config/website-builder.php' => config_path('website-builder.php')], 'config');
        }
        $this->app->register(
            ThemeServiceProvider::class,
        );


    }


    private function enableModules(): void
    {
        $moduleBasePath = $modulePath = app_path(). '/Modules/';
        foreach (config($this->config. '.modules', []) as $module => $isTurnedOn)
        {
            if ($isTurnedOn)
            {
                $modulePath = $moduleBasePath .Str::studly($module). DIRECTORY_SEPARATOR;

                $moduleName = Str::snake($module);
                $moduleConfigPath = $modulePath. 'config.php';

                if( File::exists($moduleConfigPath) ) {

                    $this->mergeConfigFrom($moduleConfigPath, $moduleName);
                    $this->loadViewsFrom($modulePath.'Views', $moduleName);
                    $this->loadMigrationsFrom($modulePath. 'Migrations');
                    $this->loadTranslationsFrom($modulePath.'Translations', $moduleName);

                    if ($this->app->runningInConsole()) {
                        $moduleCommands = [];
                        $commandDirPath = $modulePath. 'Commands';
                        if (File::isDirectory($commandDirPath)) {
                            foreach (File::files($modulePath. 'Commands') as $file) {
                                $pathInfo = pathinfo($file);
                                $moduleCommands[] = "App\\Modules\\" .Str::studly($module). "\\Commands\\{$pathInfo['filename']}";
                            }
                            $this->commands($moduleCommands);
                        }
                    }
                    $this->app->get('router')->aliasMiddleware('module' , ModuleMiddleware::class);
                    // register middle wares
                    $moduleMiddleware = config($module. '.middleware', []);
                    foreach ($moduleMiddleware as $key => $middleware) {
                        $this->app->get('router')->aliasMiddleware($key , $middleware);
                    }

                    // register service provider
                    $moduleProviders = config($module. '.providers', []);
                    foreach ($moduleProviders as $provider) {
                        $this->app->register($provider);
                    }

                    
                }
            }
        }
    }

    /**
     *
     * Enable i18n
     */
    private function enableI18n(): self
    {
        if( config($this->config. '.i18n') )
        {
            $requested_lang = (strlen(request()->segment(1)) === 2) ? request()->segment(1) : '';
            $requested_country = (strlen(request()->segment(2)) === 2) ? request()->segment(2) : '';

            // set the local for application
            // very important to support multi-languages
            app()->setLocale($requested_lang ?: config('app.fallback_locale'));

            // set url prefix for routing purpose
            config(['langPrefix' => trim("{$requested_lang}/{$requested_country}", DIRECTORY_SEPARATOR)]);
        }
        return $this;
    }

}
