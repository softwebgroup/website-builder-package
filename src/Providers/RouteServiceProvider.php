<?php
namespace WBuilder\Core\Providers;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * @var string
     */
    private $config = 'website-builder';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        $this->routes(function () {
            $routes = __DIR__."/../routes.php";
            if (File::exists($routes)) {
                Route::prefix("/")
                    ->middleware(['web'])
                    ->group($routes);
            }

            foreach (config($this->config. '.modules', []) as $module => $isTurnedOn) {
                if ($isTurnedOn) {
                    $routePrefix = config($module. '.prefix') ?? '/';
                    $modulePath = app_path() . '/Modules/' . Str::studly($module) . DIRECTORY_SEPARATOR;
                    $moduleRoutes = $modulePath . 'routes.php';
                    if (File::exists($moduleRoutes)) {
                        $middlewares = config($module. '.route_middleware', ['web']);
                        if(!in_array('module:'.$module, $middlewares))
                            $middlewares[] = 'module:'.$module;
                        Route::prefix($module)
                            ->middleware($middlewares)
                            ->group($moduleRoutes);

                    }
                }
            }

            $routes = __DIR__."/../page.routes.php";
            if (File::exists($routes)) {
                Route::prefix("/")
                    ->middleware(['web'])
                    ->group($routes);
            }

        });





    }
}
