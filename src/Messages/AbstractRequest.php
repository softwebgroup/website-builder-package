<?php
namespace WBuilder\Core\Messages;

use GuzzleHttp\Psr7\Stream;
use GuzzleHttp\Psr7\StreamWrapper;
use http\Client\Request;
use Illuminate\Support\Facades\Auth;
use WBuilder\Core\Enums\Mode;

abstract class AbstractRequest extends \WBuilder\Core\Common\Messages\AbstractRequest
{
    const API_VERSION = '1.0';

    protected $method;
    protected $endpoint;
    protected $id;

    protected function getBaseData($action, $method = 'POST', $id = null)
    {
        $data = array();
        $data['VERSION'] = static::API_VERSION;
        $this->method = $method;
        $this->id = $id;
        $this->endpoint = $action.(($this->id)?"/{$this->id}":"");

        return $data;
    }


    public function sendData($data)
    {

        $headers = [];
        $parameters = $this->getParameters();
        unset($parameters['authorize_key']);
        unset($parameters['mode']);
        unset($parameters['preview_mode']);
        unset($parameters['endpoint']);
        $parameters['app_id'] = $this->getAuthorizeKey();
        if($this->getAccessToken())
            $headers['Authorization'] = "Bearer {$this->getAccessToken()}";

        if($this->method == "GET"){
            if(count($parameters) > 0)
                $this->endpoint .= "?" . http_build_query($parameters, '', '&');
            $httpResponse = $this->httpClient->request($this->method, $this->endpoint, $headers);
        }else{
            $body = $this->toJSON($parameters);
            $headers['Accept'] = 'application/json';
            $headers['Content-type'] = 'application/json';
            $httpResponse = $this->httpClient->request($this->method, $this->endpoint, $headers, $body);
            if(strpos($this->endpoint, "oauth/token") > 0 && $httpResponse->getStatusCode() == 200){
                $token = json_decode($httpResponse->getBody()->getContents(), true);
                if(array_key_exists('access_token', $token)){
                    $access_token = $token['access_token'];
                    $headers['Authorization'] = "Bearer $access_token";
                    $httpResponse = $this->httpClient->request("GET", config("website-builder.microservices.auth")."profile/me", $headers);
                    $profile = json_decode($httpResponse->getBody()->getContents(), true);
                    $profile['access_token'] = $access_token;
                    return $this->createResponse(json_encode($profile));
                }
            }
        }
        if(strpos($this->endpoint, "getByApplicationIdAndSlug") > 0){
            

        }


        return $this->createResponse($httpResponse->getBody()->getContents());
    }
    public function toJSON($data, $options = 0)
    {
        // Because of PHP Version 5.3, we cannot use JSON_UNESCAPED_SLASHES option
        // Instead we would use the str_replace command for now.
        // TODO: Replace this code with return json_encode($this->toArray(), $options | 64); once we support PHP >= 5.4
        if (version_compare(phpversion(), '5.4.0', '>=') === true) {
            return json_encode($data, $options | 64);
        }
        return str_replace('\\/', '/', json_encode($data, $options));
    }


    protected function createResponse($data)
    {
        return $this->response = new Response($this, $data);
    }

}
