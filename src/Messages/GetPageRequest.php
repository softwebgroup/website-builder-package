<?php

namespace WBuilder\Core\Messages;

use WBuilder\Core\Models\Page;

class GetPageRequest extends AbstractRequest
{
    protected Page $model;

    public function getData()
    {
        $data = $this->getBaseData(config("website-builder.microservices.builder")."pages/slug/get", 'GET');
        return $data;
    }

}
