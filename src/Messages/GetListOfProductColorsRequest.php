<?php

namespace WBuilder\Core\Messages;

use WBuilder\Core\Models\Product;
use WBuilder\Core\Models\ProductColor;
use WBuilder\Core\Types\ListOfCategories;
use WBuilder\Core\Types\ListOfProduct;
use WBuilder\Core\Types\ListOfProductColor;

class GetListOfProductColorsRequest extends AbstractRequest
{
    protected $data_type = 'list_object';
    protected ListOfProductColor $list_type;
    protected ProductColor $model;

    public function getData()
    {
        $data = $this->getBaseData(config("website-builder.microservices.product")."options/values", 'GET');
        return $data;
    }

    
}
