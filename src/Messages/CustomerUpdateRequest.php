<?php

namespace WBuilder\Core\Messages;

use WBuilder\Core\Models\Customer;
use WBuilder\Core\Models\ProductColor;
use WBuilder\Core\Types\ListOfProduct;
use WBuilder\Core\Types\ListOfProductColor;

class CustomerUpdateRequest extends AbstractRequest
{
    protected Customer $model;

    public function getData()
    {
        $data = $this->getBaseData('/customers/update', 'POST');
        return $data;
    }
}
