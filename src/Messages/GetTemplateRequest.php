<?php

namespace WBuilder\Core\Messages;

use WBuilder\Core\Models\Template;

class GetTemplateRequest extends AbstractRequest
{
    protected Template $model;
    public function getData()
    {
        $data = $this->getBaseData(config("website-builder.microservices.builder").'templates/getByKey', 'GET', config("website-builder.authorize_key"));
        return $data;
    }

}
