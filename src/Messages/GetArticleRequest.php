<?php

namespace WBuilder\Core\Messages;

use PhpParser\JsonDecoder;
use WBuilder\Core\Models\Article;
use WBuilder\Core\Models\Template;

class GetArticleRequest extends AbstractRequest
{
    protected Article $model;

    public function getData()
    {
        $data = $this->getBaseData(config("website-builder.microservices.article")."articles/show", 'GET', $this->getParameter('id'));
        return $data;
    }


}
