<?php

namespace WBuilder\Core\Messages;

use WBuilder\Core\Models\Order;

class CreateOrderRequest extends AbstractRequest
{
    protected Order $model;

    public function getData()
    {
        $data = $this->getBaseData('/customers/orders/create', 'POST');
        return $data;
    }
}
