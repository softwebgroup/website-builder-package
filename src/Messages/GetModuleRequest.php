<?php

namespace WBuilder\Core\Messages;

use PhpParser\JsonDecoder;
use WBuilder\Core\Models\Module;

class GetModuleRequest extends AbstractRequest
{
    protected Module $model;

    public function getData()
    {
        $data = $this->getBaseData(config("website-builder.microservices.builder")."modules/getByName", 'GET', $this->getParameter('name'));
        return $data;
    }


}
