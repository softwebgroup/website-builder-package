<?php

namespace WBuilder\Core\Messages;

use PhpParser\JsonDecoder;
use WBuilder\Core\Models\Product;
use WBuilder\Core\Models\Template;

class GetProductBySlugRequest extends AbstractRequest
{
    protected Product $model;

    public function getData()
    {
        $app_id = config("website-builder.authorize_key");
        $data = $this->getBaseData(config("website-builder.microservices.product")."products/getByApplicationIdAndSlug/$app_id/{$this->getParameter('slug')}", 'GET');
        return $data;
    }


}
