<?php

namespace WBuilder\Core\Messages;

use PhpParser\JsonDecoder;
use WBuilder\Core\Models\Order;

class GetOrderDetailsByOrderIdRequest extends AbstractRequest
{
    protected Order $model;

    public function getData()
    {
        $data = $this->getBaseData('/customers/orders/order', 'GET');
        return $data;
    }

}
