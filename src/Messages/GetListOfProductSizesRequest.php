<?php

namespace WBuilder\Core\Messages;

use WBuilder\Core\Models\Product;
use WBuilder\Core\Models\ProductSize;
use WBuilder\Core\Types\ListOfProduct;
use WBuilder\Core\Types\ListOfProductColor;
use WBuilder\Core\Types\ListOfProductSize;

class GetListOfProductSizesRequest extends AbstractRequest
{
    protected $data_type = 'list_object';
    protected ListOfProductSize $list_type;
    protected ProductSize $model;

    public function getData()
    {
        $data = $this->getBaseData(config("website-builder.microservices.product")."options/values", 'GET');
        return $data;
    }

}
