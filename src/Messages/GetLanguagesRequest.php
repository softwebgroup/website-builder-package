<?php

namespace WBuilder\Core\Messages;

use WBuilder\Core\Models\Language;

class GetLanguagesRequest extends AbstractRequest
{
    protected $data_type = 'list';
    protected Language $model;

    public function getData()
    {
        $data = $this->getBaseData('/common/languages', 'GET');
        return $data;
    }
    public function cache(){
        if($this->template && $this->template->languages)
            return $this->template->languages;
        return null;
    }

    public function parseData(){
        $languages = [];
        if($this->draft)
            $languages = $this->draft->meta_data->languages;
        else
            $languages = array($this->template->language);
        return $languages;
    }
}
