<?php

namespace WBuilder\Core\Messages;

use WBuilder\Core\Models\ProductFavorite;

class CheckFavoriteRequest extends AbstractRequest
{
    protected ProductFavorite $model;

    public function getData()
    {
        $data = $this->getBaseData('/products/favorite/check', 'GET');
        return $data;
    }
}
