<?php

namespace WBuilder\Core\Messages;

use WBuilder\Core\Models\Service;
use WBuilder\Core\Types\ListOfService;

class GetPaymentServicesRequest extends AbstractRequest
{
    protected $data_type = 'list_object';
    protected Service $model;
    protected ListOfService $list_type;

    public function getData()
    {
        $data = $this->getBaseData('/services/payments', 'GET');
        return $data;
    }

    public function parseData(){
        $rows = collect(array());
        if($this->draft){
            $rows = $this->draft->meta_data->meta->search('payment_system', 'meta_key');
        }else{
            $rows = $this->template->meta->search('payment_system', 'meta_key');
        }
        return $rows->data;
    }
}
