<?php

namespace WBuilder\Core\Messages;

use WBuilder\Core\Models\Category;
use WBuilder\Core\Types\ListOfCategories;

class GetListOfCategoriesRequest extends AbstractRequest
{
    protected $data_type = 'list_object';
    protected ListOfCategories $list_type;
    protected Category $model;

    public function getData()
    {
        $data = $this->getBaseData(config("website-builder.microservices.product")."categories", 'GET');
        return $data;
    }

}
