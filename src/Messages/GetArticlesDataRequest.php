<?php

namespace WBuilder\Core\Messages;

use WBuilder\Core\Models\Article;
use WBuilder\Core\Models\ArticlePaginate;
use WBuilder\Core\Types\ListOfArticles;

class GetArticlesDataRequest extends AbstractRequest
{
    protected $data_type = 'paginate';
    protected ArticlePaginate $model;

    public function getData()
    {
        $app_id = config("website-builder.authorize_key");
        $data = $this->getBaseData(config("website-builder.microservices.builder")."meta_elements/getByTemplateIdAndMetaKey/$app_id/{$this->getMetaKey()}", 'GET');
        return $data;
    }

}
