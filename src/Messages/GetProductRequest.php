<?php

namespace WBuilder\Core\Messages;

use PhpParser\JsonDecoder;
use WBuilder\Core\Models\Product;
use WBuilder\Core\Models\Template;

class GetProductRequest extends AbstractRequest
{
    protected Product $model;

    public function getData()
    {
        $data = $this->getBaseData(config("website-builder.microservices.product")."products/show", 'GET', $this->getParameter('id'));
        return $data;
    }


}
