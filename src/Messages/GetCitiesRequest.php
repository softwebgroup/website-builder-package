<?php

namespace WBuilder\Core\Messages;

use WBuilder\Core\Models\City;

class GetCitiesRequest extends AbstractRequest
{
    protected $data_type = 'list';
    protected City $model;

    public function getData()
    {
        $data = $this->getBaseData(config("website-builder.microservices.builder")."common/cities", 'GET');
        return $data;
    }

}
