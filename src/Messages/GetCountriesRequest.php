<?php

namespace WBuilder\Core\Messages;

use WBuilder\Core\Models\Country;

class GetCountriesRequest extends AbstractRequest
{
    protected $data_type = 'list';
    protected Country $model;

    public function getData()
    {
        $data = $this->getBaseData(config("website-builder.microservices.builder")."common/countries", 'GET');
        return $data;
    }

}
