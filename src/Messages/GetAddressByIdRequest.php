<?php

namespace WBuilder\Core\Messages;

use WBuilder\Core\Models\Address;
use WBuilder\Core\Types\ListOfArticles;

class GetAddressByIdRequest extends AbstractRequest
{
    protected Address $model;

    public function getData()
    {
        $data = $this->getBaseData('/customers/addresses/retrieveById', 'GET');
        return $data;
    }

}
