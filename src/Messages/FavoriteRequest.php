<?php

namespace WBuilder\Core\Messages;

use WBuilder\Core\Models\FavoriteAction;

class FavoriteRequest extends AbstractRequest
{
    protected FavoriteAction $model;

    public function getData()
    {
        $data = $this->getBaseData('/products/favorite', 'GET');
        return $data;
    }
}
