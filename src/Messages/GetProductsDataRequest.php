<?php

namespace WBuilder\Core\Messages;

use WBuilder\Core\Models\Product;
use WBuilder\Core\Models\ProductPaginate;
use WBuilder\Core\Types\ListOfArticles;
use WBuilder\Core\Types\ListOfProduct;

class GetProductsDataRequest extends AbstractRequest
{
    protected $data_type = 'paginate';
    protected ProductPaginate $model;

    public function getData()
    {
        $data = $this->getBaseData(config("website-builder.microservices.product")."products", 'POST');
        return $data;
    }

    
}
