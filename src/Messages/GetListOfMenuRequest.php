<?php

namespace WBuilder\Core\Messages;

use WBuilder\Core\Models\Menu;
use WBuilder\Core\Types\ListOfMenu;

class GetListOfMenuRequest extends AbstractRequest
{
    protected $data_type = 'list_object';
    protected ListOfMenu $list_type;
    protected Menu $model;

    public function getData()
    {
        $data = $this->getBaseData(config("website-builder.microservices.builder").'menu', 'GET');
        return $data;
    }

//    public function parseData(){
//        $meta = $this->template->meta->collect();
//        if($this->draft)
//            $meta = $this->draft->meta_data->meta->collect();
//        $records = $meta->where('meta_key', '=', 'menu')->first();
//
//        $data = $records->data->collect()->filter(function($item) {
//            return $item->group->name == "Header";
//        })->all();
//        return builder_put_data_to_list($data, ListOfMenu::class);
//    }
}
