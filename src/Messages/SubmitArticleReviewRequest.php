<?php

namespace WBuilder\Core\Messages;

use WBuilder\Core\Models\Customer;
use WBuilder\Core\Models\ArticleCustomerComment;
use WBuilder\Core\Types\ListOfProduct;
use WBuilder\Core\Types\ListOfProductColor;

class SubmitArticleReviewRequest extends AbstractRequest
{
    protected ArticleCustomerComment $model;

    public function getData()
    {
        $data = $this->getBaseData('/articles/review/submit', 'POST');
        return $data;
    }
}
