<?php

namespace WBuilder\Core\Messages;

use WBuilder\Core\Models\Article;
use WBuilder\Core\Models\Paginate;
use WBuilder\Core\Types\ListOfArticles;

class GetMetaDataRequest extends AbstractRequest
{
    protected $data_type = 'paginate';
    protected Paginate $model;

    public function getData()
    {
        $app_id = config("website-builder.authorize_key");
        $data = $this->getBaseData(config("website-builder.microservices.builder")."meta_elements/getByTemplateIdAndMetaKey/$app_id/{$this->getMetaKey()}", 'GET');
        return $data;
    }


}
