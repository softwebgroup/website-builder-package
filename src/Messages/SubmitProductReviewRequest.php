<?php

namespace WBuilder\Core\Messages;

use WBuilder\Core\Models\Customer;
use WBuilder\Core\Models\ProductCustomerReview;
use WBuilder\Core\Types\ListOfProduct;
use WBuilder\Core\Types\ListOfProductColor;

class SubmitProductReviewRequest extends AbstractRequest
{
    protected ProductCustomerReview $model;

    public function getData()
    {
        $data = $this->getBaseData('/products/review/submit', 'POST');
        return $data;
    }
}
