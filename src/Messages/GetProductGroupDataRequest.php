<?php

namespace WBuilder\Core\Messages;

use WBuilder\Core\Models\Article;
use WBuilder\Core\Models\Product;
use WBuilder\Core\Models\ProductPaginate;
use WBuilder\Core\Types\ListOfArticles;
use WBuilder\Core\Types\ListOfProduct;

class GetProductGroupDataRequest extends AbstractRequest
{
    protected $data_type = 'paginate';
    protected ProductPaginate $model;

    public function getData()
    {
        $app_id = config("website-builder.authorize_key");
        $data = $this->getBaseData(config("website-builder.microservices.product")."products", 'POST');
        return $data;
    }

}
