<?php

namespace WBuilder\Core\Messages;

use WBuilder\Core\Models\State;

class GetStatesRequest extends AbstractRequest
{
    protected $data_type = 'list';
    protected State $model;

    public function getData()
    {
        $data = $this->getBaseData(config("website-builder.microservices.builder")."common/states", 'GET');
        return $data;
    }

}
