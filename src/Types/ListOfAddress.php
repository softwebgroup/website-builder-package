<?php
namespace WBuilder\Core\Types;

use WBuilder\Core\Models\Address;
use Illuminate\Support\Collection;

class ListOfAddress
{
    protected ?array $items = [];

    /**
     * return items as collection for search
     * @return Collection
     */
    public function collect(){
        return collect($this->items);
    }

    /**
     * return all items
     * @return Address[]
     */
    public function all(){
        return $this->items;
    }

    /**
     * return count of items
     * @return int
     */
    public function count(){
        return count($this->items);
    }

    /**
     * insert new item to items
     * @param Address $data
     */
    public function insert(Address $data){
        $this->items[] = $data;
    }

    /**
     * return first of items
     * @return Address
     */
    public function first() : ?Address{
        if(count($this->items) > 0)
            return $this->items[0];
        return null;
    }

    /**
     * return last of items
     * @return Address
     */
    public function last() : ?Address{
        if(count($this->items) > 0)
            return $this->items[count($this->items) - 1];
        return null;
    }

    /**
     * return get item by index
     * @param $index
     * @return Address
     */
    public function get($index) : ?Address{
        if(count($this->items) > 0 && isset($this->items[$index]))
            return $this->items[$index];
        return null;
    }

    /**
     * search in items
     * @param $value
     * @param $key
     * @return Address
     */
    public function search($value, $key = 'id') : ?Address{
        return $this->collect()->firstWhere($key, '=', $value);
    }

    public function getClass(){
        return Address::class;
    }

    public function toJson(){
        $output = [];
        foreach ($this->items as $item){
            $output[] = $item->toJson();
        }
        return json_encode($output);
    }
}
