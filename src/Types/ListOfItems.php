<?php
namespace WBuilder\Core\Types;

use Illuminate\Support\Collection;
use WBuilder\Core\Classes\Cart\CartItem;
use WBuilder\Core\Models\ServiceRate;

class ListOfItems
{
    protected ?array $items  = [];
    /**
     * return items as collection for search
     * @return Collection
     */
    public function collect(){
        return collect($this->items);
    }

    /**
     * return all items
     * @return array
     */
    public function all(){
        return $this->items;
    }

    /**
     * return count of items
     * @return int
     */
    public function count(){
        return count($this->items);
    }


}
