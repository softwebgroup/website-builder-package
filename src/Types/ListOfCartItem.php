<?php
namespace WBuilder\Core\Types;

use Illuminate\Support\Collection;
use WBuilder\Core\Classes\Cart\CartItem;
use WBuilder\Core\Models\ServiceRate;

class ListOfCartItem
{
    protected ?array $items  = [];
    /**
     * return items as collection for search
     * @return Collection
     */
    public function collect(){
        return collect($this->items);
    }

    /**
     * return all items
     * @return CartItem[]
     */
    public function all(){
        return $this->items;
    }

    /**
     * return count of items
     * @return int
     */
    public function count(){
        return count($this->items);
    }

    /**
     * insert new item to cart
     * @param CartItem $cartItem
     */
    public function insert(CartItem $cartItem){
        $updated = false;
        foreach ($this->items as $index => $item){
            if($item->product->id == $cartItem->product->id){
                if($cartItem->color && $item->color && $item->color->id == $cartItem->color->id){
                    $item->quantity += $cartItem->quantity;
                    $updated = true;
                    break;
                }else{
                    if(!$cartItem->color && !$item->color){
                        $item->quantity += $cartItem->quantity;
                        $updated = true;
                        break;
                    }

                }
            }
        }
        if(!$updated){
            $this->items[] = $cartItem;
        }

    }

    /**
     * update quantity of items
     * @param CartItem $cartItem
     */
    public function update(CartItem $cartItem){
        $updated = false;
        foreach ($this->items as $index => $item){
            if($item->product->id == $cartItem->product->id){
                if($cartItem->color && $item->color && $item->color->id == $cartItem->color->id){
                    $item->quantity = $cartItem->quantity;
                    $updated = true;
                    break;
                }else{
                    if(!$cartItem->color && !$item->color){
                        $item->quantity = $cartItem->quantity;
                        $updated = true;
                        break;
                    }

                }
            }
        }
        if(!$updated){
            $this->items[] = $cartItem;
        }

    }
    /**
     * delete item of items
     * @param CartItem $cartItem
     */
    public function delete(CartItem $cartItem){
        foreach ($this->items as $index => $item){
            if($item->product->id == $cartItem->product->id){
                if($cartItem->color && $item->color && $item->color->id == $cartItem->color->id){
                    unset($this->items[$index]);
                    break;
                }else{
                    if(!$cartItem->color && !$item->color){
                        unset($this->items[$index]);
                        break;
                    }

                }
            }
        }

    }

    /**
     * delete item of items
     * @param CartItem $cartItem
     */
    public function remove($id){
        foreach ($this->items as $index => $item){
            if($item->id == $id){
                unset($this->items[$index]);
            }
        }

    }

    /**
     * return first of items
     * @return CartItem
     */
    public function first() : ?CartItem{
        if(count($this->items) > 0)
            return $this->items[0];
        return null;
    }

    /**
     * return last of items
     * @return CartItem
     */
    public function last() : ?CartItem{
        if(count($this->items) > 0)
            return $this->items[count($this->items) - 1];
        return null;
    }

    /**
     * return get item by index
     * @param int $index
     * @return CartItem
     */
    public function get($index) : ?CartItem{
        if(count($this->items) > 0 && isset($this->items[$index]))
            return $this->items[$index];
        return null;
    }

    /**
     * search in items
     * @param $value
     * @param $key
     * @return CartItem
     */
    public function search($value, $key = 'id') : ?CartItem{
        $collect = collect($this->items);
        return $collect->firstWhere($key, '=', $value);
    }

    /**
     * sum key in cart prices
     * @param $key
     * @return float
     */
    public function sum($key){
        $sum = 0;
        /* @var $item CartItem */
        foreach ($this->items as $item){
            if($item->product->price && $item->product->price->summary && isset($item->product->price->summary->$key)){
                $sum += ($item->product->price->summary->$key * $item->quantity);
            }
        }
        return $sum;
    }

    
    public function getClass(){
        return CartItem::class;
    }

    public function toJson(){
        $output = [];
        foreach ($this->items as $item){
            $output[] = $item->toJson();
        }
        return json_encode($output);
    }
}
