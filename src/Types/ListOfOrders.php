<?php
namespace WBuilder\Core\Types;

use WBuilder\Core\Models\Order;
use Illuminate\Support\Collection;

class ListOfOrders
{
    protected ?array $items = [];

    /**
     * return items as collection for search
     * @return Collection
     */
    public function collect(){
        return collect($this->items);
    }

    /**
     * return all items
     * @return Order[]
     */
    public function all(){
        return $this->items;
    }

    /**
     * return count of items
     * @return int
     */
    public function count(){
        return count($this->items);
    }

    /**
     * insert new item to items
     * @param Order $data
     */
    public function insert(Order $data){
        $this->items[] = $data;
    }

    /**
     * return first of items
     * @return Order
     */
    public function first() : ?Order{
        if(count($this->items) > 0)
            return $this->items[0];
        return null;
    }

    /**
     * return last of items
     * @return Order
     */
    public function last() : ?Order{
        if(count($this->items) > 0)
            return $this->items[count($this->items) - 1];
        return null;
    }

    /**
     * return get item by index
     * @param $index
     * @return Order
     */
    public function get($index) : ?Order{
        if(count($this->items) > 0 && isset($this->items[$index]))
            return $this->items[$index];
        return null;
    }

    /**
     * search in items
     * @param $value
     * @param $key
     * @return Order
     */
    public function search($value, $key = 'id') : ?Order{
        return $this->collect()->firstWhere($key, '=', $value);
    }

    public function getClass(){
        return Order::class;
    }

    public function toJson(){
        $output = [];
        foreach ($this->items as $item){
            $output[] = $item->toJson();
        }
        return json_encode($output);
    }
}
