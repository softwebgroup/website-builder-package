<?php
namespace WBuilder\Core\Types;

use Illuminate\Support\Collection;
use WBuilder\Core\Models\ArticleCustomerComment;

class ListOfArticleCustomerComment
{
    protected ?array $items = [];

    /**
     * return items as collection for search
     * @return Collection
     */
    public function collect(){
        return collect($this->items);
    }

    /**
     * return all items
     * @return ArticleCustomerComment[]
     */
    public function all(){
        return $this->items;
    }

    /**
     * return count of items
     * @return int
     */
    public function count(){
        return count($this->items);
    }

    /**
     * insert new item to items
     * @param ArticleCustomerComment $data
     */
    public function insert(ArticleCustomerComment $data){
        $this->items[] = $data;
    }

    /**
     * return first of items
     * @return ArticleCustomerComment
     */
    public function first() : ?ArticleCustomerComment{
        if(count($this->items) > 0)
            return $this->items[0];
        return null;
    }

    /**
     * return last of items
     * @return ArticleCustomerComment
     */
    public function last() : ?ArticleCustomerComment{
        if(count($this->items) > 0)
            return $this->items[count($this->items) - 1];
        return null;
    }

    /**
     * return get item by index
     * @param $index
     * @return ArticleCustomerComment
     */
    public function get($index) : ?ArticleCustomerComment{
        if(count($this->items) > 0 && isset($this->items[$index]))
            return $this->items[$index];
        return null;
    }

    /**
     * search in items
     * @param $value
     * @param $key
     * @return ArticleCustomerComment
     */
    public function search($value, $key = 'id') : ?ArticleCustomerComment{
        $collect = collect($this->items);
        return $collect->firstWhere($key, '=', $value);
    }

    public function getClass(){
        return ArticleCustomerComment::class;
    }

    public function toJson(){
        $output = [];
        foreach ($this->items as $item){
            $output[] = $item->toJson();
        }
        return json_encode($output);
    }
}
