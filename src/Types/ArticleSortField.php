<?php
namespace WBuilder\Core\Types;

class ArticleSortField
{
    public static $_LAST_SEEN_ARTICLES = "last_visited_at";
    public static  $_LATEST_ARTICLES = "created_at";
    public static  $_MOST_VISITED_ARTICLES = "visit_count";
    public static  $_POPULAR_ARTICLES = "comments_count";
}
