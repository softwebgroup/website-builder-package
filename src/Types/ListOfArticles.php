<?php
namespace WBuilder\Core\Types;

use WBuilder\Core\Models\Article;
use Illuminate\Support\Collection;

class ListOfArticles
{
    protected ?array $items = [];

    /**
     * return items as collection for search
     * @return Collection
     */
    public function collect(){
        return collect($this->items);
    }

    /**
     * return all items
     * @return Article[]
     */
    public function all(){
        return $this->items;
    }

    /**
     * return count of items
     * @return int
     */
    public function count(){
        return count($this->items);
    }

    /**
     * insert new item to items
     * @param Article $data
     */
    public function insert(Article $data){
        $this->items[] = $data;
    }

    /**
     * return first of items
     * @return Article
     */
    public function first() : ?Article{
        if(count($this->items) > 0)
            return $this->items[0];
        return null;
    }

    /**
     * return last of items
     * @return Article
     */
    public function last() : ?Article{
        if(count($this->items) > 0)
            return $this->items[count($this->items) - 1];
        return null;
    }

    /**
     * return get item by index
     * @param $index
     * @return Article
     */
    public function get($index) : ?Article{
        if(count($this->items) > 0 && isset($this->items[$index]))
            return $this->items[$index];
        return null;
    }

    /**
     * search in items
     * @param $value
     * @param $key
     * @return Article
     */
    public function search($value, $key = 'id') : ?Article{
        return $this->collect()->firstWhere($key, '=', $value);
    }

    public function getClass(){
        return Article::class;
    }

    public function toJson(){
        $output = [];
        foreach ($this->items as $item){
            $output[] = $item->toJson();
        }
        return json_encode($output);
    }
}
