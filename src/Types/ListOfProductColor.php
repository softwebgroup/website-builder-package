<?php
namespace WBuilder\Core\Types;

use Illuminate\Support\Collection;
use WBuilder\Core\Models\ProductColor;

class ListOfProductColor
{
    protected ?array $items = [];

    /**
     * return items as collection for search
     * @return Collection
     */
    public function collect(){
        return collect($this->items);
    }

    /**
     * return all items
     * @return ProductColor[]
     */
    public function all(){
        return $this->items;
    }

    /**
     * return count of items
     * @return int
     */
    public function count(){
        return count($this->items);
    }

    /**
     * insert new item to items
     * @param ProductColor $data
     */
    public function insert(ProductColor $data){
        $this->items[] = $data;
    }

    /**
     * return first of items
     * @return ProductColor
     */
    public function first() : ?ProductColor{
        if(count($this->items) > 0)
            return $this->items[0];
        return null;
    }

    /**
     * return last of items
     * @return ProductColor
     */
    public function last() : ?ProductColor{
        if(count($this->items) > 0)
            return $this->items[count($this->items) - 1];
        return null;
    }

    /**
     * return get item by index
     * @param $index
     * @return ProductColor
     */
    public function get($index) : ?ProductColor{
        if(count($this->items) > 0 && isset($this->items[$index]))
            return $this->items[$index];
        return null;
    }

    /**
     * search in items
     * @param $value
     * @param $key
     * @return ProductColor
     */
    public function search($value, $key = 'id') : ?ProductColor{
        $collect = collect($this->items);
        return $collect->firstWhere($key, '=', $value);
    }

    public function has($values, $key = 'id'){
        $collect = collect($this->items);
        return $collect->whereIn($key, $values);
    }

    public function getClass(){
        return ProductColor::class;
    }

    public function toJson(){
        $output = [];
        foreach ($this->items as $item){
            $output[] = $item->toJson();
        }
        return json_encode($output);
    }
}
