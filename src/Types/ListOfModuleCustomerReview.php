<?php
namespace WBuilder\Core\Types;

use Illuminate\Support\Collection;
use WBuilder\Core\Models\ModuleCustomerReview;

class ListOfModuleCustomerReview
{
    protected ?array $items = [];

    /**
     * return items as collection for search
     * @return Collection
     */
    public function collect(){
        return collect($this->items);
    }

    /**
     * return all items
     * @return ModuleCustomerReview[]
     */
    public function all(){
        return $this->items;
    }

    /**
     * return count of items
     * @return int
     */
    public function count(){
        return count($this->items);
    }

    /**
     * insert new item to items
     * @param ModuleCustomerReview $data
     */
    public function insert(ModuleCustomerReview $data){
        $this->items[] = $data;
    }

    /**
     * return first of items
     * @return ModuleCustomerReview
     */
    public function first() : ?ModuleCustomerReview{
        if(count($this->items) > 0)
            return $this->items[0];
        return null;
    }

    /**
     * return last of items
     * @return ModuleCustomerReview
     */
    public function last() : ?ModuleCustomerReview{
        if(count($this->items) > 0)
            return $this->items[count($this->items) - 1];
        return null;
    }

    /**
     * return get item by index
     * @param $index
     * @return ModuleCustomerReview
     */
    public function get($index) : ?ModuleCustomerReview{
        if(count($this->items) > 0 && isset($this->items[$index]))
            return $this->items[$index];
        return null;
    }

    /**
     * search in items
     * @param $value
     * @param $key
     * @return ModuleCustomerReview
     */
    public function search($value, $key = 'id') : ?ModuleCustomerReview{
        $collect = collect($this->items);
        return $collect->firstWhere($key, '=', $value);
    }

    public function getClass(){
        return ModuleCustomerReview::class;
    }

    public function toJson(){
        $output = [];
        foreach ($this->items as $item){
            $output[] = $item->toJson();
        }
        return json_encode($output);
    }
}
