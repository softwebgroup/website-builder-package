<?php
namespace WBuilder\Core\Types;

use Illuminate\Support\Collection;
use WBuilder\Core\Models\Service;

class ListOfService
{
    protected ?array $items = [];

    /**
     * return items as collection for search
     * @return Collection
     */
    public function collect(){
        return collect($this->items);
    }

    /**
     * return all items
     * @return Service[]
     */
    public function all(){
        return $this->items;
    }

    /**
     * return count of items
     * @return int
     */
    public function count(){
        return count($this->items);
    }

    /**
     * insert new item to items
     * @param Service $data
     */
    public function insert(Service $data){
        $this->items[] = $data;
    }

    /**
     * return first of items
     * @return Service
     */
    public function first() : ?Service{
        if(count($this->items) > 0)
            return $this->items[0];
        return null;
    }

    /**
     * return last of items
     * @return Service
     */
    public function last() : ?Service{
        if(count($this->items) > 0)
            return $this->items[count($this->items) - 1];
        return null;
    }

    /**
     * return get item by index
     * @param $index
     * @return Service
     */
    public function get($index) : ?Service{
        if(count($this->items) > 0 && isset($this->items[$index]))
            return $this->items[$index];
        return null;
    }

    /**
     * search in items
     * @param $value
     * @param $key
     * @return Service
     */
    public function search($value, $key = 'id') : ?Service{
        $collect = collect($this->items);
        return $collect->firstWhere($key, '=', $value);
    }

    public function getClass(){
        return Service::class;
    }
    
    public function toJson(){
        $output = [];
        foreach ($this->items as $item){
            $output[] = $item->toJson();
        }
        return json_encode($output);
    }
}
