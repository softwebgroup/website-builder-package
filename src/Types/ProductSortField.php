<?php
namespace WBuilder\Core\Types;

class ProductSortField
{
    public $_LAST_SEEN_PRODUCTS = "last_visited_at";
    public $_LATEST_PRODUCTS = "created_at";
    public $_MOST_VISITED_PRODUCTS = "visit_count";
    public $_MOST_RATTED_PRODUCTS = "rating";
}
