<?php
namespace WBuilder\Core\Types;

use Illuminate\Support\Collection;
use WBuilder\Core\Models\ArticleMedia;

class ListOfArticleMedia
{
    protected ?array $items = [];

    /**
     * return items as collection for search
     * @return Collection
     */
    public function collect(){
        return collect($this->items);
    }

    /**
     * return all items
     * @return ArticleMedia[]
     */
    public function all(){
        return $this->items;
    }

    /**
     * return count of items
     * @return int
     */
    public function count(){
        return count($this->items);
    }

    /**
     * insert new item to items
     * @param ArticleMedia $data
     */
    public function insert(ArticleMedia $data){
        $this->items[] = $data;
    }

    /**
     * return first of items
     * @return ArticleMedia
     */
    public function first() : ?ArticleMedia{
        if(count($this->items) > 0)
            return $this->items[0];
        return null;
    }

    /**
     * return last of items
     * @return ArticleMedia
     */
    public function last() : ?ArticleMedia{
        if(count($this->items) > 0)
            return $this->items[count($this->items) - 1];
        return null;
    }

    /**
     * return get item by index
     * @param $index
     * @return ArticleMedia
     */
    public function get($index) : ?ArticleMedia{
        if(count($this->items) > 0 && isset($this->items[$index]))
            return $this->items[$index];
        return null;
    }

    /**
     * search in items
     * @param $value
     * @param $key
     * @return ArticleMedia
     */
    public function search($value, $key = 'id') : ?ArticleMedia{
        $collect = collect($this->items);
        return $collect->firstWhere($key, '=', $value);
    }

    public function getClass(){
        return ArticleMedia::class;
    }

    public function toJson(){
        $output = [];
        foreach ($this->items as $item){
            $output[] = $item->toJson();
        }
        return json_encode($output);
    }
}
