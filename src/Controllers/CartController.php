<?php

namespace App\Http\Controllers\Builder;

use App\Http\Controllers\Controller;

class CartController extends Controller
{
    use \WBuilder\Core\Traits\CartController;
}
