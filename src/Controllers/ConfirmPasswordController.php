<?php

namespace App\Http\Controllers\Builder;

use App\Http\Controllers\Controller;

class ConfirmPasswordController extends Controller
{
    use \WBuilder\Core\Traits\ConfirmPasswordController;

}
