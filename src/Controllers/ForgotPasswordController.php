<?php

namespace App\Http\Controllers\Builder;

use App\Http\Controllers\Controller;

class ForgotPasswordController extends Controller
{
    use \WBuilder\Core\Traits\ForgotPasswordController;

}
