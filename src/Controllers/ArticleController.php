<?php

namespace App\Http\Controllers\Builder;

use App\Http\Controllers\Controller;

class ArticleController extends Controller
{
    use \WBuilder\Core\Traits\ArticleController;
}
