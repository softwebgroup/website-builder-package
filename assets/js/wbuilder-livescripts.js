(function (window, document, $) {
    function nl2br (str, is_xhtml) {
        if (typeof str === 'undefined' || str === null) {
            return '';
        }
        var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
        return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
    }
    let eventMethod = window.addEventListener
        ? "addEventListener"
        : "attachEvent";
    let eventer = window[eventMethod];
    let messageEvent = eventMethod === "attachEvent"
        ? "onmessage"
        : "message";
    eventer(messageEvent, function (e) {
        if(!e.data) return;
        let data = JSON.parse(e.data);
        let ref = '.ref_elm_'+data.ref;
        // if(data.ref === "slideshow"){
        //     window.location.reload();
        // }
        if(data.action === "update_html"){
            $(ref).each(function (){
                $(this).html(nl2br(data.data));
            })

        }
        if(data.action === "update_text"){
            $(ref).each(function (){
                $(this).text((data.data));
            })
        }
        if(data.action === "update_text_color"){
            $(ref).each(function (){
                $(this).css('color', data.data);
            })
        }
        if(data.action === "update_text_size"){
            $(ref).each(function (){
                $(this).css('font-size', data.data);
            })
        }


        if(data.action === "update_href"){
            $(ref).each(function (){
                $(this).attr('href', (data.data));
            })
        }
        if(data.action === "update_image"){
            if($(ref).length > 0){
                $(ref).each(function (){
                    if($(this).prop('tagName').toLowerCase() === "img"){
                        $(this).attr('src', (data.data));
                    }else{
                        $(this).css('background-image', `url('${data.data}')`);
                    }
                })
            }else if($(`.ref_elm_post_${data.id}_slideshow`).length > 0){
                ref = $(`.ref_elm_post_${data.id}_slideshow div.tp-bgimg`);
                $(ref).each(function (){
                    $(this).css('background-image', 'url("' + data.data + '")')
                        .attr('src', data.data)
                        .data('src', data.data);
                })
            }
        }
        if(data.action === "update_post_image"){
            $(ref).each(function (){
                if($(this).prop('tagName').toLowerCase() === "img"){
                    $(this).attr('src', (data.data));
                }else{
                    $(this).css('background-image', `url('${data.data}')`);
                }
            })
        }

        if(data.action === "update_social"){
            $(ref).each(function (){
                let template = $(this).find('template');
                if(template.length > 0){
                    $(ref+" .ref_elm_container").html("");
                    data.data.map((m) => {
                        let html = $(template[0]).html();
                        html = html.replaceAll("@URL@", m.url).replaceAll("@ID@", m.id).replaceAll("@ICON@", m.icon);
                        $(ref+" .ref_elm_container").append($(html))
                    })

                }
            })
        }

        if(data.action === "update_section_status"){
            $(ref).each(function (){
                if(data.data){
                    $(this).hide();
                }else{
                    $(this).show();
                }

            })
        }
        if(data.action === "update_image_group"){
            $(ref).each(function (){
                if($(this).prop('tagName').toLowerCase() === "img"){
                    $(this).attr('src', (data.data));
                }else{
                    $(this).css('background-image', `url('${data.data}')`);
                }
            })
        }
        if(data.action === "group_action_page"){
            if(data.data.type && data.data.type === "scroll"){
                $('body,html').stop(true).animate({
                    'scrollTop': $(`#${data.data.url}`).offset().top
                }, 800 )

            }
            if(data.data.type && data.data.type === "redirect"){
                let newURL = window.location.protocol + "//" + window.location.host + data.data.url + window.location.search;
                window.location.href = newURL
            }

        }
        if(data.action === "update_data_template"){
            $(ref).each(function (){
                let self = this;
                (function (window, document, $, undefined){
                    $.ajax({
                        url: "/ajax/template/data-refresh",
                        type: "POST",
                        data: {
                            data: JSON.stringify(data.data),
                            template: $(self).data('template'),
                            param: $(self).data('param')
                        },
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(response){
                            $(self).html(response.html);
                        }
                    });
                }(window, document, jQuery));

            });
        }
        if(data.action === "create_new_page"){
            let newURL = window.location.protocol + "//" + window.location.host + "/blank_page/" + encodeURIComponent(JSON.stringify(data.data));
            window.location.href = newURL
        }
    });
}(window, document, jQuery));
