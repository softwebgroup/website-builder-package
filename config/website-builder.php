<?php
return [
    'authorize_key' => env('WB_AUTHORIZE_KEY', 'authorize_key'),
    'client_id' => env('WB_CLIENT_ID', 'client_id'),
    'client_secret' => env('WB_CLIENT_SECRET', 'client_secret'),
    "preview_mode" => env('WB_PREVIEW_MODE', true),
    "microservices" => [
        'builder' => "https://dev.wpbuilder.ca/ms-wb/v1/",
        'auth' => "https://dev.wpbuilder.ca/ms-auth/v1/",
        'media' => "https://dev.wpbuilder.ca/ms-media/v1/",
        'article' => "https://dev.wpbuilder.ca/ms-articles/v1/",
        'review' => "https://dev.wpbuilder.ca/ms-reviews/v1/",
        'product' => "https://dev.wpbuilder.ca/ms-products/v1/",

    ],
    "i18n"     => false,
    "https"    => false,
    "modules"  => [

    ]

];
